<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::group([
    'namespace' => 'Api',
], function () {

    Route::prefix('Auth')->group(function () {

        Route::post('login', 'LoginController@login');
        Route::post('register', 'RegisterController@register');
	    Route::post('forgetPassword', 'ForgotPasswordController@forgetPassword');        
	    Route::post('resetPassword', 'ForgotPasswordController@resetPassword');
        Route::post('changePassword', 'ResetPasswordController@changPassword');
        Route::post('checkCode', 'ResetPasswordController@checkCodeActivation');
        Route::post('resendCode', 'ResetPasswordController@resendCode');
        Route::post('editProfile', 'UsersController@editProfile')->middleware('apiToken');
        Route::post('logOut','LoginController@logOut');

    });
    
    Route::prefix('users')->group(function () {
        
        Route::get('userId','UsersController@getUser');
        Route::get('listFavourite','UsersController@listFavourite');
        Route::post('makeFavourite','UsersController@makeFavourite');

        Route::get('notification','UsersController@notification');
        Route::get('notifyAndMessageCount','UsersController@notifyAndMessageCount');
        
        Route::get('listFollowers','UsersController@listFollowers');
        Route::get('listBlocking','UsersController@listBlocking');
        Route::get('adsFollowers','UsersController@adsFollowers');
        Route::post('followOrUnFollow','UsersController@followOrUnFollowUser');
        Route::post('blockUser','UsersController@blockUser');
    });
    

    Route::prefix('search')->group(function () {

        Route::get('Ads','SearchController@searchAds');

    });

    Route::prefix('categories')->group(function () {

        Route::get('/','CategoriesController@getCategory');


    });

    Route::prefix('cities')->group(function () {

        Route::get('/','CityController@getCity');


    });

    Route::prefix('ad')->group(function () {

        Route::get('listLatest','AdsController@listAdsLatest');
        Route::get('randomData','AdsController@dataAds');
        Route::get('myAdvertisements','AdsController@myAdvertisements');
        Route::get('similar','AdsController@similarAdvertisement'); 
        Route::get('show','AdsController@getAdvertisementById');
        Route::post('store','AdsController@store')->middleware('order'); 
        Route::post('edit','AdsController@editAdvertisement')->middleware('order'); 
        Route::post('delete','AdsController@delete');
        Route::post('adsTaxs','AdsController@adsTaxs');
        Route::post('acceptedOrRefuseTax','AdsController@acceptedOrRefuseTax');
        Route::post('canComment','AdsController@canComment');
        Route::post('updateTime','AdsController@updateAdvertisement');
        
    });
    
     Route::prefix('comments')->group(function () {

        Route::get('ListComments','CommentsController@ListCommentBelongToAds');
        Route::post('makeComment','CommentsController@makeComment');
        Route::post('replyComment','CommentsController@replyComment');
        Route::post('editComment','CommentsController@editComment');
        Route::post('deleteComment','CommentsController@deleteComment');

    });
    
     Route::prefix('messages')->group(function () {

        
        Route::get('all','MessageController@index');
        Route::post('storeMessage','MessageController@storeMessage');
        Route::get('betweenUsers','MessageController@getAllMessageBetween');
        Route::post('makeAsRead','MessageController@makeAsRead');

    });


    Route::prefix('setting')->group(function () {


        Route::get('frequentlyAsked ','SettingController@frequentlyAsked');
        Route::get('suspendElement','SettingController@suspendElement');
        Route::get('aboutUs','SettingController@aboutUs');
        Route::get('appPercentage','SettingController@appPercentage');
        Route::get('terms','SettingController@terms');
        Route::get('treaty_of_Use','SettingController@treaty_of_Use');
        Route::get('banks','SettingController@listBanks');
        Route::get('global','SettingController@global');
        Route::post('contactUs','SettingController@contactUs');
        Route::post('reportsForads','SettingController@reportsForads');
        
        Route::post('reportComment','SettingController@reportComment');
        
        Route::post('bankTransfer','SettingController@bankTransfer');
        Route::post('testNotify','SettingController@testNotify');
        
        

    });

    Route::post('uploadImages','UsersController@uploadImage');

});


Route::post('/deleteNotify',function(){
    
    return \App\Models\Notification::where('sender_id',1)->delete();
    
});

 





