<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
    
    Route::group(['prefix' => 'administrator'], function () {


        Route::get('/login', 'Admin\LoginController@login')->name('admin.login');
        Route::post('/login', 'Admin\LoginController@postLogin')->name('admin.postLogin');
        
        // Password Reset Routes...
        
        Route::get('password/reset', 'Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('administrator.password.request');
        Route::post('password/email', 'Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('administrator.password.email');
        Route::get('password/reset/{token}', 'Admin\Auth\ResetPasswordController@showResetForm')->name('administrator.password.reset.token');
        Route::post('password/reset', 'Admin\Auth\ResetPasswordController@reset');
        
    });
    
    Route::group(['prefix' => 'administrator', 'middleware' => ['admin']], function () {
        
        Route::get('/', 'Admin\HomeController@index')->name('home');
        Route::get('/home', 'Admin\HomeController@index')->name('admin.home');
    
    
        // administrators --- Admin Panel users
        // add admin or some one who controll Or help for admin panel
    
        Route::resource('helpAdmin', 'Admin\HelpAdminController');
        Route::get('helpAdmin/{id}/delete', 'Admin\HelpAdminController@delete')->name('helpAdmin.delete');
        Route::post('helpAdmin/{id}/delete', 'Admin\HelpAdminController@deleteHelpAdmin')->name('helpAdmin.message.delete');
        Route::post('helpAdmin/{id}/suspend', 'Admin\HelpAdminController@suspendHelpAdmin')->name('helpAdmin.message.suspend');
        Route::get('user/{id}/delete', 'Admin\UsersController@delete')->name('user.for.delete');
        Route::post('user/suspend', 'Admin\HelpAdminController@suspend')->name('user.suspend');
        // Roles routes ..........
        Route::resource('roles', 'Admin\RolesController');
        Route::post('role/delete', 'Admin\RolesController@delete')->name('role.delete');
    
    
        Route::resource('users', 'Admin\UsersController');
        Route::post('accepted','Admin\UsersController@accpetedUser')->name('user.accepted');
    
        Route::get('settings/aboutus', 'Admin\SettingsController@aboutus')->name('settings.aboutus');
                Route::get('settings/taxs', 'Admin\SettingsController@taxs')->name('settings.taxs');
        Route::get('settings/terms', 'Admin\SettingsController@terms')->name('settings.terms');
        Route::get('settings/suspendElement', 'Admin\SettingsController@suspendElement')->name('settings.suspendElement');

        Route::resource('faqs', 'Admin\FaqsController');

        Route::get('/settings/app-general-settings', 'Admin\SettingsController@appGeneralSettings')->name('settings.app.general');
        Route::get('settings/contacts', 'Admin\SettingsController@contactus')->name('settings.contactus');


        Route::post('/settings', 'Admin\SettingsController@store')->name('administrator.settings.store');
    
        Route::post('contactus/reply/{id}', 'Admin\SupportsController@reply')->name('support.reply');
        Route::get('contactus', 'Admin\SupportsController@index')->name('support.index');
        Route::get('contactus/{id}', 'Admin\SupportsController@show')->name('support.show');
        Route::post('support/contact/delete', 'Admin\SupportsController@delete')->name('support.contact.delete');


        Route::resource('supports', 'Admin\SupportsController');
        Route::post('supports/delete', 'Admin\SupportsController@delete')->name('supports.delete');

        Route::resource('types', 'Admin\TypesSupportController');

        Route::get('public/notifications', 'Admin\NotificationsController@publicNotifications')->name('public.notifications');
        Route::get('public/notifications/create', 'Admin\NotificationsController@createPublicNotifications')->name('create.public.notifications');
        Route::get('public/notifications/problems', 'Admin\NotificationsController@problems')->name('public.notifications.problems');

        Route::post('send/public/notifications', 'Admin\NotificationsController@sendPublicNotifications')->name('send.public.notifications');
        Route::post('notifications/delete', 'Admin\NotificationsController@delete')->name('notification.delete');


        Route::post('city/delete/group', 'Admin\CitiesController@groupDelete')->name('cities.group.delete');
        Route::post('cities/delete', 'Admin\CitiesController@delete')->name('city.delete');
        Route::resource('cities', 'Admin\CitiesController');
        Route::post('city/suspend', 'Admin\CitiesController@suspend')->name('city.suspend');

        // -------------------------------------- categories .................
        Route::resource('categories', 'Admin\CategoriesController');
        Route::post('categories/delete', 'Admin\CategoriesController@delete')->name('categories.delete');
        Route::post('categories/suspend', 'Admin\CategoriesController@suspend')->name('categories.suspend');


        Route::resource('ads', 'Admin\AdsController');
        Route::post('ads/suspend', 'Admin\AdsController@suspend')->name('ads.suspend');


        Route::resource('banks', 'Admin\BanksController');
        Route::post('bank/suspend', 'Admin\BanksController@suspend')->name('bank.suspend');


        Route::resource('reports', 'Admin\ReportsController');
        
        Route::post('/logout', 'Admin\LoginController@logout')->name('administrator.logout');
        
    });
    
        Route::get('/landing', 'LandingPageController@index')->name('langPage.index');
        
        Route::get('/share', 'LandingPageController@share')->name('langPage.share');
        
        Route::post('user/update/token', function (Request $request) {

    $user = \App\User::whereId($request->id)->first();


    if ($request->token) {
        $data = \App\Models\Device::where('device', $request->token)->first();
        if ($data) {
            $data->user_id = $user->id;
            $data->save();
        } else {


            $data = new \App\Models\Device;
            $data->device = $request->token;
            $data->user_id = $user->id;
            $data->device_type = 'web';
            $data->save();
        }
    }


})->name('user.update.token');
        
         
    

    
    
    
    
    
