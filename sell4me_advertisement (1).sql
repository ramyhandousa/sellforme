-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 31, 2020 at 05:00 PM
-- Server version: 5.7.29
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sell4me_advertisement`
--

-- --------------------------------------------------------

--
-- Table structure for table `abilities`
--

CREATE TABLE `abilities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entity_id` int(10) UNSIGNED DEFAULT NULL,
  `entity_type` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `only_owned` tinyint(1) NOT NULL DEFAULT '0',
  `scope` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abilities`
--

INSERT INTO `abilities` (`id`, `name`, `title`, `entity_id`, `entity_type`, `only_owned`, `scope`, `created_at`, `updated_at`) VALUES
(2, 'users_manage', 'إدارة المستخدمين', NULL, NULL, 0, NULL, NULL, NULL),
(3, 'statistics_manage', 'إدارة الإحصائيات', NULL, NULL, 0, NULL, NULL, NULL),
(4, '*', 'All abilities', NULL, '*', 0, NULL, '2018-06-27 10:24:49', '2018-06-27 10:24:49'),
(5, 'app_users_manage', 'إدارة مستخدمي التطبيق', NULL, NULL, 0, NULL, NULL, NULL),
(6, 'permission_manage', 'إدارة الصلاحيات والمهام', NULL, NULL, 0, NULL, NULL, NULL),
(7, 'content_manage', 'إدارة المحتوى', NULL, NULL, 0, NULL, NULL, NULL),
(8, 'contact_manage', 'إدارة التواصل', NULL, NULL, 0, NULL, NULL, NULL),
(9, 'settings_manage', 'إدارة الإعدادات', NULL, NULL, 0, NULL, NULL, NULL),
(10, 'reports_manage', 'إدارة التقارير', NULL, NULL, 0, NULL, NULL, NULL),
(11, 'members_manage', 'إدارة العضويات\r\n', NULL, NULL, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `city_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitute` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitute` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `can_comment` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'can comment or not ',
  `is_suspend` tinyint(4) NOT NULL DEFAULT '0',
  `is_read` enum('true','false') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'false',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `user_id`, `category_id`, `city_id`, `name`, `description`, `phone`, `address`, `latitute`, `longitute`, `images`, `can_comment`, `is_suspend`, `is_read`, `created_at`, `updated_at`) VALUES
(76, 130, 7, 2, 'فله في الرياض', 'وصف', '+966583847762', 'الرياض', NULL, NULL, '[\"1\",\"2\"]', 0, 1, 'true', '2019-11-18 22:14:06', '2019-12-08 00:46:01'),
(77, 129, 7, 2, 'فلل للبيع', 'فلل للبيع', '+966557576746', 'الخبر', NULL, NULL, '[\"3\",\"4\"]', 0, 1, 'true', '2019-11-18 22:25:41', '2019-12-08 00:46:12'),
(78, 131, 5, 32, 'sell for me', 'Does', '+966512345688', 'Makkah', NULL, NULL, '[\"5\"]', 0, 1, 'true', '2019-11-19 19:17:45', '2019-12-08 00:45:58'),
(79, 133, 7, 32, 'شيفروليه', 'شيفروليه', '+966512345', 'السعودية', NULL, NULL, '[\"6\"]', 0, 1, 'true', '2019-11-20 15:37:35', '2019-12-08 00:46:09'),
(80, 133, 6, 5, 'نيسان', 'وصف المنتج', '+966512345677', 'نيسان', NULL, NULL, '[\"7\"]', 0, 1, 'true', '2019-11-20 16:50:25', '2019-12-08 00:45:51'),
(81, 132, 8, 5, 'تويتا', 'وصف المنتج', '+966512345678', 'تويتا', NULL, NULL, '[\"8\"]', 0, 1, 'true', '2019-11-20 18:46:13', '2019-12-08 00:45:49'),
(82, 132, 5, 5, 'شيفروليه', 'وصف المنتج', '+966512345678', 'المنصورة', NULL, NULL, '[\"9\"]', 0, 1, 'true', '2019-11-20 18:49:55', '2019-12-08 00:46:08'),
(83, 129, 7, 2, 'بقره', 'بقره', '+966557576746', 'الخبر', NULL, NULL, '[\"11\"]', 0, 1, 'true', '2019-11-22 01:28:24', '2019-12-08 00:44:20'),
(84, 129, 3, 2, 'هيونداي', 'هيونداي', '+966557576746', 'الرياض', NULL, NULL, '[\"12\",\"13\"]', 1, 1, 'true', '2019-11-25 18:33:01', '2019-12-08 00:45:24'),
(85, 129, 14, 5, 'اعلان تجريبي', 'محتوى', '+966557576746', 'الخبر', NULL, NULL, '[\"14\"]', 0, 1, 'true', '2019-11-27 23:10:42', '2019-12-08 00:45:46'),
(86, 129, 22, 2, 'عقار الرياض فلل', 'عقار الرياض فلل', '+966557576746', 'الرياض', NULL, NULL, '[\"19\",\"20\"]', 0, 1, 'true', '2019-12-24 03:28:55', '2020-01-11 22:19:11'),
(87, 129, 21, 5, 'عقار اراضي جده', 'عقار اراضي جده', '+966557576746', 'الرياض', NULL, NULL, '[\"21\",\"22\"]', 0, 1, 'true', '2019-12-24 03:38:16', '2020-01-11 22:19:15'),
(88, 129, 83, 32, 'سيارات اخرى المدينة المنورة', 'سيارات اخرى المدينة المنورة', '+966557576746', 'المدينة المنورة', NULL, NULL, '[\"23\",\"24\"]', 0, 1, 'true', '2019-12-24 14:53:08', '2020-01-11 22:19:07'),
(90, 129, 45, 61, 'سيارة مصر رينو', 'سيارة مصر ريتوو', '+966557576746', 'القاهرة', NULL, NULL, '[\"27\"]', 0, 1, 'false', '2020-01-08 20:56:53', '2020-01-11 22:19:05'),
(97, 129, 22, 69, 'سيارة عماني', 'سيارة \n\n\n\nعماني', '+966557576746', 'عمان', NULL, NULL, '[\"34\",\"35\",\"36\"]', 0, 0, 'true', '2020-03-14 21:13:35', '2020-03-14 22:18:04'),
(98, 129, 22, 35, 'فلل', 'فلل الخبر', '+966557576746', 'الخبر', NULL, NULL, '[\"37\"]', 0, 0, 'true', '2020-03-14 22:17:30', '2020-03-14 22:19:27'),
(99, 141, 87, 35, 'بيع مكيفات مستعمل وجديد', 'بيع مكيفات مستعمل', '+966557277821', 'الدمام', NULL, NULL, '[\"38\"]', 0, 0, 'true', '2020-03-20 03:32:07', '2020-03-20 03:32:18'),
(100, 130, 22, 61, 'عقار جديد', 'وصف العقار', '+201148863042', 'مصر', NULL, NULL, '[\"39\"]', 0, 0, 'true', '2020-03-20 04:04:46', '2020-03-20 04:06:00');

-- --------------------------------------------------------

--
-- Table structure for table `ads_taxs`
--

CREATE TABLE `ads_taxs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ad_id` int(10) UNSIGNED NOT NULL,
  `price` double NOT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ads_taxs`
--

INSERT INTO `ads_taxs` (`id`, `user_id`, `ad_id`, `price`, `currency`, `notes`, `status`, `created_at`, `updated_at`) VALUES
(1, 131, 79, 500, NULL, NULL, -1, '2019-11-20 18:32:03', '2019-11-20 18:40:12'),
(2, 131, 82, 248, 'درهم اماراتي', 'H', 1, '2019-11-20 18:54:22', '2019-11-20 19:10:13'),
(3, 131, 82, 2556, 'درهم اماراتي', 'Bj', 1, '2019-11-20 19:14:15', '2019-11-20 19:14:57'),
(4, 131, 82, 528, 'درهم اماراتي', 'Hhh', 1, '2019-11-20 19:16:38', '2019-11-20 19:16:45'),
(5, 131, 82, 255, 'درهم اماراتي', 'Hh', 1, '2019-11-20 19:30:06', '2019-11-20 19:30:17'),
(6, 131, 82, 588, 'درهم اماراتي', 'Hhh', 1, '2019-11-20 19:53:54', '2019-11-20 19:54:04'),
(7, 131, 82, 2333, 'درهم اماراتي', 'No', 1, '2019-11-21 01:59:52', '2019-11-21 02:00:06'),
(8, 131, 82, 2443, 'درهم اماراتي', 'Ggg', 1, '2019-11-21 02:00:55', '2019-11-21 02:01:03'),
(9, 131, 82, 855, 'دينار كويتي', 'Hhh', 1, '2019-11-21 02:01:18', '2019-11-21 02:01:23'),
(10, 131, 82, 55, 'درهم اماراتي', 'Bb', -1, '2019-11-21 02:02:06', '2019-11-21 02:02:14'),
(11, 131, 82, 55, 'درهم اماراتي', 'Bb', 1, '2019-11-21 02:02:54', '2019-11-21 02:03:06'),
(12, 131, 82, 55, 'درهم اماراتي', 'Bubble', 1, '2019-11-21 02:05:18', '2019-11-21 02:05:24'),
(13, 131, 82, 25, 'درهم اماراتي', 'Bb', 1, '2019-11-21 02:08:10', '2019-11-21 02:08:23'),
(14, 131, 82, 544, 'درهم اماراتي', 'Hh', 1, '2019-11-21 02:12:40', '2019-11-21 02:12:49'),
(15, 129, 76, 500, NULL, NULL, 0, '2019-11-21 17:40:52', '2019-11-21 17:40:52'),
(16, 132, 80, 500, NULL, NULL, 1, '2019-11-24 17:48:52', '2019-11-24 18:15:02'),
(17, 132, 80, 589, NULL, NULL, 1, '2019-11-24 17:50:03', '2019-11-24 17:53:56'),
(18, 132, 80, 5, NULL, NULL, 1, '2019-11-24 18:19:10', '2019-11-24 18:20:50'),
(19, 132, 80, 5, NULL, NULL, 1, '2019-11-24 18:51:53', '2019-11-24 18:52:10'),
(20, 129, 76, 1000, 'ريال سعودي', 'ممتاز', 0, '2019-11-24 18:52:38', '2019-11-24 18:52:38'),
(21, 132, 80, 5, NULL, NULL, 0, '2019-11-24 18:54:22', '2019-11-24 18:54:22'),
(22, 132, 80, 5, NULL, NULL, 1, '2019-11-24 18:54:39', '2019-11-24 18:54:45'),
(23, 132, 80, 5, NULL, NULL, 1, '2019-11-24 19:17:52', '2019-11-24 19:18:01'),
(24, 132, 80, 5, NULL, NULL, 0, '2019-11-24 19:21:43', '2019-11-24 19:21:43'),
(25, 132, 80, 5, NULL, NULL, 0, '2019-11-24 19:21:55', '2019-11-24 19:21:55'),
(26, 132, 80, 5, NULL, NULL, 0, '2019-11-24 19:27:10', '2019-11-24 19:27:10'),
(27, 132, 80, 5, NULL, NULL, 0, '2019-11-24 19:27:24', '2019-11-24 19:27:24'),
(28, 132, 80, 5, NULL, NULL, 0, '2019-11-24 19:36:41', '2019-11-24 19:36:41'),
(29, 132, 80, 5, NULL, NULL, 0, '2019-11-24 20:22:11', '2019-11-24 20:22:11'),
(30, 132, 80, 5, NULL, NULL, 0, '2019-11-24 20:22:43', '2019-11-24 20:22:43'),
(31, 132, 80, 5, NULL, NULL, 0, '2019-11-24 20:24:13', '2019-11-24 20:24:13'),
(32, 132, 80, 5, NULL, NULL, 0, '2019-11-24 20:24:51', '2019-11-24 20:24:51'),
(33, 132, 80, 5, NULL, NULL, 0, '2019-11-24 20:30:52', '2019-11-24 20:30:52'),
(34, 132, 80, 5, NULL, NULL, 0, '2019-11-24 20:40:07', '2019-11-24 20:40:07'),
(35, 132, 80, 5, NULL, NULL, 0, '2019-11-24 20:41:31', '2019-11-24 20:41:31'),
(36, 132, 80, 5, NULL, NULL, 0, '2019-11-24 20:42:06', '2019-11-24 20:42:06'),
(37, 132, 80, 5, NULL, NULL, 0, '2019-11-24 20:45:41', '2019-11-24 20:45:41'),
(38, 132, 80, 5, NULL, NULL, 0, '2019-11-24 20:46:50', '2019-11-24 20:46:50'),
(39, 132, 80, 5, NULL, NULL, 0, '2019-11-24 20:49:08', '2019-11-24 20:49:08'),
(40, 132, 80, 5, NULL, NULL, 0, '2019-11-24 20:51:31', '2019-11-24 20:51:31'),
(41, 132, 80, 5, NULL, NULL, 0, '2019-11-24 20:51:44', '2019-11-24 20:51:44'),
(42, 132, 80, 5, NULL, NULL, 0, '2019-11-24 20:53:27', '2019-11-24 20:53:27'),
(43, 132, 80, 5, NULL, NULL, 0, '2019-11-24 20:53:48', '2019-11-24 20:53:48'),
(44, 132, 80, 5, NULL, NULL, 0, '2019-11-24 21:03:23', '2019-11-24 21:03:23'),
(45, 132, 80, 5, NULL, NULL, 0, '2019-11-24 21:03:59', '2019-11-24 21:03:59'),
(46, 129, 76, 4005, 'دينار بحريني', 'جاهز', 1, '2019-11-26 20:25:05', '2019-11-26 20:25:16'),
(47, 129, 76, 5000033, 'دينار بحريني', 'تدخله', 1, '2019-11-26 20:25:46', '2019-11-26 20:25:56'),
(48, 130, 82, 788, 'درهم اماراتي', 'Hi', 1, '2019-11-26 22:37:29', '2019-11-26 22:38:48'),
(49, 130, 82, 7666, 'درهم اماراتي', 'Hi', 1, '2019-11-26 22:46:33', '2019-11-26 22:46:55'),
(50, 130, 82, 33333, 'دينار كويتي', 'Did', 1, '2019-11-26 22:51:01', '2019-11-26 22:51:12'),
(51, 130, 82, 44, 'دينار كويتي', 'Did', 1, '2019-11-26 22:52:03', '2019-11-26 22:52:07'),
(52, 130, 82, 5555, 'درهم اماراتي', 'Sss', 1, '2019-11-26 22:52:59', '2019-11-26 22:53:03'),
(53, 130, 82, 88, 'درهم اماراتي', 'J', 1, '2019-11-26 22:55:29', '2019-11-26 22:55:36'),
(54, 130, 82, 3322, 'درهم اماراتي', 'We', 1, '2019-11-26 23:07:22', '2019-11-26 23:07:26'),
(55, 130, 82, 333, 'درهم اماراتي', 'As', 1, '2019-11-26 23:08:20', '2019-11-26 23:08:25'),
(56, 130, 82, 55, 'دينار كويتي', 'We', 1, '2019-11-26 23:08:43', '2019-11-26 23:08:46'),
(57, 130, 82, 8, 'دينار كويتي', 'Hi', 1, '2019-11-26 23:09:52', '2019-11-26 23:09:57'),
(58, 129, 99, 100, 'ريال سعودي', 'اشتري ب 100', 1, '2020-03-20 03:32:50', '2020-03-20 03:33:24'),
(59, 129, 99, 200, 'ريال سعودي', 'اشتري ب 200', 1, '2020-03-20 03:38:58', '2020-03-20 03:39:20'),
(60, 129, 99, 976453, 'ريال سعودي', 'ةذوذسنن', 1, '2020-03-20 04:03:19', '2020-03-20 04:03:38'),
(61, 129, 100, 646454, 'درهم اماراتي', 'كاش', 1, '2020-03-20 04:06:14', '2020-03-20 04:08:05'),
(62, 130, 99, 500, 'ريال سعودي', 'Hh', 1, '2020-03-20 04:18:22', '2020-03-20 05:23:10'),
(63, 134, 98, 6464, 'درهم اماراتي', 'Nansn', 0, '2020-03-21 00:09:44', '2020-03-21 00:09:44');

-- --------------------------------------------------------

--
-- Table structure for table `assigned_roles`
--

CREATE TABLE `assigned_roles` (
  `id` int(11) NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `entity_id` int(10) UNSIGNED NOT NULL,
  `entity_type` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scope` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assigned_roles`
--

INSERT INTO `assigned_roles` (`id`, `role_id`, `entity_id`, `entity_type`, `scope`) VALUES
(1, 1, 1, 'App\\User', NULL),
(6, 17, 62, 'App\\User', NULL),
(62, 4, 81, 'App\\User', NULL),
(63, 5, 81, 'App\\User', NULL),
(64, 17, 81, 'App\\User', NULL),
(65, 18, 81, 'App\\User', NULL),
(66, 19, 81, 'App\\User', NULL),
(67, 26, 81, 'App\\User', NULL),
(68, 21, 81, 'App\\User', NULL),
(69, 22, 81, 'App\\User', NULL),
(70, 23, 81, 'App\\User', NULL),
(71, 24, 81, 'App\\User', NULL),
(72, 25, 81, 'App\\User', NULL),
(117, 4, 135, 'App\\User', NULL),
(118, 5, 135, 'App\\User', NULL),
(119, 17, 135, 'App\\User', NULL),
(120, 18, 135, 'App\\User', NULL),
(121, 19, 135, 'App\\User', NULL),
(122, 26, 135, 'App\\User', NULL),
(123, 21, 135, 'App\\User', NULL),
(124, 22, 135, 'App\\User', NULL),
(125, 23, 135, 'App\\User', NULL),
(126, 24, 135, 'App\\User', NULL),
(127, 25, 135, 'App\\User', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_number` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iban_number` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `name`, `account_number`, `iban_number`, `image`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'مصرف الإنماء - alinma bank', '068202349613000', 'SA3605000068202349613000', 'https://sell4me.ml/public/files/banks/1577226411.bdv5uk7ncNBSKmcA4Tfjمصرف الانماء.jpg', 1, '2019-03-27 12:43:21', '2019-12-25 01:30:25'),
(2, 'مصرف الراجحي - alrajhi bank', '527608010000184', 'SA2780000527608010000184', 'https://sell4me.ml/public/files/banks/1563557024.9KPyf6w9OqG5f5YD0Ng1الراجحي.png', 1, '2019-07-20 02:23:44', '2019-12-25 01:31:34');

-- --------------------------------------------------------

--
-- Table structure for table `bank_transfers`
--

CREATE TABLE `bank_transfers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `userName` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `userAccount` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `iban` varchar(191) CHARACTER SET utf8 DEFAULT NULL,
  `money` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blockes`
--

CREATE TABLE `blockes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `blocker_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `is_suspend` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `image`, `parent_id`, `is_suspend`, `created_at`, `updated_at`) VALUES
(1, 'العقارات', NULL, 0, 0, '2019-03-26 22:00:00', '2019-12-22 01:44:00'),
(3, 'محلات', NULL, 1, 0, '2019-03-27 13:36:40', '2019-12-22 01:37:44'),
(4, 'ال', 'http://localhost/Advertisement/public/files/categories/1553694100.qXxJpZsAD9gc1BMbuhjjCategories.gif', 0, 1, '2019-03-27 13:41:40', '2019-11-27 23:58:32'),
(5, 'شقق', NULL, 1, 0, '2019-03-27 13:45:47', '2019-12-22 01:37:05'),
(6, 'ابراج', NULL, 1, 0, '2019-03-27 13:36:40', '2019-12-22 01:38:02'),
(7, 'عماره', NULL, 1, 0, '2019-03-27 13:45:47', '2019-12-22 01:37:25'),
(8, 'مزارع', NULL, 1, 0, '2019-07-24 06:19:40', '2019-12-22 01:36:39'),
(9, 'حيوانات', NULL, 0, 0, '2019-07-24 06:36:19', '2019-11-27 23:57:58'),
(10, 'جوالات', NULL, 0, 0, '2019-07-24 06:38:31', '2019-11-27 23:57:42'),
(11, 'سيارات', NULL, 0, 0, '2019-07-24 06:39:44', '2019-11-27 23:57:10'),
(12, 'اثاث', NULL, 0, 0, '2019-09-28 15:26:43', '2019-12-22 01:31:06'),
(13, 'الكل', NULL, 0, 1, '2019-11-12 14:50:42', '2019-11-12 14:50:42'),
(14, 'أراضي للبيع', 'http://sell4me.ml/public/files/categories/1574104878.QqdB80LBU6JBUv2WIUnfbg-7.jpg', 12, 1, '2019-11-18 22:21:18', '2019-11-27 23:40:37'),
(15, 'ارض خام', NULL, 1, 0, '2019-11-26 20:39:09', '2019-12-22 01:36:16'),
(16, 'قصور', NULL, 1, 0, '2019-11-27 23:45:00', '2019-12-22 01:35:46'),
(17, 'استراحات', NULL, 1, 0, '2019-11-27 23:45:28', '2019-12-22 01:35:10'),
(18, 'مجمعات', NULL, 1, 0, '2019-11-27 23:47:13', '2019-12-22 01:34:51'),
(19, 'غرف عزاب', NULL, 1, 0, '2019-11-27 23:49:34', '2019-12-22 01:34:26'),
(20, 'مستودعات', NULL, 1, 0, '2019-11-27 23:53:07', '2019-12-22 01:33:42'),
(21, 'اراضي', NULL, 1, 0, '2019-11-27 23:53:39', '2019-12-22 01:33:05'),
(22, 'فلل', NULL, 1, 0, '2019-11-27 23:54:18', '2019-12-22 01:32:13'),
(23, 'الكترونيات', NULL, 0, 0, '2019-11-28 00:00:41', '2019-12-08 01:09:27'),
(24, 'تويوتا', NULL, 11, 0, '2019-11-28 00:04:59', '2019-11-28 00:04:59'),
(25, 'هيونداي', NULL, 11, 0, '2019-11-28 00:05:22', '2019-11-28 00:05:22'),
(26, 'فورد', NULL, 11, 0, '2019-11-28 00:05:40', '2019-11-28 00:05:40'),
(27, 'شيفروليه', NULL, 11, 0, '2019-11-28 00:06:16', '2019-11-28 00:06:16'),
(28, 'نيسان', NULL, 11, 0, '2019-11-28 00:06:31', '2019-11-28 00:06:31'),
(29, 'مرسيدس', NULL, 11, 0, '2019-11-28 00:06:53', '2019-11-28 00:06:53'),
(30, 'كيا', NULL, 11, 0, '2019-11-28 00:07:09', '2019-11-28 00:07:09'),
(31, 'لكزس', NULL, 11, 0, '2019-11-28 00:07:29', '2019-11-28 00:07:29'),
(32, 'جي إم سي', NULL, 11, 0, '2019-11-28 00:07:57', '2019-11-28 00:07:57'),
(33, 'هوندا', NULL, 11, 0, '2019-11-28 00:08:16', '2019-11-28 00:08:16'),
(34, 'دودج', NULL, 11, 0, '2019-11-28 00:08:39', '2019-11-28 00:08:39'),
(35, 'ميتسوبيشي', NULL, 11, 0, '2019-11-28 00:09:03', '2019-11-28 00:09:03'),
(36, 'بي ام دبليو', NULL, 11, 0, '2019-11-28 00:09:32', '2019-11-28 00:09:32'),
(37, 'مازدا', NULL, 11, 0, '2019-11-28 00:09:50', '2019-11-28 00:09:50'),
(38, 'ايسوزو', NULL, 11, 0, '2019-11-28 00:10:08', '2019-11-28 00:10:08'),
(39, 'جيب', NULL, 11, 0, '2019-11-28 00:10:23', '2019-11-28 00:10:23'),
(40, 'كرايسلر', NULL, 11, 0, '2019-11-28 00:10:40', '2019-11-28 00:10:40'),
(41, 'لاند روفر', NULL, 11, 0, '2019-11-28 00:11:00', '2019-11-28 00:11:00'),
(42, 'شانجان', NULL, 11, 0, '2019-11-28 00:11:15', '2019-11-28 00:11:15'),
(43, 'اودي', NULL, 11, 0, '2019-11-28 00:11:31', '2019-11-28 00:11:31'),
(44, 'كاديلاك', NULL, 11, 0, '2019-11-28 00:11:47', '2019-11-28 00:11:47'),
(45, 'رينو', NULL, 11, 0, '2019-11-28 00:12:00', '2019-11-28 00:12:00'),
(46, 'بورش', NULL, 11, 0, '2019-11-28 00:12:28', '2019-11-28 00:12:28'),
(47, 'جيلي', NULL, 11, 0, '2019-11-28 00:12:41', '2019-11-28 00:12:41'),
(48, 'ديهاتسو', NULL, 11, 0, '2019-11-28 00:13:06', '2019-11-28 00:13:06'),
(49, 'سوزوكي', NULL, 11, 0, '2019-11-28 00:13:40', '2019-11-28 00:13:40'),
(50, 'لينكون', NULL, 11, 0, '2019-11-28 00:13:55', '2019-11-28 00:13:55'),
(51, 'ميركوري', NULL, 11, 0, '2019-11-28 00:14:13', '2019-11-28 00:14:13'),
(52, 'فولكسفاغن', NULL, 11, 0, '2019-11-28 00:14:57', '2019-11-28 00:14:57'),
(53, 'انفنتي', NULL, 11, 0, '2019-11-28 00:15:20', '2019-11-28 00:15:20'),
(54, 'إم جي', NULL, 11, 0, '2019-11-28 00:15:41', '2019-11-28 00:15:41'),
(55, 'بنتلي', NULL, 11, 0, '2019-11-28 00:15:56', '2019-11-28 00:15:56'),
(56, 'بيجو', NULL, 11, 0, '2019-11-28 00:16:10', '2019-11-28 00:16:10'),
(57, 'فولفو', NULL, 11, 0, '2019-11-28 00:16:24', '2019-11-28 00:16:24'),
(58, 'همر', NULL, 11, 0, '2019-11-28 00:16:37', '2019-11-28 00:16:37'),
(59, 'جاغوار', NULL, 11, 0, '2019-11-28 00:16:53', '2019-11-28 00:16:53'),
(60, 'هافال', NULL, 11, 0, '2019-11-28 00:17:07', '2019-11-28 00:17:07'),
(61, 'رولز رويس', NULL, 11, 0, '2019-11-28 00:17:37', '2019-11-28 00:17:37'),
(62, 'مازيراتي', NULL, 11, 0, '2019-11-28 00:18:06', '2019-11-28 00:18:06'),
(63, 'فيات', NULL, 11, 0, '2019-11-28 00:18:21', '2019-11-28 00:18:21'),
(64, 'ميني', NULL, 11, 0, '2019-11-28 00:18:34', '2019-11-28 00:18:34'),
(65, 'شيري', NULL, 11, 0, '2019-11-28 00:18:48', '2019-11-28 00:18:48'),
(66, 'كلاسيكية', NULL, 11, 0, '2019-11-28 00:19:16', '2019-11-28 00:19:16'),
(67, 'جي أيه سي', NULL, 11, 0, '2019-11-28 00:19:40', '2019-11-28 00:22:38'),
(68, 'جريت وول', NULL, 11, 0, '2019-11-28 00:20:03', '2019-11-28 00:20:03'),
(69, 'سوبارو', NULL, 11, 0, '2019-11-28 00:20:47', '2019-11-28 00:20:47'),
(70, 'ليفان', NULL, 11, 0, '2019-11-28 00:21:11', '2019-11-28 00:21:11'),
(71, 'فاو', NULL, 11, 0, '2019-11-28 00:21:29', '2019-11-28 00:21:29'),
(72, 'ستيروين', NULL, 11, 0, '2019-11-28 00:23:24', '2019-11-28 00:23:24'),
(73, 'استون مارتن', NULL, 11, 0, '2019-11-28 00:23:49', '2019-11-28 00:23:49'),
(74, 'لامبورغيني', NULL, 11, 0, '2019-11-28 00:24:23', '2019-11-28 00:24:23'),
(75, 'فوتون', NULL, 11, 0, '2019-11-28 00:24:39', '2019-11-28 00:24:39'),
(76, 'اوبل', NULL, 11, 0, '2019-11-28 00:25:40', '2019-11-28 00:25:40'),
(77, 'ماكلارين', NULL, 11, 0, '2019-11-28 00:26:08', '2019-11-28 00:26:08'),
(78, 'وظائف', NULL, 0, 0, '2019-11-28 00:27:03', '2019-11-28 00:27:03'),
(79, 'ملابس', NULL, 0, 0, '2019-11-28 00:27:22', '2019-11-28 00:27:22'),
(80, 'لوحات مميزة', NULL, 0, 0, '2019-11-28 00:28:56', '2019-11-28 00:28:56'),
(81, 'ارقام مميزة', NULL, 0, 0, '2019-11-28 00:29:22', '2019-11-28 00:29:22'),
(82, 'تقسيط', NULL, 0, 0, '2019-11-28 00:30:06', '2019-11-28 00:30:06'),
(83, 'اخرى', NULL, 11, 0, '2019-11-28 00:33:10', '2019-11-28 00:33:10'),
(84, 'درجات و دبابات', NULL, 0, 0, '2019-11-28 00:34:15', '2019-11-28 00:34:15'),
(85, 'ساعات', NULL, 0, 0, '2019-11-28 00:35:45', '2019-11-28 00:35:45'),
(86, 'اخرى', NULL, 0, 1, '2019-11-28 00:36:12', '2020-03-14 21:29:30'),
(87, 'خدمات', NULL, 0, 0, '2019-11-28 00:36:34', '2019-11-28 00:36:34'),
(88, 'جمال', NULL, 9, 0, '2020-03-14 21:17:14', '2020-03-14 22:12:12'),
(89, 'ايفون', NULL, 10, 0, '2020-03-14 21:17:28', '2020-03-14 22:09:51'),
(90, 'كنب', NULL, 12, 0, '2020-03-14 21:17:43', '2020-03-14 22:07:01'),
(91, 'الكترونيات المنزل', NULL, 23, 0, '2020-03-14 21:18:06', '2020-03-14 22:06:33'),
(92, 'جميع الوظائف', NULL, 78, 0, '2020-03-14 21:18:17', '2020-03-14 22:04:55'),
(93, 'رجالي', NULL, 79, 0, '2020-03-14 21:18:30', '2020-03-14 22:01:03'),
(94, 'لوحات سيارات', NULL, 80, 0, '2020-03-14 21:18:48', '2020-03-14 22:00:33'),
(95, 'ارقام مميزة', NULL, 81, 0, '2020-03-14 21:19:02', '2020-03-14 21:39:34'),
(96, 'تقسيط سيارات', NULL, 82, 0, '2020-03-14 21:19:17', '2020-03-14 21:32:44'),
(97, 'درجات', NULL, 84, 0, '2020-03-14 21:19:28', '2020-03-14 21:32:04'),
(98, 'ساعات فاخره', NULL, 85, 0, '2020-03-14 21:19:47', '2020-03-14 21:31:14'),
(99, 'الكل', NULL, 86, 1, '2020-03-14 21:20:00', '2020-03-14 21:30:16'),
(100, 'بناء و مقاولات', NULL, 87, 0, '2020-03-14 21:20:12', '2020-03-14 21:27:10'),
(101, 'نقل عفش', NULL, 87, 0, '2020-03-14 21:27:24', '2020-03-14 21:27:24'),
(102, 'خدمات الحدائق', NULL, 87, 0, '2020-03-14 21:28:09', '2020-03-14 21:28:09'),
(103, 'دعاية و تسويق', NULL, 87, 0, '2020-03-14 21:28:42', '2020-03-14 21:28:42'),
(104, 'دبابات', NULL, 84, 0, '2020-03-14 21:32:19', '2020-03-14 21:32:19'),
(105, 'نسائي', NULL, 79, 0, '2020-03-14 22:01:16', '2020-03-14 22:01:16'),
(106, 'اطفال', NULL, 79, 0, '2020-03-14 22:01:33', '2020-03-14 22:01:33'),
(107, 'غرف نوم', NULL, 12, 0, '2020-03-14 22:07:16', '2020-03-14 22:07:16'),
(108, 'مطابخ', NULL, 12, 0, '2020-03-14 22:07:31', '2020-03-14 22:07:31'),
(109, 'طاولة طعام', NULL, 12, 0, '2020-03-14 22:08:22', '2020-03-14 22:08:22'),
(110, 'تحف', NULL, 12, 0, '2020-03-14 22:08:36', '2020-03-14 22:08:36'),
(111, 'انارات', NULL, 12, 0, '2020-03-14 22:08:52', '2020-03-14 22:08:52'),
(112, 'اثاث مكتبي', NULL, 12, 0, '2020-03-14 22:09:13', '2020-03-14 22:09:13'),
(113, 'سامسونج', NULL, 10, 0, '2020-03-14 22:10:44', '2020-03-14 22:10:44'),
(114, 'هواوي', NULL, 10, 0, '2020-03-14 22:11:03', '2020-03-14 22:11:03'),
(115, 'لابتوب', NULL, 23, 0, '2020-03-14 22:11:45', '2020-03-14 22:11:45'),
(116, 'خيل', NULL, 9, 0, '2020-03-14 22:12:43', '2020-03-14 22:12:43'),
(117, 'صقور', NULL, 9, 0, '2020-03-14 22:12:57', '2020-03-14 22:12:57'),
(118, 'طيور', NULL, 9, 0, '2020-03-14 22:13:11', '2020-03-14 22:13:11'),
(119, 'ببغاء', NULL, 9, 0, '2020-03-14 22:13:25', '2020-03-14 22:13:25'),
(120, 'كلاب', NULL, 9, 0, '2020-03-14 22:13:38', '2020-03-14 22:13:38'),
(121, 'قطط', NULL, 9, 0, '2020-03-14 22:14:04', '2020-03-14 22:14:04'),
(122, 'اخرى', NULL, 9, 0, '2020-03-14 22:14:25', '2020-03-14 22:14:25');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(10) UNSIGNED NOT NULL,
  `ad_id` int(10) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `parent_id`, `user_id`, `ad_id`, `comment`, `created_at`, `updated_at`) VALUES
(218, 0, 131, 82, 'New', '2019-11-20 20:03:15', '2019-11-20 20:03:15'),
(221, 0, 133, 78, 'kjnkjnjknjjb', '2019-11-25 18:03:09', '2019-11-25 18:03:09');

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE `conversations` (
  `id` int(10) UNSIGNED NOT NULL,
  `ad_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `conversations`
--

INSERT INTO `conversations` (`id`, `ad_id`, `created_at`, `updated_at`) VALUES
(1, 77, '2019-11-18 22:29:39', '2019-11-18 22:29:39'),
(2, 76, '2019-11-18 22:41:29', '2019-11-26 20:26:12'),
(3, 78, '2019-11-19 23:39:53', '2019-11-19 23:39:53'),
(4, 80, '2019-11-20 16:52:11', '2019-11-24 18:53:40'),
(5, 82, '2019-11-20 18:58:17', '2019-11-20 18:58:17'),
(6, 79, '2019-11-23 17:08:40', '2019-11-23 17:08:40'),
(7, 79, '2019-11-23 17:17:56', '2019-11-23 17:17:56'),
(8, 83, '2019-11-23 17:23:14', '2019-11-23 17:23:14'),
(9, 83, '2019-11-23 17:29:22', '2019-11-23 17:29:22'),
(10, 76, '2019-11-23 17:48:06', '2019-11-24 19:41:55'),
(11, 80, '2019-11-24 20:17:03', '2019-11-24 20:17:03'),
(12, 83, '2019-11-26 20:16:30', '2019-11-26 20:16:30'),
(13, 91, '2020-02-11 00:50:08', '2020-02-11 11:21:23'),
(14, 99, '2020-03-20 03:33:37', '2020-03-20 03:33:37'),
(15, 99, '2020-03-20 03:35:15', '2020-03-20 04:05:15'),
(16, 100, '2020-03-20 04:06:31', '2020-03-20 04:07:56'),
(17, 99, '2020-03-20 04:17:55', '2020-03-20 05:23:25'),
(18, 98, '2020-03-21 00:10:33', '2020-03-21 00:10:53');

-- --------------------------------------------------------

--
-- Table structure for table `conversation_user`
--

CREATE TABLE `conversation_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `conversation_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `read_at` datetime DEFAULT NULL,
  `is_online` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `conversation_user`
--

INSERT INTO `conversation_user` (`user_id`, `conversation_id`, `deleted_at`, `read_at`, `is_online`, `created_at`, `updated_at`) VALUES
(129, 1, NULL, '2020-02-18 11:44:34', 0, NULL, NULL),
(129, 2, NULL, '2020-02-18 11:46:14', 0, NULL, NULL),
(129, 8, NULL, '2020-02-18 11:52:17', 0, NULL, NULL),
(129, 9, NULL, '2019-12-01 21:46:19', 0, NULL, NULL),
(129, 12, NULL, '2020-03-20 03:45:52', 0, NULL, NULL),
(129, 13, NULL, '2020-02-18 12:01:40', 0, NULL, NULL),
(129, 15, NULL, NULL, 0, NULL, NULL),
(129, 16, NULL, '2020-03-21 00:10:25', 0, NULL, NULL),
(129, 18, NULL, '2020-03-21 00:10:53', 0, NULL, NULL),
(130, 1, NULL, '2019-11-19 14:29:58', 0, NULL, NULL),
(130, 2, NULL, '2019-11-26 20:26:44', 0, NULL, NULL),
(130, 10, NULL, NULL, 0, NULL, NULL),
(130, 12, NULL, '2020-02-11 00:46:22', 0, NULL, NULL),
(130, 16, NULL, '2020-03-20 04:08:06', 0, NULL, NULL),
(130, 17, NULL, NULL, 0, NULL, NULL),
(131, 3, NULL, '2019-11-19 23:50:12', 0, NULL, NULL),
(131, 4, NULL, NULL, 0, NULL, NULL),
(131, 5, NULL, '2019-11-23 14:42:30', 0, NULL, NULL),
(131, 6, NULL, NULL, 0, NULL, NULL),
(132, 5, NULL, '2019-11-21 02:12:55', 0, NULL, NULL),
(132, 7, NULL, NULL, 0, NULL, NULL),
(132, 9, NULL, '2019-11-23 17:30:31', 0, NULL, NULL),
(132, 11, NULL, NULL, 0, NULL, NULL),
(133, 3, NULL, '2019-11-23 15:40:41', 0, NULL, NULL),
(133, 4, NULL, '2019-12-09 13:14:43', 0, NULL, NULL),
(133, 6, NULL, '2019-11-23 17:44:08', 0, NULL, NULL),
(133, 7, NULL, '2019-11-23 17:26:50', 0, NULL, NULL),
(133, 8, NULL, NULL, 0, NULL, NULL),
(133, 10, NULL, '2019-12-09 13:13:41', 0, NULL, NULL),
(133, 11, NULL, '2019-12-09 00:02:10', 0, NULL, NULL),
(134, 18, NULL, NULL, 0, NULL, NULL),
(137, 13, NULL, '2020-02-21 00:32:43', 0, NULL, NULL),
(141, 15, NULL, '2020-03-26 21:09:42', 0, NULL, NULL),
(141, 17, NULL, '2020-03-20 05:23:25', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `counteries`
--

CREATE TABLE `counteries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `counteries`
--

INSERT INTO `counteries` (`id`, `name`, `parent_id`, `image`, `key_city`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'مصر', 0, 'https://sell4me.ml/public/files/helpAdmin/1563913886.xTcCYcp6j5z34AqYZcwbمصر.png', '+20', 0, '2019-03-27 11:43:47', '2019-12-24 18:53:47'),
(2, 'الرياض', 3, 'http://sell4me.ml/public/files/helpAdmin/1563312581.ZBi3aVXS3cgXGMHVyX3i002-saudi-arabia.png', '0', 0, '2019-03-27 11:43:58', '2019-07-17 06:29:41'),
(3, 'السعودية', 0, 'https://sell4me.ml/public/files/helpAdmin/1563914500.Pr4iwCZJh2xEIoQr8puKالسعودية.png', '+966', 0, '2019-03-27 11:43:47', '2020-01-08 16:05:26'),
(4, 'الإمارات', 0, 'https://sell4me.ml/public/files/helpAdmin/1563914096.gQRsmKKFzO3chRhQu0utالامارات.png', '+971', 0, '2019-07-17 05:50:04', '2019-12-24 18:53:49'),
(5, 'جده', 3, 'http://sell4me.ml/public/files/helpAdmin/1563312616.XAfU0W94rmmfW1zLip6C003-united-arab-emirates.png', NULL, 0, '2019-07-17 05:50:04', '2019-10-29 17:46:23'),
(6, 'الكويت', 0, 'https://sell4me.ml/public/files/helpAdmin/1563914279.vSxqseYZZ3lMhR0EZrfdالكويت.png', '+965', 0, '2019-07-17 06:24:33', '2019-12-24 18:53:51'),
(7, 'البحرين', 0, 'https://sell4me.ml/public/files/helpAdmin/1563913954.woYT69XMmwinNZHIw89Sالبحرين.png', '+973', 0, '2019-07-17 06:25:00', '2019-12-24 18:52:25'),
(8, 'أبها', 3, 'https://sell4me.ml/public/files/helpAdmin/1566584358.DbXNMTIpZuY4ORkCzYxJالبحرين.png', NULL, 0, '2019-07-17 06:30:40', '2019-10-29 17:45:51'),
(9, 'عمان', 0, 'https://sell4me.ml/public/files/helpAdmin/1563914427.G2PwYVPDKb4XGKojEvnFعمان.jpg', '+968', 0, '2019-07-24 05:40:27', '2019-12-24 18:53:54'),
(10, 'الاردن', 0, 'https://sell4me.ml/public/files/helpAdmin/1563914621.vheRcxVYRieFVcPFCYGCالاردن.png', '+962', 0, '2019-07-24 05:43:41', '2019-12-24 18:53:56'),
(11, 'المغرب', 0, 'https://sell4me.ml/public/files/helpAdmin/1563914653.W5kvNX5gIeHnTZ4d4Wxkالمغرب.png', '+212', 0, '2019-07-24 05:44:13', '2019-12-24 18:53:59'),
(12, 'اليمن', 0, 'https://sell4me.ml/public/files/helpAdmin/1563914726.Qj0YS8u5gk2ftAvirZicاليمن.png', '+967', 0, '2019-07-24 05:45:26', '2019-12-24 18:54:03'),
(13, 'سوريا', 0, 'https://sell4me.ml/public/files/helpAdmin/1563914818.U8alg2WSuBJnHTUWjMfoسوريا.png', '+963', 0, '2019-07-24 05:46:58', '2019-12-24 18:54:07'),
(14, 'ليبيا', 0, 'https://sell4me.ml/public/files/helpAdmin/1563914918.4EFz2otv0T819CWwkYbVليبيا.png', '+218', 0, '2019-07-24 05:48:39', '2019-12-24 18:54:19'),
(15, 'لبنان', 0, 'https://sell4me.ml/public/files/helpAdmin/1563915023.Qvy7sdgdAasxH3uEamoyلبنان.png', '+961', 0, '2019-07-24 05:50:23', '2019-12-24 18:54:17'),
(16, 'الجزائر', 0, 'https://sell4me.ml/public/files/helpAdmin/1563915099.PcH4Mgdt4jmkaOSrwnFAالجزائر.png', '+213', 0, '2019-07-24 05:51:39', '2019-12-24 18:54:15'),
(17, 'العراق', 0, 'https://sell4me.ml/public/files/helpAdmin/1563915199.Qz07XuGmI0c5n8wXw9xGالعراق.png', '+964', 0, '2019-07-24 05:53:19', '2019-12-24 18:54:13'),
(18, 'مكة المكرمة', 3, NULL, NULL, 0, '2019-07-24 06:13:48', '2019-10-29 17:44:30'),
(31, 'تبوك', 3, NULL, NULL, 0, '2019-10-29 17:44:55', '2019-12-24 19:03:55'),
(32, 'المدينة المنورة', 3, NULL, '+', 0, '2019-10-29 17:45:14', '2019-10-29 17:45:14'),
(35, 'الشرقية', 3, NULL, '+', 0, '2019-12-24 19:04:23', '2019-12-24 19:04:23'),
(36, 'القصيم', 3, NULL, '+', 0, '2019-12-24 19:04:42', '2019-12-24 19:04:42'),
(37, 'ابها', 3, NULL, '+', 0, '2019-12-24 19:05:43', '2019-12-24 19:05:43'),
(38, 'حائل', 3, NULL, '+', 0, '2019-12-24 19:05:57', '2019-12-24 19:05:57'),
(39, 'ينبع', 3, NULL, '+', 0, '2019-12-24 19:06:13', '2019-12-24 19:06:13'),
(40, 'حفر الباطن', 3, NULL, '+', 0, '2019-12-24 19:06:28', '2019-12-24 19:06:28'),
(41, 'الطائف', 3, NULL, '+', 0, '2019-12-24 19:06:44', '2019-12-24 19:06:44'),
(42, 'الباحة', 3, NULL, '+', 0, '2019-12-24 19:07:14', '2019-12-24 19:07:14'),
(43, 'جيزان', 3, NULL, '+', 0, '2019-12-24 19:07:34', '2019-12-24 19:07:34'),
(44, 'نجران', 3, NULL, '+', 0, '2019-12-24 19:07:50', '2019-12-24 19:07:50'),
(45, 'الجوف', 3, NULL, '+', 0, '2019-12-24 19:08:12', '2019-12-24 19:08:12'),
(46, 'عرعر', 3, NULL, '+', 0, '2019-12-24 19:08:28', '2019-12-24 19:08:28'),
(60, 'جميع المدن', 14, NULL, '+', 0, '2020-01-08 16:09:20', '2020-01-11 21:53:11'),
(61, 'جميع المدن', 1, NULL, '+', 0, '2020-01-08 16:09:55', '2020-01-11 21:52:53'),
(62, 'جميع المدن', 17, NULL, '+', 0, '2020-01-11 21:58:57', '2020-01-11 21:58:57'),
(63, 'جميع المدن', 16, NULL, '+', 0, '2020-01-11 21:59:19', '2020-01-11 21:59:19'),
(64, 'جميع المدن', 15, NULL, '+', 0, '2020-01-11 21:59:40', '2020-01-11 21:59:40'),
(65, 'جميع المدن', 13, NULL, '+', 0, '2020-01-11 21:59:54', '2020-01-11 21:59:54'),
(66, 'جميع المدن', 12, NULL, '+', 0, '2020-01-11 22:00:17', '2020-01-11 22:00:17'),
(67, 'جميع المدن', 11, NULL, '+', 0, '2020-01-11 22:00:28', '2020-01-11 22:00:28'),
(68, 'جميع المدن', 10, NULL, '+', 0, '2020-01-11 22:00:42', '2020-01-11 22:00:42'),
(69, 'جميع المدن', 9, NULL, '+', 0, '2020-01-11 22:00:58', '2020-01-11 22:00:58'),
(70, 'جميع المدن', 7, NULL, '+', 0, '2020-01-11 22:01:11', '2020-01-11 22:01:11'),
(71, 'جميع المدن', 6, NULL, '+', 0, '2020-01-11 22:01:24', '2020-01-11 22:01:24'),
(72, 'جميع المدن', 4, NULL, '+', 0, '2020-01-11 22:01:40', '2020-01-11 22:01:40');

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `device` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`id`, `user_id`, `device`, `device_type`, `created_at`, `updated_at`) VALUES
(99, 1, 'eLFTjLKFN-w:APA91bHw7Q85NTt04IaLRuVrf0sOVutZ7Qn6Z7vHNZSqbB5dSPZRmJfyM67it_zpgo3ZSlqev3JJgoL7ofZ_kWmWvc2xJTUCXNUmRTCjFQJC0_-D7ynY5qpzbrqT8i_Q9ycCJUP59WRu', 'web', '2019-09-23 17:10:44', '2019-09-23 17:10:44'),
(100, 1, 'fQiMh1KnWOo:APA91bEiGUgtYt7uo-cjOxjS3GkVP2jryOe9CZofhQlXsAYfsAl1PWRY7gnhZBdRySviSs1dr39c3GMUqmBoTz2KWoYF0E_6avLjSjeOOzQURU_yDCh4JuVV0V35CgNWXF6DtJI4maVs', 'web', '2019-09-24 11:10:53', '2019-09-24 11:10:53'),
(103, 1, 'fVB7A-gBoVc:APA91bHfy7jI8tRCKLHtFDfbxw9SRlnOL-lBb_97yIuVl_1K2_ff4fVflVVGK_gP-8-5xB8a9OJ1hH9wGisqRqTXN1_ASGUpq8cUM0O9BhN3sszX7zwmv1-kvHquk64ta47Lv1BT_n28', 'web', '2019-09-28 01:08:15', '2019-09-28 01:08:15'),
(105, 1, 'dUsrwEa80sE:APA91bEUHdtRkvyGDWhjaoncyylsiSuII7MU8zR3ftePDJe87F_FAG8MX-zgwb2u6StUaPwopFkNGIsAE6UqK019SO6z9_IhHHtp1d9cHeZmL4jYZdHP4Ky0xoGh_pUzEFGYiPVLhCQD', 'web', '2019-10-21 15:24:36', '2019-10-21 15:24:36'),
(117, 1, 'e1I8naXh-iE:APA91bHMTS26NECsyQ2W7WTztJ7m-Omndm1VbfulJxzDPZWoub4gt1gA1VxC9TZUXS_hG6WCRIpwq9IgtXyp0Wzuzr4uwa6yN0fJi2xkAVsD6RRUeV2Lye0Rq4a49us9CkKWY5ectfLR', 'web', '2019-10-29 17:13:20', '2019-10-29 17:13:20'),
(127, 133, 'eNG8mBpe3hk:APA91bHavhA8BoSVuOnnjgkLbQESuBnmjArYdg6t4swOZn17IIrir3Ks8gN4qf9Z0exYPW6EcUmxN_v29jaQ5Iy-XGQHGeXEBjfbfuwAZPQcCz4-zJyzccC3t9oKjNQIDX7ww3xTFahk', 'android', '2019-11-19 15:52:52', '2019-11-23 15:35:43'),
(128, 131, 'cg6IxN1JnY0:APA91bFsE9mR8jMqWdUb6w8_5G9PKKngG0BxhO8fYNO9PXs3-A7LOJx2DLKxqUtUJw-K-L4s5Q0D806laEt2o4w8GH_rNyfKTilAoJ_Vl6ArgI1VFxZPv7mIx1U2hdtUmUv_acZ-d9lt', 'android', '2019-11-19 17:01:51', '2019-11-19 17:01:51'),
(129, 130, 'cKkOlzYJi18:APA91bHbRHdursTrNjvV6wSG101RIrXoMEmw_bbDg3u9MoyJxfjouY6i9ggAwYIFeF5rFA_W4lRm0bvVW30dOe3wAAomPcRci5e6xEUfn96hEqvCF9ikH84DFskGNOO61tLd7aF4ypub', 'Ios', '2019-11-19 17:02:50', '2019-11-19 17:02:50'),
(130, 131, 'cdP7vq3YB2M:APA91bH3suBKtpkIwPqOgfp7ZdcK6TkFQsN4bzovGDfF9tb6nKdmte5h7XxTovdA_LvuyR4nduOhwrXBiXuV7BzIKtauNBXkRL9qzedB77OmyJjtDThQTTlFbS5QtWnjVjlYNp1tj9L7', 'Ios', '2019-11-19 17:17:07', '2019-11-19 17:17:07'),
(136, 130, 'eb2zDOy_rac:APA91bHIU40XELaJ2MWaCD_-o2T5hvNRPoPTPjTqN-QF-fIzojvfw_ol5AzIkYDyuEizGzh78x_VOnvdRzSe1EMFGx4szVudvFAHf6FXpp8FvKdi_soqZkt4u7mwt4yo_z-ZlbATCd3b', 'Ios', '2019-11-21 14:38:35', '2019-11-21 14:38:35'),
(141, 131, 'fBB-pmCQcYg:APA91bFDCqrita7qn_9LMVg21KARs3cpi-RXjLI8cnODNE9FTGn7m8zJesD6Q8D_G5WCrEJQjwCdUZD1lhL8c393OKe9Wnk8yrHjXukl5Wh20eaFLeV4J_rQosac-esEMvgMb0-EmNFm', 'android', '2019-11-24 21:11:35', '2019-11-24 21:11:35'),
(143, 132, 'cnbSaXZq6Rc:APA91bFsbhh6FIz1ewFZ78gj1w61Kc4tUJ6Or1673LEa-U5TZbXhhbi5Lgm-d3DKhb49ELMPh4hlwXSgVK7Yv9Fz6DHvrBvIC0KjQ-O2uaAgG7tPpvL1cvmsM1WWWUJqDnPzKKaXxShn', 'Ios', '2019-11-26 00:39:18', '2019-11-26 00:39:18'),
(148, 129, 'fDz4xCjtXjQ:APA91bFz2MWZ9wocXQR0iPqTO5i9n8K1RuwU1GUvVg7nxnCYwnyvj5Mp8rQRZmp-XuTZHd8qDUpCdWQtLOPnfjFUDJs9yx6zF9NohrwRsjBAP5e2aH-kcwfCgRGKdloaPSr0UOI_BgoE', 'Ios', '2019-11-26 20:24:13', '2019-11-26 20:24:13'),
(151, 129, 'eoZoE9MQg10:APA91bGKsno06PJrSEq51n_SxDoXm08x9x-RCmVXfGajMgbYHJhMQJJhOsTpagLargahjFRZjrl0nAnTUm_WgPRx9JFgho2dXq5LZoCe2xkLuOILrhiwP_sRFeJmqFY3Dw7IxAXTi5oE', 'Ios', '2019-11-27 23:07:41', '2019-11-27 23:07:41'),
(152, 130, 'fxlWUtY4ZgU:APA91bHAhhh4ahxwyglERjkuP6kZCvD089rLgG1W--R7yUOem23qRRe16LosEhLMLsaeLj7CZ8XfmFkudYo2he7_i0ADjPK8zCdpdJzAm601jIAkbalWW4crHVqKKC9DAZg8Fooam28f', 'Ios', '2019-11-28 10:39:00', '2019-11-28 10:39:00'),
(153, 129, 'cTLaVXHWuFc:APA91bFlLYcXdf0zeM2mEp27E0k8D1YnSbf62vR0Gigv0tXerg1nHKO8OmStsyJefyjnvaIt8NRm1aFAkfj_T2COKvdVzn-UHJ3pFhvx1s7j5AyoWKLHkbwhrH7PGluX13Dbu09_uhPx', 'Ios', '2019-12-07 22:49:49', '2019-12-07 22:49:49'),
(154, 130, 'eC25bVtnUNc:APA91bGWm-V29POJFZrCcfYR18g8C2Yl2GyiHW3qgAprBNQCq0yEohB9x38piHiDgk4LRIMZflExLl-4tUvtr_J-uCaeE-bdqjqKG8b-SwgvQqgtcmcLRqNEO6QV1XIV3uKRWqtfKr7Y', 'Ios', '2019-12-08 17:32:44', '2019-12-08 17:32:44'),
(156, 130, 'cIatD0-SbIQ:APA91bFKO1MeDgz5KwLJu_pxtb2yNDk2ZwIqTMo1kaLP2xF4lgdfqSj-aBd6wqg03753IIVFnMtRFqBhucB6Mnk0DRfLcJUFOvQe310qDufSpfr49FfqAOe3Wxfb-sssgmRqoiYPGJnL', 'android', '2019-12-22 17:41:00', '2019-12-22 17:41:00'),
(157, 129, 'dtLb1v4dMRU:APA91bFXv9HYdqESEMXpvEowJ_W-qU-t3a_Kx9pmLxv9EAbNqwninI3bKbyXHw6Mrj_m_jFCbMQbUa3Hn2NQvFpRYV1SBSIAxpgymukysA73woPoP4ZEKTdU7yzjCV9PsRNBjaNnLeDi', 'Ios', '2019-12-22 19:17:43', '2019-12-22 19:17:43'),
(158, 136, 'ftbGuKTlSYo:APA91bGnjGpbNMpY4BTRf7IhBomayXQOEXzNBgb88GuUkIx1HdOA3U-BgtEcsgw9qLhM5rJlK960DPPxAoOi3FcTLMZRZX_ufFjM4mRQPyEXEbabUPnSuaiNLWoXEPUpr1tNX1jCF8w5', 'Ios', '2019-12-23 21:48:05', '2019-12-23 21:48:05'),
(159, 129, 'cvSowqiYxEY:APA91bEe8RAlnpk9hCwI_0FLYlmkJKjA2CgjnX7xyNT7CnIbx37ZMnGg7gMK4iBj9Sy5mTeKhMOGK7joFLmim95Q_x4fhTRUMLvbSDf3a6z4jx9NfGrsNcLnkr_PuFjF19Nkf1l6HlLy', 'Ios', '2019-12-23 22:53:51', '2019-12-23 22:53:51'),
(160, 136, 'cLh7-Bpgus4:APA91bHbmTyXRSiRSLD0LQD0gN8PnsieQcmH9NudOzmSdkk7u0Qu66EBHIdJxFCepYjn_XHXesuG6xZrOa20aVhwyCdJpX2pevDnlB7a6kOKprG0vAbKYuEWDw57TyawIeJVn3hqhiu9', 'Ios', '2019-12-23 23:49:20', '2019-12-23 23:49:20'),
(161, 129, 'fccGhYCeTQc:APA91bGWJUto3frSuuw1FCFglVasLGQTCX3rTH6gt_xxV4cqdM6VBc0gT1IrNUAXQBO3Oh33uDXm2iHIx0MV_BPKw4QLsieQnrVLuBwXQn2KJWbrHj-qZ_UV3miBU6ikz-kE6QEae9xz', 'Ios', '2019-12-23 23:55:32', '2019-12-23 23:55:32'),
(162, 129, 'cE91k8bL1po:APA91bF5QGV4t_9DGiM0AjFV8zC4DJgZC5ay7xMlZQD-_KMHwauCeQ7mtjvNyh5XunG7umdrvCo2nWDhkHNcY1eLXGB4w1cXMV_KbHkFsHw-Um_n_pyQTUsA4VF_erfuk-0DjoPhSebN', 'Ios', '2019-12-24 03:27:05', '2019-12-24 03:27:05'),
(164, 132, 'fBh0jHWF6Ww:APA91bHCKo5JxdJAKmfYqkAlvEbG-crTMC1OU-__FF2Jp9iZ36KTTRLyw2u8zgwrw-5Gatoo0KPwXgt80Uz6RLqrcHEwVXAqmaeMqXVVLNigZMykksz30U9RcYVmYCVuPJB046HZq-D6', 'Ios', '2020-01-02 05:48:29', '2020-01-02 05:48:29'),
(166, 129, 'f1e8NpEzP8I:APA91bFM5-uFb3gzmp9BpuHuF6ngzNhSZx8CFWmaoUnocOgOJU9BWh0sHzdjfEN-qNkQvtxvtjfmWXtehsw_0ljLKNenJqbPbkIqxCl77i7zJutiZpfyMzfYRGaDLKWVwNb_Tf58DFD_', 'Ios', '2020-01-04 18:33:17', '2020-01-04 18:33:17'),
(168, 129, 'de2amxjhWJc:APA91bGWRDNzZc8RU2CKs1oxLe17fAOHDO6GMnRRTcPpHU6MQCjlrNynvl08v4G2zdEZ_pkklD-Lb0D0l--K__wUhPm4qyR2M1h42Mb8QqgN_uf_7w1cv531vHvvyoDCaRioa1CQAuHQ', 'Ios', '2020-01-08 20:37:22', '2020-01-08 20:37:22'),
(170, 129, 'cZonkY58kHE:APA91bHvbWRtBxN3kwwYPKFUy8iuNX752zGVZecoigkuLjoxNpBsccxfDfcesDZniNkLmr4-N47f5-V5FRlpf35E8-5QIUm44ilQOZYGIG-hNXRhhkbZcsIcUUx_qy8k-RfVawzV7Db6', 'Ios', '2020-01-09 15:39:03', '2020-01-09 15:39:03'),
(173, 129, 'coLNGj-yM1o:APA91bEbH7QoXz747t1_h4Hh3hD-rthxx5HP57vi08hLoYqr77o6AHssPrCs0Q7W72dIOHMvLNCpSZ-KX8vHWDJw2n8mJCayCT0lchEVJFEOpGtztZAvdpF-eP8loK8qQM387efh3tK5', 'Ios', '2020-01-09 20:13:33', '2020-01-09 20:13:33'),
(176, 132, 'enxTdZ-BxU8:APA91bGzeD1OdMZ_2xzwhEU2QQPc60ULeQb8DQec38QMRI8f5ZLsHlSVcds0lj1IM7iFMwnz_ANM-525pa-z-UbmZrTaTPX5zMEXZsEzmASKz6sd2qWClSWBkL_DRbyrCGzqlGkDDGwo', 'Ios', '2020-01-22 16:26:06', '2020-01-22 16:26:06'),
(180, 137, 'dS0bKgfodTs:APA91bFEnfzqwuU-0U3lWwJQeVzhv74g961NBanRaWBD7nwrgAl3XfH1RWNPy93T8aFDx_DzdnEMty0naa5QCsWWpgwjYNLogrgDuT5UeUHYMJ8SdA5oNAbMOuccw9NbIlNvd2GEA8Dz', 'Ios', '2020-01-25 23:31:34', '2020-01-25 23:31:34'),
(181, 137, 'freasmVHJS8:APA91bGKJDoQqXkUxxxVwqhPc1T1QWtqLI5rjrRlKHhmqOm0g1JXHAklN0Sv-FSvEaLtwG71rafc1KtVBy8pnLF7xXstOLYpCfT5RtmpjB8PYk-keW2OF64J41H7zKfUioqiUmDr7Jm9', 'Ios', '2020-01-27 03:17:27', '2020-01-27 03:17:27'),
(182, 137, 'eSmeIgXCjpg:APA91bHczBq4kZN1FvGX3OYojhZGXXQjf0n9Z2TN3LYPejI3OAxPr8u_iVE9ISrMEdUb9BPb4EIjSoK4CGn-AN0R596SdYZJkYqUJfx2UNccoKkr3bdJbT8D3E3LptFFmbF7ihYy4NFl', 'Ios', '2020-01-27 21:11:56', '2020-01-27 21:11:56'),
(184, 137, 'cHZ19kGx1nU:APA91bF5ha4Id_jG9_eTHzbx3hefOYSMRp7iLvO0RWwlD1peWohh11kDBfci5fC8t2g045FeTi8FYaNiRh-Wj6UkOOAmNfHoQ0a9pBvWhpQ6R_BUHw6vM0JiN5CPaZ8UCDXLQQ2ptvmO', 'Ios', '2020-01-28 13:21:18', '2020-01-28 13:21:18'),
(185, 137, 'efstEZhvjXw:APA91bHh18Yy3wsP4nlcLKrVLaBElbcCq1RYXJhhFO_k7uS_gUniqNxJ9tblllPBHscEya6znuqDnBIISvU5FRDPBeaY0uhJ4xdxVtw2Pb5A_VgV7z9OeZ5DatZVjRUvUpWEidEnKyyU', 'Ios', '2020-01-28 22:27:42', '2020-01-28 22:27:42'),
(186, 129, 'f5-dSuDjzwk:APA91bHDxgqh0-bm_jkScbC4c76cdwIri3aZb038pCod6jjsJtCVeYgFCKdA8CVSiYX807YulRpziziUDG-RgsMXVTxY35iX5GXjT2OKceZRMlkG1PH52BIcyUNE7FYfTB3DJ-bjEyeY', 'Ios', '2020-01-30 01:05:35', '2020-01-30 01:05:35'),
(189, 130, 'fmLswmLFvz4:APA91bGE84YQ32dr7rNp-LjWcyurhORZETM7Wmo6nznQQiQ3APIH6vbq2y3L8oDwZL7o-c2uwVWwAtjJ6VZowH7YEK4CC94BYXgmv3cIYqvw6SokcEUyjLuI8HERJZhNMCvYDTUtPam7', 'Ios', '2020-02-05 14:56:47', '2020-02-05 14:56:47'),
(193, 137, 'erOQPyMFitg:APA91bHrv46V80ExpgYM-pOtmOjESMWc3XgtafQhwFJRv6tJgFYOys4Ham-n3dao8LhAuu6oP6Hr1RCcSEDZlmo6x_athA9vSTRuDwdR4pTnJFfZIxtV6z2VRYu2YylnyyOoK4lYgE7Z', 'Ios', '2020-02-05 19:02:52', '2020-02-05 19:02:52'),
(195, 137, 'fRp_K6vvBGQ:APA91bENPvflmW0k79mYeIrGcC9YUDyvK-WexhHJVnQqYWs5jldlRdG138ulhv5kliZOXlvmwzx9UMKxlz8j1-_wU0RZAvILOv9r5NHXi-0wz3FCk6Zoh8hNkva5FFSiGV-iaQmbird9', 'Ios', '2020-02-07 20:42:20', '2020-02-07 20:42:20'),
(196, 137, 'cwUuWnEryu0:APA91bEr977vLyu1Pim1yITHm5L4Oqh5mcRl2I81kZhtbUMoAwvovm5Ih8vzJDUXAxulZjYtDghB3QIU1YgLeWNWGFkvfwwG1d4tqWsRXY9xGFesFgnsDHwt7MajrCzc7Gn_oIlHDrIa', 'Ios', '2020-02-07 22:27:50', '2020-02-07 22:27:50'),
(197, 137, 'fG6yVCh_o80:APA91bFvjc1kn10f7w5Xvbk-TyMDEloNivluYAD_EBti8zULCUrRFO9HSKFhEZVk8iAQ7UXIQBq5zCUB187D4FHBNQPH0DcpNXrIkidg_NhJePov_K-SfxWSySnAOJUAWIPCP1cCtQrJ', 'Ios', '2020-02-08 00:15:55', '2020-02-08 00:15:55'),
(198, 137, 'ep5fAWiIaMo:APA91bF713VGw89lQWnV-f_8QqKEiFg04QnL8uhPU_YohLUUlLWhxNtdPCbXv2pNLRIZEEi_nHr0wxMSYwKyJz8LMcJVa7JW-J2lVadu1GJx642voMRUj-OVl68v7sKtcjtbFX8eiJy9', 'Ios', '2020-02-08 16:06:35', '2020-02-08 16:06:35'),
(199, 137, 'fpcva-Wll_k:APA91bHWeVHBVlfVw1s51dI8zZmkTD1HsohcfRG-2ITJ-N84TacNFiN1wUtX-6xeKM3mrGN7pG0hKFdk_9Q8Q8OVvOOTB7ZuK9jM1uv6oxyMirzA_CfvjTfA5pJRDR8xCICD5yd39-zK', 'Ios', '2020-02-08 19:51:04', '2020-02-08 19:51:04'),
(200, 137, 'cnRFJHprFK8:APA91bHoPuW15ogSIhUzTuLmG02lGNFufkgmlpGHUavyqTA5ZMoecvtz_YndmSLOy96HFPb1-RLOC-U2Z8TDcwTfKlIaAPNYxeX96aQdkfS5zW4fLugO4x2VqEtplxAQwF8sgEWhY2Yv', 'Ios', '2020-02-09 12:56:21', '2020-02-09 12:56:21'),
(201, 137, 'dqWVi07hEZY:APA91bFEZTsAbhvQdr2ha6ksaES0WqqeB7nzTfDW-a3RIfcSNSa2SSDLvAo6Uostc6cXlJ259iAdwgfJe9VJcy50QmkU5q5V2bdlJzmWWPDmHJhTvuzWorXzgOB_rMf2rTwBgiFDPHxd', 'Ios', '2020-02-10 02:35:45', '2020-02-10 02:35:45'),
(204, 137, 'd7ocFQ18tdw:APA91bF6ZvYydNJsXoZUzxMJbhjHgsWo9TIeqtH25p0-0YB_XAYIXR9yeV1smG-Sl5tIRZcJxVPPDHJUyBIjMpvHkiADY6ubA0AOMSdZmMXHgDvFkmTf_aj3HsBF3aT_9ANU_M3qniNw', 'Ios', '2020-02-11 00:49:48', '2020-02-11 00:49:48'),
(205, 137, 'ewkAs-wxRR8:APA91bHDeNUNskpISqprYFZNb_8OFhNF-O-aaH8EIt-N8AWC_YTDQd6LCqQ23oscp0qXyU9S2qnZDg4pod4K-9cUKx85QMo11hXTfPs5mPLEiNF3tLYwtLRmf-E1uFH5bHGTLPx1V0Xg', 'Ios', '2020-02-11 00:55:40', '2020-02-11 00:55:40'),
(206, 137, 'cQAFGT0jKr4:APA91bGSwLQ3ZvSh-i2Z3LMr6tJXT7qj3hF0Mwn3Tr-lnSyfjRuVgKfAqehGM8BrGgHDPfh3J0TtDQ9KbkfQ37VIecQMIjIh39lptJbEI5z0g0JhT9ao2YiDWxRteg7QqO0FNdO1cYDY', 'Ios', '2020-02-11 01:24:44', '2020-02-11 01:24:44'),
(207, 137, 'cdLk8JemiSA:APA91bHnL3VAHxxctBKqTOShMvP7pIcYAKtLhfF-2Nf2aOlqHi_uJnTu7dXky-OAObrj1I6QbFDRh3hXvpbYeE5mNIWOBubLpgQVbixU38hzfJJjPZ1JxMR7mMfc5SJQI4geNwhYl8jE', 'Ios', '2020-02-11 12:00:18', '2020-02-11 12:00:18'),
(209, 137, 'd1y-BjE7Bdc:APA91bEq3ydZvz_cEKYGYHG55k3r0R9g6wLrsJ-koqOhE_h9DuNeu6mgq6e4WtWFbp3Hneuw65L-N-jx_JkHPBpIV59i4P94R8rXhfCUO1HPXF3hdF7TPUYh5qTvPGQ8JcV1WI0nis2m', 'Ios', '2020-02-11 12:26:44', '2020-02-11 12:26:44'),
(211, 137, 'fozZNvDJhI4:APA91bGnZbmWZQSGQ26cntUrWKDJy6SxoSF-FE7YwUh3ZtMXzd1CnY4twt2MQ3J-PirqtoQM2H33IIAugwXtsUNKtsxJiam5mHSubYMWTf7dxKbSUStAjiHayyyvXl4Le7zCASq71mPe', 'Ios', '2020-02-12 01:44:14', '2020-02-12 01:44:14'),
(212, 129, 'eISYaAt_MFc:APA91bE0jH4irUAx6HfkW1wbEmeqk547GRV7glOOj-eT5dn2I3eOGCCPI5ufcsVGqipZIfJxf-a2ZlZiMlkk9C4hjVvTq7GmfZiEUeuSrMiNf987APznoY7y-T9bFY1og_MDinyRKDO3', 'Ios', '2020-02-17 16:19:12', '2020-02-17 16:19:12'),
(214, 129, 'd1F-pGci1rg:APA91bH_SIhy2up-94tm_s7RYXezBZHwazj3-7UxvbPRA-1MCXQZrh8vFOWc-HAyN7Lx5e4di3yfduxKPUT66hNcEMIpx1sqxWAMpwmp2I1v44XZzwKOeYbUhp2fgaRcczQpX5hEV8pK', 'Ios', '2020-02-18 11:51:03', '2020-02-18 11:51:03'),
(215, 137, 'cEA8ZTUo-Vs:APA91bGC3bporluReSgNfLm4Q8ylvGaviChSsnNrPDZZ_JkRVLlv190hoMpoKp_-WVR3TjaCc0BJbgaojfVB7uNOGfAHjJMOvUNc_v3BabSaxgpF3OPQ-jeqgt9VgIy-bs1vleez_2q_', 'Ios', '2020-02-20 15:38:38', '2020-02-20 15:38:38'),
(216, 137, 'c4LzyNQIDW4:APA91bF5-oxt-RYEkdocqHR8iTDAZjn7HqVOiaCBVoJtIKijMj9o3qpaIMQ606zcngS0hDPOSd8a6kw6vU-7nmFpaJXG6uUaIknmtwit-14vpqdwYc2XXEw-Sx9NDfWiLL81pO-fRtDY', 'Ios', '2020-02-21 19:50:03', '2020-02-21 19:50:03'),
(217, 137, 'csZVnni23rU:APA91bF6fHn-FDYh371uukj69YuKCaVOxD1g3PsQKdIavQboV4TGT1lEp9dwRD_mTqt1tm6Pgh-Il5HQi9NXD-Ug2g5KZ5EK0Lt6_Qq4BEYYSYIaYc1hydHUQEGFWhcMILUMKRxYu1ai', 'Ios', '2020-02-22 18:51:52', '2020-02-22 18:51:52'),
(221, 129, 'e-7wB0A4330:APA91bEH_qQ8b1DsiGtRmKXtYGgcq6M5mpE73IlHwpJz75R74IJZGynE-gWrvA2-EeVbl3kKCGywR3LcFAYsphipsUuctMsFngh5sKj9C84Do7seVgfqEJ_MBMLrR3JeqqnV4jZNyaHL', 'Ios', '2020-02-24 18:35:17', '2020-02-24 18:35:17'),
(222, 129, 'egq81ZpKbaA:APA91bHQTTrl7bNy-wOnmTGkproczSkSp8F2Bfc72_FnRKaVzEfDC6_VAMnLOs-vH0Gg6EQMms7fZ4MzU37ICc3ZBa_APVnSIBsAJ0rT0HZfQOdnRpUKpVyaXtg4yeNDvKamijTTo3nE', 'Ios', '2020-02-25 23:58:59', '2020-02-25 23:58:59'),
(223, 137, 'fTYj5NARwlg:APA91bFmYZW5gXieR4ZY9CpoffLU7TpQr1ICEc0STH-wPyVMnVXBxCFjfdIWx6oRmZy62AKiW8ys-m9gUEOQLCoxopSH8p8N7f1c20zG4HIlr5BVFY8aGBLsIiLod043I3p9T7Ifqkjt', 'Ios', '2020-02-26 17:22:04', '2020-02-26 17:22:04'),
(224, 137, 'dfBMpBjEGqs:APA91bEG4Ol4VaBmHU5beS3LcTg5dnMu984tkLgjbs1KJhIqBIxhwCRlWY_5xOlfbEULzfqEDM0P7fb0D3U4zJbe34O4JNRCLTI6aNdAgmo-WEpT9dHfqDd1JPlUdZJFrO7AF9AHhyfW', 'Ios', '2020-02-26 18:18:57', '2020-02-26 18:18:57'),
(225, 129, 'c6JoStoSj5o:APA91bFGqhbuB_2tw2ijpJqIjSLlnclgXqrwQ7Elw8Oxad-Z6ObNK3Qdf6-FG3Z0ez8lshMZFPdkqxjSCOtL6JC9QDMbtued2DIuapttNE-rBfrCIwrYaj9ld2VhzSrMSIIIAaZUlPad', 'Ios', '2020-03-08 17:34:46', '2020-03-08 17:34:46'),
(226, 137, 'cerQ3NZn6no:APA91bEDpPEAgag9qloN9rc5cZ0eGZOergYXf7_dNDMzUqVj729OAoCFyh_w5YfLZ6YrLINKS3JWArL7iy7T76VROLznfWz_2JRuk2c4SWbowG0T5CCZzLP0CkCSb9CfJ-VR-0CRmgOb', 'Ios', '2020-03-12 19:22:57', '2020-03-12 19:22:57'),
(230, 130, 'cTknPhAd9AQ:APA91bHq7mGNoUwmheiS4e3MOdR7938xijF-j_ledpQyR9ppJyHYqbf9a75tfRoW3Iev84GNd37vG5s-5J2B3iu3nNqzchzpQ1Z2-6YiKDmYmb0mkhKXbSnsJgxZHPl_3POhQ3ojH_jF', 'Ios', '2020-03-20 04:03:38', '2020-03-20 04:03:38'),
(231, 129, 'dc4EDYY5qhc:APA91bG75NUMZ2YxUyPQPI0dU4jW5J2W2uXLeKjXB6KgkK_gdbLhpy4XwziBzrJdId6yRxqTPmr3jkZjeBBUJPlB9goctMrgRVMgmi4Tz-QjgehCnL89sTupKQk5T0xOGUhpvBtjNMck', 'Ios', '2020-03-20 04:04:45', '2020-03-20 04:04:45'),
(232, 137, 'cIugV9N6Co4:APA91bHcJDoH2QijlZ-y7Iz3EZPqCQC3j-0nUv7rt1QF9ziw2LXgiHAVkVwIqqYKgSZEspww232pr_Nq29sF2sneoyd4mrKqzGQCsBnOPge0NeUNXptDFvHcJxXk1gbeAer_sP1XVCFt', 'Ios', '2020-03-20 08:31:13', '2020-03-20 08:31:13');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `favorite_user`
--

CREATE TABLE `favorite_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `ads_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `favorite_user`
--

INSERT INTO `favorite_user` (`id`, `user_id`, `ads_id`, `created_at`, `updated_at`) VALUES
(2, 132, 80, '2019-11-21 17:10:41', '2019-11-21 17:10:41');

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

CREATE TABLE `followers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `follow_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `url`, `created_at`, `updated_at`) VALUES
(1, 'http://sell4me.ml/public/files/users/profiles1574104406.sXHXXYZmJcBfL2YfHHW5fileName.jpg', '2019-11-18 22:13:26', '2019-11-18 22:13:26'),
(2, 'http://sell4me.ml/public/files/users/profiles1574104419.vJn4O7UADFPuZ2OyX4BsfileName.jpg', '2019-11-18 22:13:39', '2019-11-18 22:13:39'),
(3, 'http://sell4me.ml/public/files/users/profiles1574105118.vDb7jbmfqh1934VtUja0fileName.jpg', '2019-11-18 22:25:18', '2019-11-18 22:25:18'),
(4, 'http://sell4me.ml/public/files/users/profiles1574105129.Tb1YaYsSfvOSP8EfVCrhfileName.jpg', '2019-11-18 22:25:29', '2019-11-18 22:25:29'),
(5, 'http://sell4me.ml/public/files/users/profiles1574177975.3BhGsKLxs7stO9ZcM5cXfileName.jpg', '2019-11-19 18:39:36', '2019-11-19 18:39:36'),
(6, 'http://sell4me.ml/public/files/users/profiles1574253407.3GepYksfEWuG65HdQoQkfileName.jpg', '2019-11-20 15:36:47', '2019-11-20 15:36:47'),
(7, 'http://sell4me.ml/public/files/users/profiles1574257809.5YsW4RipIDAcMaN6TZx1fileName.jpg', '2019-11-20 16:50:09', '2019-11-20 16:50:09'),
(8, 'http://sell4me.ml/public/files/users/profiles1574264757.vcxC5Q6hKdGAUXcGgbnDfileName.jpg', '2019-11-20 18:45:57', '2019-11-20 18:45:57'),
(9, 'http://sell4me.ml/public/files/users/profiles1574264985.lvjpb43mZRejrAkorQQ8fileName.jpg', '2019-11-20 18:49:46', '2019-11-20 18:49:46'),
(10, 'http://sell4me.ml/public/files/users/profiles1574373191.JWjAA1f8d5fnUGFpXYpyfileName.jpg', '2019-11-22 00:53:11', '2019-11-22 00:53:11'),
(11, 'http://sell4me.ml/public/files/users/profiles1574375261.D7BtcgT70va8DoBrsjsyfileName.jpg', '2019-11-22 01:27:41', '2019-11-22 01:27:41'),
(12, 'http://sell4me.ml/public/files/users/profiles1574695936.EpwS7SOZXKZFOhyhLwK1fileName.jpg', '2019-11-25 18:32:16', '2019-11-25 18:32:16'),
(13, 'http://sell4me.ml/public/files/users/profiles1574695960.oZDFn1cQJYwDC3LRCvxifileName.jpg', '2019-11-25 18:32:40', '2019-11-25 18:32:40'),
(14, 'http://sell4me.ml/public/files/users/profiles1574885431.tozUggRnD2GTa8lqI2tRfileName.jpg', '2019-11-27 23:10:31', '2019-11-27 23:10:31'),
(15, 'https://sell4me.ml/public/files/users/profiles1577134569.pom0wO2XgvtkxdoHzGiufileName.jpg', '2019-12-23 23:56:09', '2019-12-23 23:56:09'),
(16, 'https://sell4me.ml/public/files/users/profiles1577134590.D2sbIwgSYz5O3Mx6gt1yfileName.jpg', '2019-12-23 23:56:31', '2019-12-23 23:56:31'),
(17, 'https://sell4me.ml/public/files/users/profiles1577134733.hsssrnBQHx6DjQtmkH47fileName.jpg', '2019-12-23 23:58:53', '2019-12-23 23:58:53'),
(18, 'https://sell4me.ml/public/files/users/profiles1577135044.cx2zD61Zx3gdhvU1Vo46fileName.jpg', '2019-12-24 00:04:04', '2019-12-24 00:04:04'),
(19, 'https://sell4me.ml/public/files/users/profiles1577147297.wl1nskig45BU5c8uQwhUfileName.jpg', '2019-12-24 03:28:17', '2019-12-24 03:28:17'),
(20, 'https://sell4me.ml/public/files/users/profiles1577147320.mePbrCvAid3Btagy62ROfileName.jpg', '2019-12-24 03:28:40', '2019-12-24 03:28:40'),
(21, 'https://sell4me.ml/public/files/users/profiles1577147871.7mQjnnwdkTfgMS4zGVYqfileName.jpg', '2019-12-24 03:37:51', '2019-12-24 03:37:51'),
(22, 'https://sell4me.ml/public/files/users/profiles1577147887.4rkgkq8VV4e2EWFdmLMkfileName.jpg', '2019-12-24 03:38:07', '2019-12-24 03:38:07'),
(23, 'https://sell4me.ml/public/files/users/profiles1577188333.bBzdDsoQXtQkbf2JnadrfileName.jpg', '2019-12-24 14:52:13', '2019-12-24 14:52:13'),
(24, 'https://sell4me.ml/public/files/users/profiles1577188370.cykodeoa3KNjqzIelSrufileName.jpg', '2019-12-24 14:52:50', '2019-12-24 14:52:50'),
(25, 'https://sell4me.ml/public/files/users/profiles1577448471.RTYza6WfB0HqRJ1QXDOWfileName.jpg', '2019-12-27 15:07:51', '2019-12-27 15:07:51'),
(26, 'https://sell4me.ml/public/files/users/profiles1577448481.5a5b1EaYZ9cK2l3xJXFKfileName.jpg', '2019-12-27 15:08:01', '2019-12-27 15:08:01'),
(27, 'https://sell4me.ml/public/files/users/profiles1578506182.2kMO3coXUCyoqA2y9Z55fileName.jpg', '2020-01-08 20:56:22', '2020-01-08 20:56:22'),
(28, 'https://sell4me.ml/public/files/users/profiles1578944324.tVmcMecL4cn6EzvyqeAgfileName.jpg', '2020-01-13 22:38:44', '2020-01-13 22:38:44'),
(29, 'https://sell4me.ml/public/files/users/profiles1581179008.hIFA14Wpo8xtkpVjDHtPfileName.jpg', '2020-02-08 19:23:29', '2020-02-08 19:23:29'),
(30, 'https://sell4me.ml/public/files/users/profiles1582664695.XFUxQaDiroUdWwFEtMn1fileName.jpg', '2020-02-26 00:04:55', '2020-02-26 00:04:55'),
(31, 'https://sell4me.ml/public/files/users/profiles1582664705.QUbhxweY5HAwoKu0TrYqfileName.jpg', '2020-02-26 00:05:05', '2020-02-26 00:05:05'),
(32, 'https://sell4me.ml/public/files/users/profiles1583440237.3plalCjtyYjXEG00B3K5fileName.jpg', '2020-03-05 23:30:37', '2020-03-05 23:30:37'),
(33, 'https://sell4me.ml/public/files/users/profiles1583440288.VagDltsl7gzPQmohSslqfileName.jpg', '2020-03-05 23:31:28', '2020-03-05 23:31:28'),
(34, 'https://sell4me.ml/public/files/users/profiles1584209547.Vd8fTDTkjAVAmOd4KeqAfileName.jpg', '2020-03-14 21:12:28', '2020-03-14 21:12:28'),
(35, 'https://sell4me.ml/public/files/users/profiles1584209555.snYSZqiagIRXHzCc7lpbfileName.jpg', '2020-03-14 21:12:36', '2020-03-14 21:12:36'),
(36, 'https://sell4me.ml/public/files/users/profiles1584209565.DggVB7ge4nxmeVsXxFlrfileName.jpg', '2020-03-14 21:12:45', '2020-03-14 21:12:45'),
(37, 'https://sell4me.ml/public/files/users/profiles1584213443.WrAEPeRLIU5EMl8XOVZmfileName.jpg', '2020-03-14 22:17:23', '2020-03-14 22:17:23'),
(38, 'https://sell4me.ml/public/files/users/profiles1584664296.dUV3lcVzCRc9B9dUOSk9fileName.jpg', '2020-03-20 03:31:36', '2020-03-20 03:31:36'),
(39, 'https://sell4me.ml/public/files/users/profiles1584666278.05utmROzI85zHs2hxP62fileName.jpg', '2020-03-20 04:04:38', '2020-03-20 04:04:38');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `conversation_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `conversation_id`, `user_id`, `message`, `created_at`, `updated_at`) VALUES
(1, 1, 130, 'اهلا', '2019-11-18 22:29:39', '2019-11-18 22:29:39'),
(2, 1, 129, 'ااا', '2019-11-18 22:29:51', '2019-11-18 22:29:51'),
(3, 1, 129, 'مرحبا', '2019-11-18 22:30:10', '2019-11-18 22:30:10'),
(4, 2, 129, 'هلا مرحبا', '2019-11-18 22:41:29', '2019-11-18 22:41:29'),
(5, 2, 130, 'هلاا', '2019-11-18 22:52:00', '2019-11-18 22:52:00'),
(6, 3, 133, 'Wel', '2019-11-19 23:39:54', '2019-11-19 23:39:54'),
(7, 4, 131, 'Test', '2019-11-20 16:52:11', '2019-11-20 16:52:11'),
(8, 5, 131, 'Wel', '2019-11-20 18:58:17', '2019-11-20 18:58:17'),
(9, 5, 132, 'You', '2019-11-21 02:13:07', '2019-11-21 02:13:07'),
(10, 6, 131, 'hello', '2019-11-23 17:08:40', '2019-11-23 17:08:40'),
(11, 6, 131, 'hello', '2019-11-23 17:14:20', '2019-11-23 17:14:20'),
(12, 6, 131, 'hello', '2019-11-23 17:15:20', '2019-11-23 17:15:20'),
(13, 6, 131, 'hello', '2019-11-23 17:16:07', '2019-11-23 17:16:07'),
(14, 7, 132, 'hello', '2019-11-23 17:17:57', '2019-11-23 17:17:57'),
(15, 8, 133, 'mm,m,', '2019-11-23 17:23:14', '2019-11-23 17:23:14'),
(16, 8, 133, 'mm,,m', '2019-11-23 17:23:34', '2019-11-23 17:23:34'),
(17, 8, 133, 'mm,,m', '2019-11-23 17:24:03', '2019-11-23 17:24:03'),
(18, 8, 133, 'hello', '2019-11-23 17:24:40', '2019-11-23 17:24:40'),
(19, 7, 133, 'vhjhjhjvjh', '2019-11-23 17:27:09', '2019-11-23 17:27:09'),
(20, 9, 132, 'Hi', '2019-11-23 17:29:23', '2019-11-23 17:29:23'),
(21, 9, 132, 'Hi', '2019-11-23 17:29:38', '2019-11-23 17:29:38'),
(22, 9, 132, 'Her', '2019-11-23 17:29:46', '2019-11-23 17:29:46'),
(23, 9, 132, 'Her', '2019-11-23 17:30:35', '2019-11-23 17:30:35'),
(24, 10, 133, 'klmkmkl', '2019-11-23 17:48:07', '2019-11-23 17:48:07'),
(25, 10, 133, 'hello', '2019-11-23 17:48:27', '2019-11-23 17:48:27'),
(26, 10, 133, 'jjkjkjjbb', '2019-11-23 17:48:37', '2019-11-23 17:48:37'),
(27, 4, 133, 'jkjkjk', '2019-11-24 18:15:11', '2019-11-24 18:15:11'),
(28, 10, 133, 'hello', '2019-11-24 18:44:58', '2019-11-24 18:44:58'),
(29, 10, 130, 'معاكي', '2019-11-24 18:47:34', '2019-11-24 18:47:34'),
(30, 10, 130, 'معاكي', '2019-11-24 18:48:15', '2019-11-24 18:48:15'),
(31, 10, 130, 'معاكي', '2019-11-24 18:49:59', '2019-11-24 18:49:59'),
(32, 10, 130, 'معاكي', '2019-11-24 18:50:14', '2019-11-24 18:50:14'),
(33, 10, 130, 'معاكي', '2019-11-24 18:51:14', '2019-11-24 18:51:14'),
(34, 10, 130, 'معاكي', '2019-11-24 18:51:57', '2019-11-24 18:51:57'),
(35, 4, 133, 'jnjk', '2019-11-24 18:53:34', '2019-11-24 18:53:34'),
(36, 4, 133, 'jjbyu', '2019-11-24 18:53:39', '2019-11-24 18:53:39'),
(37, 10, 133, 'hello', '2019-11-24 19:41:47', '2019-11-24 19:41:47'),
(38, 10, 132, 'hello', '2019-11-24 19:41:55', '2019-11-24 19:41:55'),
(39, 11, 132, 'hello', '2019-11-24 20:17:04', '2019-11-24 20:17:04'),
(40, 11, 132, 'hello', '2019-11-24 20:17:14', '2019-11-24 20:17:14'),
(41, 12, 130, 'اهلا', '2019-11-26 20:16:31', '2019-11-26 20:16:31'),
(42, 2, 130, 'Hi', '2019-11-26 20:26:12', '2019-11-26 20:26:12'),
(43, 5, 132, 'Hi', '2019-11-26 23:16:42', '2019-11-26 23:16:42'),
(44, 13, 137, 'Hi', '2020-02-11 00:50:08', '2020-02-11 00:50:08'),
(45, 13, 137, 'Welll', '2020-02-11 00:51:06', '2020-02-11 00:51:06'),
(46, 13, 137, 'Hgggg', '2020-02-11 00:56:01', '2020-02-11 00:56:01'),
(47, 13, 137, 'Rrr', '2020-02-11 01:05:04', '2020-02-11 01:05:04'),
(48, 13, 137, 'Wwww', '2020-02-11 01:09:42', '2020-02-11 01:09:42'),
(49, 13, 137, 'Rrrrrrrrrrrrr', '2020-02-11 01:53:11', '2020-02-11 01:53:11'),
(50, 13, 137, 'We', '2020-02-11 11:20:20', '2020-02-11 11:20:20'),
(51, 13, 137, 'Ddd', '2020-02-11 11:21:18', '2020-02-11 11:21:18'),
(52, 13, 137, 'You', '2020-02-11 11:21:23', '2020-02-11 11:21:23'),
(53, 15, 129, 'هلا', '2020-03-20 03:35:15', '2020-03-20 03:35:15'),
(54, 15, 129, 'هلا', '2020-03-20 03:35:42', '2020-03-20 03:35:42'),
(55, 15, 141, 'هلا فيك', '2020-03-20 03:36:09', '2020-03-20 03:36:09'),
(56, 15, 141, 'ا', '2020-03-20 03:37:10', '2020-03-20 03:37:10'),
(57, 15, 141, 'ااا', '2020-03-20 03:39:49', '2020-03-20 03:39:49'),
(58, 15, 129, 'هلا', '2020-03-20 03:40:14', '2020-03-20 03:40:14'),
(59, 15, 129, 'تتتلل', '2020-03-20 03:40:33', '2020-03-20 03:40:33'),
(60, 15, 141, 'ا', '2020-03-20 03:45:22', '2020-03-20 03:45:22'),
(61, 15, 141, 'ا', '2020-03-20 03:45:55', '2020-03-20 03:45:55'),
(62, 15, 141, 'سلام', '2020-03-20 03:47:04', '2020-03-20 03:47:04'),
(63, 15, 141, 'هلا', '2020-03-20 03:47:11', '2020-03-20 03:47:11'),
(64, 15, 141, 'ا', '2020-03-20 03:47:55', '2020-03-20 03:47:55'),
(65, 15, 141, 'ء', '2020-03-20 03:47:57', '2020-03-20 03:47:57'),
(66, 15, 141, 'سلام', '2020-03-20 03:51:39', '2020-03-20 03:51:39'),
(67, 15, 141, 'ا', '2020-03-20 03:53:06', '2020-03-20 03:53:06'),
(68, 15, 141, 'هلا', '2020-03-20 03:54:25', '2020-03-20 03:54:25'),
(69, 15, 141, 'ء', '2020-03-20 03:54:48', '2020-03-20 03:54:48'),
(70, 15, 141, 'ء', '2020-03-20 03:54:59', '2020-03-20 03:54:59'),
(71, 15, 129, 'هيريري', '2020-03-20 03:55:40', '2020-03-20 03:55:40'),
(72, 15, 129, 'هلاتنين', '2020-03-20 04:03:00', '2020-03-20 04:03:00'),
(73, 15, 141, 'هلا', '2020-03-20 04:03:46', '2020-03-20 04:03:46'),
(74, 15, 129, 'Ushshsh', '2020-03-20 04:05:00', '2020-03-20 04:05:00'),
(75, 15, 141, 'ااا', '2020-03-20 04:05:15', '2020-03-20 04:05:15'),
(76, 16, 129, 'نستيويمير', '2020-03-20 04:06:31', '2020-03-20 04:06:31'),
(77, 16, 129, 'مسوينسمير', '2020-03-20 04:06:34', '2020-03-20 04:06:34'),
(78, 16, 129, 'سنيريمبوووووو', '2020-03-20 04:06:36', '2020-03-20 04:06:36'),
(79, 16, 129, 'اااا', '2020-03-20 04:06:38', '2020-03-20 04:06:38'),
(80, 16, 130, 'اهلاا', '2020-03-20 04:07:48', '2020-03-20 04:07:48'),
(81, 16, 130, 'اهلاا', '2020-03-20 04:07:51', '2020-03-20 04:07:51'),
(82, 16, 130, 'تتتتت', '2020-03-20 04:07:53', '2020-03-20 04:07:53'),
(83, 16, 130, 'تتتتت', '2020-03-20 04:07:55', '2020-03-20 04:07:55'),
(84, 17, 130, 'هلاا', '2020-03-20 04:17:55', '2020-03-20 04:17:55'),
(85, 17, 130, 'السلام عليكم', '2020-03-20 04:18:00', '2020-03-20 04:18:00'),
(86, 17, 141, 'وعليكم وسلام', '2020-03-20 05:23:24', '2020-03-20 05:23:24'),
(87, 18, 134, 'Ffgf', '2020-03-21 00:10:33', '2020-03-21 00:10:33'),
(88, 18, 129, 'االبيا', '2020-03-21 00:10:48', '2020-03-21 00:10:48'),
(89, 18, 129, 'نتلهناباتا', '2020-03-21 00:10:53', '2020-03-21 00:10:53');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_01_19_213650_create_profiles_table', 1),
(3, '2019_01_19_235930_create_images_table', 1),
(4, '2019_01_20_000036_create_verify_users_table', 1),
(5, '2019_01_20_000112_create_rates_table', 1),
(6, '2019_01_20_000501_create_devices_table', 1),
(7, '2019_01_20_000645_create_orders_table', 1),
(8, '2019_01_25_125247_create_ratings_table', 2),
(9, '2019_03_27_121230_create_counteries_table', 3),
(10, '2019_03_28_122214_create_ads_table', 4),
(12, '2019_03_31_212139_create_views_table', 5),
(13, '2019_03_31_213907_create_comments_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `ads_id` int(11) DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` float NOT NULL DEFAULT '0' COMMENT 'price for ads_taxs',
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `is_read` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `sender_id`, `ads_id`, `tax_id`, `title`, `body`, `price`, `type`, `is_read`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 0, '2019-11-21 18:15:56', '2019-11-21 18:15:56'),
(2, 129, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:15:56', '2019-11-22 00:47:34'),
(3, 130, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:15:56', '2019-11-21 18:16:03'),
(4, 132, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:15:56', '2019-11-21 18:38:35'),
(5, 133, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:15:56', '2019-11-23 15:35:50'),
(6, 1, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 0, '2019-11-21 18:16:21', '2019-11-21 18:16:21'),
(7, 129, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:16:21', '2019-11-22 00:47:34'),
(8, 130, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:16:21', '2019-11-21 18:16:36'),
(9, 132, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:16:21', '2019-11-21 18:38:34'),
(10, 133, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:16:21', '2019-11-23 15:35:50'),
(11, 1, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 0, '2019-11-21 18:16:53', '2019-11-21 18:16:53'),
(12, 129, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:16:53', '2019-11-22 00:47:34'),
(13, 130, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:16:53', '2019-11-21 18:24:31'),
(14, 132, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:16:53', '2019-11-21 18:38:34'),
(15, 133, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:16:53', '2019-11-23 15:35:50'),
(16, 1, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 0, '2019-11-21 18:18:55', '2019-11-21 18:18:55'),
(17, 129, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:18:55', '2019-11-22 00:47:34'),
(18, 130, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:18:55', '2019-11-21 18:24:30'),
(19, 132, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:18:55', '2019-11-21 18:38:34'),
(20, 133, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:18:55', '2019-11-23 15:35:50'),
(21, 1, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 0, '2019-11-21 18:19:31', '2019-11-21 18:19:31'),
(22, 129, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:19:31', '2019-11-22 00:47:34'),
(23, 130, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:19:31', '2019-11-21 18:24:30'),
(24, 132, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:19:31', '2019-11-21 18:38:34'),
(25, 133, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:19:31', '2019-11-23 15:35:50'),
(26, 1, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 0, '2019-11-21 18:20:49', '2019-11-21 18:20:49'),
(27, 129, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:20:49', '2019-11-22 00:47:34'),
(28, 130, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:20:49', '2019-11-21 18:24:30'),
(29, 132, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:20:49', '2019-11-21 18:38:33'),
(30, 133, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:20:49', '2019-11-23 15:35:49'),
(31, 1, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 0, '2019-11-21 18:21:25', '2019-11-21 18:21:25'),
(32, 129, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:21:25', '2019-11-22 00:47:34'),
(33, 130, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:21:25', '2019-11-21 18:24:30'),
(34, 132, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:21:25', '2019-11-21 18:38:33'),
(35, 133, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:21:25', '2019-11-23 15:35:49'),
(36, 1, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 0, '2019-11-21 18:33:34', '2019-11-21 18:33:34'),
(37, 129, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:33:34', '2019-11-22 00:47:34'),
(38, 130, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:33:34', '2019-11-21 18:44:48'),
(39, 132, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:33:34', '2019-11-21 18:38:33'),
(40, 133, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:33:34', '2019-11-23 15:35:49'),
(41, 1, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 0, '2019-11-21 18:34:09', '2019-11-21 18:34:09'),
(42, 129, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:34:09', '2019-11-22 00:47:34'),
(43, 130, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:34:09', '2019-11-21 18:44:48'),
(44, 132, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:34:09', '2019-11-21 18:38:33'),
(45, 133, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:34:09', '2019-11-23 15:35:49'),
(46, 1, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 0, '2019-11-21 18:34:53', '2019-11-21 18:34:53'),
(47, 129, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:34:53', '2019-11-22 00:47:33'),
(48, 130, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:34:53', '2019-11-21 18:44:47'),
(49, 132, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:34:53', '2019-11-21 18:38:33'),
(50, 133, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:34:53', '2019-11-23 15:35:49'),
(51, 1, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 0, '2019-11-21 18:37:41', '2019-11-21 18:37:41'),
(52, 129, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:37:41', '2019-11-22 00:47:33'),
(53, 130, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:37:41', '2019-11-21 18:44:47'),
(54, 132, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:37:41', '2019-11-21 18:38:32'),
(55, 133, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:37:41', '2019-11-23 15:35:49'),
(56, 1, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 0, '2019-11-21 18:41:34', '2019-11-21 18:41:34'),
(57, 129, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:41:34', '2019-11-22 00:47:33'),
(58, 130, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:41:34', '2019-11-21 18:44:47'),
(59, 132, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:41:34', '2019-11-21 22:23:18'),
(60, 133, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:41:34', '2019-11-23 15:35:49'),
(61, 1, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 0, '2019-11-21 18:45:01', '2019-11-21 18:45:01'),
(62, 129, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:45:01', '2019-11-22 00:47:33'),
(63, 130, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:45:01', '2019-11-21 18:45:08'),
(64, 132, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:45:01', '2019-11-21 22:23:18'),
(65, 133, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:45:01', '2019-11-23 15:35:49'),
(66, 1, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 0, '2019-11-21 18:45:18', '2019-11-21 18:45:18'),
(67, 129, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:45:18', '2019-11-22 00:47:33'),
(68, 130, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:45:18', '2019-11-21 18:51:02'),
(69, 132, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:45:18', '2019-11-21 22:23:18'),
(70, 133, 1, 0, NULL, 'مرحبا', 'Welcome', 0, 15, 1, '2019-11-21 18:45:18', '2019-11-23 15:35:49'),
(71, 133, 132, 80, 16, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 500 علي الإعلان ( نيسان )', 500, 3, 1, '2019-11-24 17:48:52', '2019-11-24 17:49:05'),
(72, 133, 132, 80, 17, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 589 علي الإعلان ( نيسان )', 589, 3, 1, '2019-11-24 17:50:03', '2019-11-24 17:50:06'),
(73, 132, 133, 80, 17, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان نيسان', 0, 4, 1, '2019-11-24 17:53:56', '2019-11-26 00:39:24'),
(74, 132, 133, 80, 16, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان نيسان', 0, 4, 1, '2019-11-24 18:15:02', '2019-11-26 00:39:24'),
(75, 133, 132, 80, 18, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 18:19:10', '2019-11-24 19:27:05'),
(76, 132, 133, 80, 18, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان نيسان', 0, 4, 1, '2019-11-24 18:20:51', '2019-11-26 00:39:24'),
(77, 133, 132, 80, 19, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 18:51:53', '2019-11-24 19:27:05'),
(78, 132, 133, 80, 19, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان نيسان', 0, 4, 1, '2019-11-24 18:52:10', '2019-11-26 00:39:23'),
(79, 130, 129, 76, 20, 'سوم الإعلانات', ' قام المستخدم عبدالعزيز الدوسري بتقديم سوم بمبلغ 1000 علي الإعلان ( فله في الرياض )', 1000, 3, 1, '2019-11-24 18:52:38', '2019-11-26 15:49:31'),
(80, 133, 132, 80, 21, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 18:54:23', '2019-11-24 19:27:05'),
(81, 133, 132, 80, 22, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 18:54:39', '2019-11-24 19:27:05'),
(82, 132, 133, 80, 22, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان نيسان', 0, 4, 1, '2019-11-24 18:54:46', '2019-11-26 00:39:23'),
(83, 133, 132, 80, 23, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 19:17:53', '2019-11-24 19:27:05'),
(84, 132, 133, 80, 23, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان نيسان', 0, 4, 1, '2019-11-24 19:18:01', '2019-11-26 00:39:23'),
(85, 133, 132, 80, 24, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 19:21:43', '2019-11-24 19:27:05'),
(86, 133, 132, 80, 25, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 19:21:56', '2019-11-24 19:27:04'),
(87, 133, 132, 80, 26, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 19:27:10', '2019-11-24 20:39:52'),
(88, 133, 132, 80, 27, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 19:27:24', '2019-11-24 20:39:52'),
(89, 133, 132, 80, 28, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 19:36:42', '2019-11-24 20:39:52'),
(90, 133, 132, 80, 29, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 20:22:11', '2019-11-24 20:39:52'),
(91, 133, 132, 80, 30, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 20:22:43', '2019-11-24 20:39:52'),
(92, 133, 132, 80, 31, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 20:24:13', '2019-11-24 20:39:51'),
(93, 133, 132, 80, 32, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 20:24:52', '2019-11-24 20:39:51'),
(94, 133, 132, 80, 33, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 20:30:52', '2019-11-24 20:39:51'),
(95, 133, 132, 80, 34, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 20:40:07', '2019-11-24 20:41:23'),
(96, 133, 132, 80, 35, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 20:41:31', '2019-11-24 20:42:02'),
(97, 133, 132, 80, 36, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 20:42:06', '2019-11-24 20:45:35'),
(98, 133, 132, 80, 37, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 20:45:41', '2019-11-24 20:46:44'),
(99, 133, 132, 80, 38, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 20:46:50', '2019-11-24 20:49:02'),
(100, 133, 132, 80, 39, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 20:49:08', '2019-11-24 20:51:37'),
(101, 133, 132, 80, 40, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 20:51:31', '2019-11-24 20:51:37'),
(102, 133, 132, 80, 41, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 20:51:44', '2019-11-24 20:52:05'),
(103, 133, 132, 80, 42, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 20:53:28', '2019-11-24 20:53:34'),
(104, 133, 132, 80, 43, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 20:53:48', '2019-11-24 20:53:53'),
(105, 133, 132, 80, 44, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 21:03:24', '2019-11-24 21:03:28'),
(106, 133, 132, 80, 45, 'سوم الإعلانات', ' قام المستخدم ahmed بتقديم سوم بمبلغ 5 علي الإعلان ( نيسان )', 5, 3, 1, '2019-11-24 21:03:59', '2019-11-24 21:04:00'),
(107, 131, 133, 78, NULL, ' التعليقات ', 'maher  قام بالتعليق علي إعلانك ', 0, 21, 0, '2019-11-25 17:57:12', '2019-11-25 17:57:12'),
(108, 131, 133, 78, NULL, ' التعليقات ', 'maher  قام بالتعليق علي إعلانك ', 0, 21, 0, '2019-11-25 18:02:51', '2019-11-25 18:02:51'),
(109, 131, 133, 78, NULL, ' التعليقات ', 'maher  قام بالتعليق علي إعلانك ', 0, 21, 0, '2019-11-25 18:03:10', '2019-11-25 18:03:10'),
(110, 1, 1, 0, NULL, 'اهلا', 'اهلا', 0, 15, 0, '2019-11-26 20:10:37', '2019-11-26 20:10:37'),
(111, 129, 1, 0, NULL, 'اهلا', 'اهلا', 0, 15, 1, '2019-11-26 20:10:37', '2019-11-26 20:11:18'),
(112, 130, 1, 0, NULL, 'اهلا', 'اهلا', 0, 15, 1, '2019-11-26 20:10:37', '2019-11-26 20:11:05'),
(113, 132, 1, 0, NULL, 'اهلا', 'اهلا', 0, 15, 1, '2019-11-26 20:10:37', '2019-11-26 22:03:12'),
(114, 133, 1, 0, NULL, 'اهلا', 'اهلا', 0, 15, 0, '2019-11-26 20:10:37', '2019-11-26 20:10:37'),
(115, 1, 1, 0, NULL, 'اشعار تجريبي', 'اشعار تجريبي', 0, 15, 0, '2019-11-26 20:13:15', '2019-11-26 20:13:15'),
(116, 129, 1, 0, NULL, 'اشعار تجريبي', 'اشعار تجريبي', 0, 15, 1, '2019-11-26 20:13:15', '2019-11-26 20:15:30'),
(117, 130, 1, 0, NULL, 'اشعار تجريبي', 'اشعار تجريبي', 0, 15, 1, '2019-11-26 20:13:15', '2019-11-26 20:15:41'),
(118, 132, 1, 0, NULL, 'اشعار تجريبي', 'اشعار تجريبي', 0, 15, 1, '2019-11-26 20:13:15', '2019-11-26 22:03:11'),
(119, 133, 1, 0, NULL, 'اشعار تجريبي', 'اشعار تجريبي', 0, 15, 0, '2019-11-26 20:13:15', '2019-11-26 20:13:15'),
(120, 1, 1, 0, NULL, 'اشعار تجريبي', 'اشعار تجريبي', 0, 15, 0, '2019-11-26 20:13:53', '2019-11-26 20:13:53'),
(121, 129, 1, 0, NULL, 'اشعار تجريبي', 'اشعار تجريبي', 0, 15, 1, '2019-11-26 20:13:53', '2019-11-26 20:15:30'),
(122, 130, 1, 0, NULL, 'اشعار تجريبي', 'اشعار تجريبي', 0, 15, 1, '2019-11-26 20:13:53', '2019-11-26 20:15:41'),
(123, 132, 1, 0, NULL, 'اشعار تجريبي', 'اشعار تجريبي', 0, 15, 1, '2019-11-26 20:13:53', '2019-11-26 22:03:11'),
(124, 133, 1, 0, NULL, 'اشعار تجريبي', 'اشعار تجريبي', 0, 15, 0, '2019-11-26 20:13:53', '2019-11-26 20:13:53'),
(125, 1, 1, 0, NULL, 'جديد', 'جديد', 0, 15, 0, '2019-11-26 20:15:09', '2019-11-26 20:15:09'),
(126, 129, 1, 0, NULL, 'جديد', 'جديد', 0, 15, 1, '2019-11-26 20:15:09', '2019-11-26 20:15:30'),
(127, 130, 1, 0, NULL, 'جديد', 'جديد', 0, 15, 1, '2019-11-26 20:15:09', '2019-11-26 20:15:41'),
(128, 132, 1, 0, NULL, 'جديد', 'جديد', 0, 15, 1, '2019-11-26 20:15:09', '2019-11-26 22:03:11'),
(129, 133, 1, 0, NULL, 'جديد', 'جديد', 0, 15, 0, '2019-11-26 20:15:09', '2019-11-26 20:15:09'),
(130, 1, 1, 0, NULL, 'مرحبا 120', 'برحبا120', 0, 15, 0, '2019-11-26 20:17:40', '2019-11-26 20:17:40'),
(131, 129, 1, 0, NULL, 'مرحبا 120', 'برحبا120', 0, 15, 1, '2019-11-26 20:17:40', '2019-11-26 20:20:15'),
(132, 130, 1, 0, NULL, 'مرحبا 120', 'برحبا120', 0, 15, 1, '2019-11-26 20:17:40', '2019-11-26 20:25:20'),
(133, 132, 1, 0, NULL, 'مرحبا 120', 'برحبا120', 0, 15, 1, '2019-11-26 20:17:40', '2019-11-26 22:03:11'),
(134, 133, 1, 0, NULL, 'مرحبا 120', 'برحبا120', 0, 15, 0, '2019-11-26 20:17:40', '2019-11-26 20:17:40'),
(135, 1, 1, 0, NULL, 'اشعار تجريبي', 'اشعار تجريبي', 0, 15, 0, '2019-11-26 20:19:39', '2019-11-26 20:19:39'),
(136, 129, 1, 0, NULL, 'اشعار تجريبي', 'اشعار تجريبي', 0, 15, 1, '2019-11-26 20:19:39', '2019-11-26 20:20:15'),
(137, 130, 1, 0, NULL, 'اشعار تجريبي', 'اشعار تجريبي', 0, 15, 1, '2019-11-26 20:19:39', '2019-11-26 20:25:20'),
(138, 132, 1, 0, NULL, 'اشعار تجريبي', 'اشعار تجريبي', 0, 15, 1, '2019-11-26 20:19:39', '2019-11-26 22:03:11'),
(139, 133, 1, 0, NULL, 'اشعار تجريبي', 'اشعار تجريبي', 0, 15, 0, '2019-11-26 20:19:39', '2019-11-26 20:19:39'),
(140, 1, 1, 0, NULL, '1290', '1299', 0, 15, 0, '2019-11-26 20:21:28', '2019-11-26 20:21:28'),
(141, 129, 1, 0, NULL, '1290', '1299', 0, 15, 1, '2019-11-26 20:21:28', '2019-11-26 20:21:38'),
(142, 130, 1, 0, NULL, '1290', '1299', 0, 15, 1, '2019-11-26 20:21:28', '2019-11-26 20:25:20'),
(143, 132, 1, 0, NULL, '1290', '1299', 0, 15, 1, '2019-11-26 20:21:28', '2019-11-26 22:03:11'),
(144, 133, 1, 0, NULL, '1290', '1299', 0, 15, 0, '2019-11-26 20:21:28', '2019-11-26 20:21:28'),
(145, 1, 1, 0, NULL, 'متبمهثتبخثسصهت', 'تبحخثستبحثصشىعرقش', 0, 15, 0, '2019-11-26 20:21:58', '2019-11-26 20:21:58'),
(146, 129, 1, 0, NULL, 'متبمهثتبخثسصهت', 'تبحخثستبحثصشىعرقش', 0, 15, 1, '2019-11-26 20:21:58', '2019-11-26 20:22:20'),
(147, 130, 1, 0, NULL, 'متبمهثتبخثسصهت', 'تبحخثستبحثصشىعرقش', 0, 15, 1, '2019-11-26 20:21:58', '2019-11-26 20:25:20'),
(148, 132, 1, 0, NULL, 'متبمهثتبخثسصهت', 'تبحخثستبحثصشىعرقش', 0, 15, 1, '2019-11-26 20:21:58', '2019-11-26 22:03:11'),
(149, 133, 1, 0, NULL, 'متبمهثتبخثسصهت', 'تبحخثستبحثصشىعرقش', 0, 15, 0, '2019-11-26 20:21:58', '2019-11-26 20:21:58'),
(150, 130, 129, 76, 46, 'سوم الإعلانات', ' قام المستخدم عبدالعزيز الدوسري بتقديم سوم بمبلغ 4005 علي الإعلان ( فله في الرياض )', 4005, 3, 1, '2019-11-26 20:25:05', '2019-11-26 20:25:20'),
(151, 129, 130, 76, 46, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان فله في الرياض', 0, 4, 1, '2019-11-26 20:25:16', '2019-11-26 20:28:46'),
(152, 130, 129, 76, 47, 'سوم الإعلانات', ' قام المستخدم عبدالعزيز الدوسري بتقديم سوم بمبلغ 5000033 علي الإعلان ( فله في الرياض )', 5000030, 3, 1, '2019-11-26 20:25:46', '2019-11-26 20:25:53'),
(153, 129, 130, 76, 47, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان فله في الرياض', 0, 4, 1, '2019-11-26 20:25:56', '2019-11-26 20:28:46'),
(154, 132, 130, 82, 48, 'سوم الإعلانات', ' قام المستخدم hesham بتقديم سوم بمبلغ 788 علي الإعلان ( شيفروليه )', 788, 3, 1, '2019-11-26 22:37:29', '2019-11-27 00:46:53'),
(155, 130, 132, 82, 48, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان شيفروليه', 0, 4, 1, '2019-11-26 22:38:48', '2020-01-09 16:50:39'),
(156, 132, 130, 82, 49, 'سوم الإعلانات', ' قام المستخدم hesham بتقديم سوم بمبلغ 7666 علي الإعلان ( شيفروليه )', 7666, 3, 1, '2019-11-26 22:46:34', '2019-11-27 00:46:53'),
(157, 130, 132, 82, 49, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان شيفروليه', 0, 4, 1, '2019-11-26 22:46:55', '2020-01-09 16:50:39'),
(158, 132, 130, 82, 50, 'سوم الإعلانات', ' قام المستخدم hesham بتقديم سوم بمبلغ 33333 علي الإعلان ( شيفروليه )', 33333, 3, 1, '2019-11-26 22:51:01', '2019-11-27 00:46:53'),
(159, 130, 132, 82, 50, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان شيفروليه', 0, 4, 1, '2019-11-26 22:51:15', '2020-01-09 16:50:39'),
(160, 132, 130, 82, 51, 'سوم الإعلانات', ' قام المستخدم hesham بتقديم سوم بمبلغ 44 علي الإعلان ( شيفروليه )', 44, 3, 1, '2019-11-26 22:52:04', '2019-11-27 00:46:53'),
(161, 130, 132, 82, 51, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان شيفروليه', 0, 4, 1, '2019-11-26 22:52:07', '2020-01-09 16:50:39'),
(162, 132, 130, 82, 52, 'سوم الإعلانات', ' قام المستخدم hesham بتقديم سوم بمبلغ 5555 علي الإعلان ( شيفروليه )', 5555, 3, 1, '2019-11-26 22:53:00', '2019-11-27 00:46:53'),
(163, 130, 132, 82, 52, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان شيفروليه', 0, 4, 1, '2019-11-26 22:53:03', '2020-01-09 16:50:39'),
(164, 132, 130, 82, 53, 'سوم الإعلانات', ' قام المستخدم hesham بتقديم سوم بمبلغ 88 علي الإعلان ( شيفروليه )', 88, 3, 1, '2019-11-26 22:55:29', '2019-11-27 00:46:52'),
(165, 130, 132, 82, 53, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان شيفروليه', 0, 4, 1, '2019-11-26 22:55:36', '2020-01-09 16:50:39'),
(166, 132, 130, 82, 54, 'سوم الإعلانات', ' قام المستخدم hesham بتقديم سوم بمبلغ 3322 علي الإعلان ( شيفروليه )', 3322, 3, 1, '2019-11-26 23:07:22', '2019-11-27 00:46:52'),
(167, 130, 132, 82, 54, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان شيفروليه', 0, 4, 1, '2019-11-26 23:07:26', '2020-01-09 16:50:39'),
(168, 132, 130, 82, 55, 'سوم الإعلانات', ' قام المستخدم hesham بتقديم سوم بمبلغ 333 علي الإعلان ( شيفروليه )', 333, 3, 1, '2019-11-26 23:08:20', '2019-11-27 00:46:52'),
(169, 130, 132, 82, 55, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان شيفروليه', 0, 4, 1, '2019-11-26 23:08:25', '2020-01-09 16:50:39'),
(170, 132, 130, 82, 56, 'سوم الإعلانات', ' قام المستخدم hesham بتقديم سوم بمبلغ 55 علي الإعلان ( شيفروليه )', 55, 3, 1, '2019-11-26 23:08:43', '2019-11-27 00:46:52'),
(171, 130, 132, 82, 56, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان شيفروليه', 0, 4, 1, '2019-11-26 23:08:46', '2020-01-09 16:50:39'),
(172, 132, 130, 82, 57, 'سوم الإعلانات', ' قام المستخدم hesham بتقديم سوم بمبلغ 8 علي الإعلان ( شيفروليه )', 8, 3, 1, '2019-11-26 23:09:52', '2019-11-27 00:46:52'),
(173, 130, 132, 82, 57, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان شيفروليه', 0, 4, 1, '2019-11-26 23:09:57', '2020-01-09 16:50:39'),
(174, 1, 1, 0, NULL, 'اعلان', 'جديد', 0, 15, 0, '2019-11-27 23:08:07', '2019-11-27 23:08:07'),
(175, 129, 1, 0, NULL, 'اعلان', 'جديد', 0, 15, 1, '2019-11-27 23:08:07', '2019-11-27 23:08:17'),
(176, 130, 1, 0, NULL, 'اعلان', 'جديد', 0, 15, 1, '2019-11-27 23:08:07', '2020-01-09 16:50:39'),
(177, 132, 1, 0, NULL, 'اعلان', 'جديد', 0, 15, 1, '2019-11-27 23:08:07', '2019-11-28 11:47:11'),
(178, 133, 1, 0, NULL, 'اعلان', 'جديد', 0, 15, 0, '2019-11-27 23:08:07', '2019-11-27 23:08:07'),
(179, 1, 1, 0, NULL, 'خاص', 'اهلا', 0, 15, 0, '2019-11-27 23:08:42', '2019-11-27 23:08:42'),
(180, 129, 1, 0, NULL, 'خاص', 'اهلا', 0, 15, 1, '2019-11-27 23:08:42', '2019-11-27 23:10:47'),
(181, 130, 1, 0, NULL, 'خاص', 'اهلا', 0, 15, 1, '2019-11-27 23:08:42', '2020-01-09 16:50:39'),
(182, 132, 1, 0, NULL, 'خاص', 'اهلا', 0, 15, 1, '2019-11-27 23:08:42', '2019-11-28 11:47:11'),
(183, 133, 1, 0, NULL, 'خاص', 'اهلا', 0, 15, 0, '2019-11-27 23:08:42', '2019-11-27 23:08:42'),
(184, 1, 1, 0, NULL, 'مرحبا', 'تجربة التطبيق', 0, 15, 0, '2019-11-27 23:08:59', '2019-11-27 23:08:59'),
(185, 129, 1, 0, NULL, 'مرحبا', 'تجربة التطبيق', 0, 15, 1, '2019-11-27 23:08:59', '2019-11-27 23:10:47'),
(186, 130, 1, 0, NULL, 'مرحبا', 'تجربة التطبيق', 0, 15, 1, '2019-11-27 23:08:59', '2020-01-09 16:50:39'),
(187, 132, 1, 0, NULL, 'مرحبا', 'تجربة التطبيق', 0, 15, 1, '2019-11-27 23:08:59', '2019-11-28 11:47:11'),
(188, 133, 1, 0, NULL, 'مرحبا', 'تجربة التطبيق', 0, 15, 0, '2019-11-27 23:08:59', '2019-11-27 23:08:59'),
(189, 1, 1, 0, NULL, 'تجربة', 'جديد', 0, 15, 0, '2019-11-27 23:11:03', '2019-11-27 23:11:03'),
(190, 129, 1, 0, NULL, 'تجربة', 'جديد', 0, 15, 1, '2019-11-27 23:11:03', '2019-11-27 23:11:10'),
(191, 130, 1, 0, NULL, 'تجربة', 'جديد', 0, 15, 1, '2019-11-27 23:11:03', '2020-01-09 16:50:39'),
(192, 132, 1, 0, NULL, 'تجربة', 'جديد', 0, 15, 1, '2019-11-27 23:11:03', '2019-11-28 11:47:11'),
(193, 133, 1, 0, NULL, 'تجربة', 'جديد', 0, 15, 0, '2019-11-27 23:11:03', '2019-11-27 23:11:03'),
(194, 1, 1, 0, NULL, 'تجربة', 'اخرى', 0, 15, 0, '2019-11-27 23:11:39', '2019-11-27 23:11:39'),
(195, 129, 1, 0, NULL, 'تجربة', 'اخرى', 0, 15, 1, '2019-11-27 23:11:39', '2019-11-27 23:12:03'),
(196, 130, 1, 0, NULL, 'تجربة', 'اخرى', 0, 15, 1, '2019-11-27 23:11:39', '2020-01-09 16:50:39'),
(197, 132, 1, 0, NULL, 'تجربة', 'اخرى', 0, 15, 1, '2019-11-27 23:11:39', '2019-11-28 11:47:11'),
(198, 133, 1, 0, NULL, 'تجربة', 'اخرى', 0, 15, 0, '2019-11-27 23:11:39', '2019-11-27 23:11:39'),
(199, 1, 1, 0, NULL, 'تجربة', 'اخضر', 0, 15, 0, '2019-11-27 23:12:49', '2019-11-27 23:12:49'),
(200, 129, 1, 0, NULL, 'تجربة', 'اخضر', 0, 15, 1, '2019-11-27 23:12:49', '2019-11-27 23:25:58'),
(201, 130, 1, 0, NULL, 'تجربة', 'اخضر', 0, 15, 1, '2019-11-27 23:12:49', '2020-01-09 16:50:39'),
(202, 132, 1, 0, NULL, 'تجربة', 'اخضر', 0, 15, 1, '2019-11-27 23:12:49', '2019-11-28 11:47:11'),
(203, 133, 1, 0, NULL, 'تجربة', 'اخضر', 0, 15, 0, '2019-11-27 23:12:49', '2019-11-27 23:12:49'),
(204, 1, 129, 76, NULL, 'تقارير الإعلانات', 'الاعلان غير صحيح', 0, 0, 0, '2019-11-27 23:18:11', '2019-11-27 23:18:11'),
(205, 1, 129, 76, NULL, 'تقارير الإعلانات', 'الاعلان غير صحيح', 0, 0, 0, '2019-11-27 23:18:11', '2019-11-27 23:18:11'),
(206, 1, 129, 76, NULL, 'تقارير الإعلانات', 'الاعلان غير صحيح', 0, 0, 0, '2019-11-27 23:18:12', '2019-11-27 23:18:12'),
(207, 1, 129, 76, NULL, 'تقارير الإعلانات', 'الاعلان غير صحيح', 0, 0, 0, '2019-11-27 23:18:12', '2019-11-27 23:18:12'),
(208, 1, 129, 76, NULL, 'تقارير الإعلانات', 'الاعلان غير صحيح', 0, 0, 0, '2019-11-27 23:18:12', '2019-11-27 23:18:12'),
(209, 1, 130, 83, NULL, 'تقارير الإعلانات', 'شكوى', 0, 0, 0, '2019-11-28 10:39:20', '2019-11-28 10:39:20'),
(210, 1, 130, 83, NULL, 'تقارير الإعلانات', 'شكوى', 0, 0, 0, '2019-11-28 10:39:21', '2019-11-28 10:39:21'),
(211, 1, 130, 83, NULL, 'تقارير الإعلانات', 'شكوى', 0, 0, 0, '2019-11-28 10:39:21', '2019-11-28 10:39:21'),
(212, 1, 130, 83, NULL, 'تقارير الإعلانات', 'شكوى', 0, 0, 0, '2019-11-28 10:39:21', '2019-11-28 10:39:21'),
(213, 1, 130, 83, NULL, 'تقارير الإعلانات', 'شكوى', 0, 0, 0, '2019-11-28 10:39:21', '2019-11-28 10:39:21'),
(214, 1, 129, NULL, NULL, 'global.connect_us', 'اقتراح', 0, 2, 0, '2020-01-11 22:04:20', '2020-01-11 22:04:20'),
(215, 1, 129, NULL, NULL, 'global.connect_us', 'اقتراح', 0, 2, 0, '2020-01-11 22:04:20', '2020-01-11 22:04:20'),
(216, 1, 129, NULL, NULL, 'global.connect_us', 'اقتراح', 0, 2, 0, '2020-01-11 22:04:20', '2020-01-11 22:04:20'),
(217, 1, 129, NULL, NULL, 'global.connect_us', 'اقتراح', 0, 2, 0, '2020-01-11 22:04:20', '2020-01-11 22:04:20'),
(218, 1, 129, NULL, NULL, 'global.connect_us', 'اقتراح', 0, 2, 0, '2020-01-11 22:04:20', '2020-01-11 22:04:20'),
(219, 1, 1, 0, NULL, 'ملاحبا', 'مرحبا', 0, 15, 0, '2020-01-11 22:06:24', '2020-01-11 22:06:24'),
(220, 129, 1, 0, NULL, 'ملاحبا', 'مرحبا', 0, 15, 1, '2020-01-11 22:06:24', '2020-01-11 22:07:03'),
(221, 130, 1, 0, NULL, 'ملاحبا', 'مرحبا', 0, 15, 1, '2020-01-11 22:06:24', '2020-03-20 04:07:59'),
(222, 131, 1, 0, NULL, 'ملاحبا', 'مرحبا', 0, 15, 0, '2020-01-11 22:06:24', '2020-01-11 22:06:24'),
(223, 132, 1, 0, NULL, 'ملاحبا', 'مرحبا', 0, 15, 0, '2020-01-11 22:06:24', '2020-01-11 22:06:24'),
(224, 133, 1, 0, NULL, 'ملاحبا', 'مرحبا', 0, 15, 0, '2020-01-11 22:06:24', '2020-01-11 22:06:24'),
(225, 134, 1, 0, NULL, 'ملاحبا', 'مرحبا', 0, 15, 1, '2020-01-11 22:06:24', '2020-03-21 00:11:28'),
(226, 136, 1, 0, NULL, 'ملاحبا', 'مرحبا', 0, 15, 0, '2020-01-11 22:06:24', '2020-01-11 22:06:24'),
(227, 1, 1, 0, NULL, 'تجربة', 'تجربة', 0, 15, 0, '2020-01-11 22:06:45', '2020-01-11 22:06:45'),
(228, 129, 1, 0, NULL, 'تجربة', 'تجربة', 0, 15, 1, '2020-01-11 22:06:45', '2020-01-11 22:07:03'),
(229, 130, 1, 0, NULL, 'تجربة', 'تجربة', 0, 15, 1, '2020-01-11 22:06:45', '2020-03-20 04:07:59'),
(230, 131, 1, 0, NULL, 'تجربة', 'تجربة', 0, 15, 0, '2020-01-11 22:06:45', '2020-01-11 22:06:45'),
(231, 132, 1, 0, NULL, 'تجربة', 'تجربة', 0, 15, 0, '2020-01-11 22:06:45', '2020-01-11 22:06:45'),
(232, 133, 1, 0, NULL, 'تجربة', 'تجربة', 0, 15, 0, '2020-01-11 22:06:45', '2020-01-11 22:06:45'),
(233, 134, 1, 0, NULL, 'تجربة', 'تجربة', 0, 15, 1, '2020-01-11 22:06:45', '2020-03-21 00:11:28'),
(234, 136, 1, 0, NULL, 'تجربة', 'تجربة', 0, 15, 0, '2020-01-11 22:06:45', '2020-01-11 22:06:45'),
(235, 1, 1, 0, NULL, 'هلا', 'هلا هلا', 0, 15, 0, '2020-01-11 22:07:16', '2020-01-11 22:07:16'),
(236, 129, 1, 0, NULL, 'هلا', 'هلا هلا', 0, 15, 1, '2020-01-11 22:07:16', '2020-01-11 22:08:14'),
(237, 130, 1, 0, NULL, 'هلا', 'هلا هلا', 0, 15, 1, '2020-01-11 22:07:16', '2020-03-20 04:07:59'),
(238, 131, 1, 0, NULL, 'هلا', 'هلا هلا', 0, 15, 0, '2020-01-11 22:07:16', '2020-01-11 22:07:16'),
(239, 132, 1, 0, NULL, 'هلا', 'هلا هلا', 0, 15, 0, '2020-01-11 22:07:16', '2020-01-11 22:07:16'),
(240, 133, 1, 0, NULL, 'هلا', 'هلا هلا', 0, 15, 0, '2020-01-11 22:07:16', '2020-01-11 22:07:16'),
(241, 134, 1, 0, NULL, 'هلا', 'هلا هلا', 0, 15, 1, '2020-01-11 22:07:16', '2020-03-21 00:11:28'),
(242, 136, 1, 0, NULL, 'هلا', 'هلا هلا', 0, 15, 0, '2020-01-11 22:07:16', '2020-01-11 22:07:16'),
(243, 1, 1, 0, NULL, 'تت', 'كحنطكنطجح', 0, 15, 0, '2020-01-11 22:10:02', '2020-01-11 22:10:02'),
(244, 129, 1, 0, NULL, 'تت', 'كحنطكنطجح', 0, 15, 1, '2020-01-11 22:10:02', '2020-01-11 22:13:59'),
(245, 130, 1, 0, NULL, 'تت', 'كحنطكنطجح', 0, 15, 1, '2020-01-11 22:10:02', '2020-03-20 04:07:59'),
(246, 131, 1, 0, NULL, 'تت', 'كحنطكنطجح', 0, 15, 0, '2020-01-11 22:10:02', '2020-01-11 22:10:02'),
(247, 132, 1, 0, NULL, 'تت', 'كحنطكنطجح', 0, 15, 0, '2020-01-11 22:10:02', '2020-01-11 22:10:02'),
(248, 133, 1, 0, NULL, 'تت', 'كحنطكنطجح', 0, 15, 0, '2020-01-11 22:10:02', '2020-01-11 22:10:02'),
(249, 134, 1, 0, NULL, 'تت', 'كحنطكنطجح', 0, 15, 1, '2020-01-11 22:10:02', '2020-03-21 00:11:28'),
(250, 136, 1, 0, NULL, 'تت', 'كحنطكنطجح', 0, 15, 0, '2020-01-11 22:10:02', '2020-01-11 22:10:02'),
(251, 1, 1, 0, NULL, 'تخثهتبحث', 'تسلحخنتثسق', 0, 15, 0, '2020-01-11 22:14:11', '2020-01-11 22:14:11'),
(252, 129, 1, 0, NULL, 'تخثهتبحث', 'تسلحخنتثسق', 0, 15, 1, '2020-01-11 22:14:11', '2020-01-11 22:16:20'),
(253, 130, 1, 0, NULL, 'تخثهتبحث', 'تسلحخنتثسق', 0, 15, 1, '2020-01-11 22:14:11', '2020-03-20 04:07:59'),
(254, 131, 1, 0, NULL, 'تخثهتبحث', 'تسلحخنتثسق', 0, 15, 0, '2020-01-11 22:14:11', '2020-01-11 22:14:11'),
(255, 132, 1, 0, NULL, 'تخثهتبحث', 'تسلحخنتثسق', 0, 15, 0, '2020-01-11 22:14:11', '2020-01-11 22:14:11'),
(256, 133, 1, 0, NULL, 'تخثهتبحث', 'تسلحخنتثسق', 0, 15, 0, '2020-01-11 22:14:11', '2020-01-11 22:14:11'),
(257, 134, 1, 0, NULL, 'تخثهتبحث', 'تسلحخنتثسق', 0, 15, 1, '2020-01-11 22:14:11', '2020-03-21 00:11:28'),
(258, 136, 1, 0, NULL, 'تخثهتبحث', 'تسلحخنتثسق', 0, 15, 0, '2020-01-11 22:14:11', '2020-01-11 22:14:11'),
(259, 1, 130, NULL, NULL, 'global.connect_us', 'اضف شكوي او اقتراح', 0, 2, 0, '2020-01-21 18:18:45', '2020-01-21 18:18:45'),
(260, 1, 130, NULL, NULL, 'global.connect_us', 'اضف شكوي او اقتراح', 0, 2, 0, '2020-01-21 18:18:45', '2020-01-21 18:18:45'),
(261, 1, 130, NULL, NULL, 'global.connect_us', 'اضف شكوي او اقتراح', 0, 2, 0, '2020-01-21 18:18:45', '2020-01-21 18:18:45'),
(262, 1, 130, NULL, NULL, 'global.connect_us', 'اضف شكوي او اقتراح', 0, 2, 0, '2020-01-21 18:18:45', '2020-01-21 18:18:45'),
(263, 1, 130, NULL, NULL, 'global.connect_us', 'اضف شكوي او اقتراح', 0, 2, 0, '2020-01-21 18:18:45', '2020-01-21 18:18:45'),
(264, 1, 129, NULL, NULL, 'global.connect_us', 'مشكلة', 0, 2, 0, '2020-03-14 21:07:02', '2020-03-14 21:07:02'),
(265, 1, 129, NULL, NULL, 'global.connect_us', 'مشكلة', 0, 2, 0, '2020-03-14 21:07:02', '2020-03-14 21:07:02'),
(266, 1, 129, NULL, NULL, 'global.connect_us', 'مشكلة', 0, 2, 0, '2020-03-14 21:07:02', '2020-03-14 21:07:02'),
(267, 1, 129, NULL, NULL, 'global.connect_us', 'مشكلة', 0, 2, 0, '2020-03-14 21:07:02', '2020-03-14 21:07:02'),
(268, 1, 129, NULL, NULL, 'global.connect_us', 'مشكلة', 0, 2, 0, '2020-03-14 21:07:02', '2020-03-14 21:07:02'),
(269, 1, 129, NULL, NULL, 'global.connect_us', 'شكوي', 0, 2, 0, '2020-03-18 20:01:08', '2020-03-18 20:01:08'),
(270, 1, 129, NULL, NULL, 'global.connect_us', 'شكوي', 0, 2, 0, '2020-03-18 20:01:08', '2020-03-18 20:01:08'),
(271, 1, 129, NULL, NULL, 'global.connect_us', 'شكوي', 0, 2, 0, '2020-03-18 20:01:08', '2020-03-18 20:01:08'),
(272, 1, 129, NULL, NULL, 'global.connect_us', 'شكوي', 0, 2, 0, '2020-03-18 20:01:08', '2020-03-18 20:01:08'),
(273, 1, 129, NULL, NULL, 'global.connect_us', 'شكوي', 0, 2, 0, '2020-03-18 20:01:08', '2020-03-18 20:01:08'),
(274, 141, 129, 99, 58, 'سوم الإعلانات', ' قام المستخدم عبدالعزيز الدوسري بتقديم سوم بمبلغ 100 علي الإعلان ( بيع مكيفات مستعمل وجديد )', 100, 3, 1, '2020-03-20 03:32:51', '2020-03-20 03:33:13'),
(275, 129, 141, 99, 58, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان بيع مكيفات مستعمل وجديد', 0, 4, 1, '2020-03-20 03:33:24', '2020-03-20 03:34:03'),
(276, 141, 129, 99, 59, 'سوم الإعلانات', ' قام المستخدم عبدالعزيز الدوسري بتقديم سوم بمبلغ 200 علي الإعلان ( بيع مكيفات مستعمل وجديد )', 200, 3, 1, '2020-03-20 03:38:58', '2020-03-20 03:39:15'),
(277, 129, 141, 99, 59, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان بيع مكيفات مستعمل وجديد', 0, 4, 1, '2020-03-20 03:39:20', '2020-03-20 03:39:30'),
(278, 141, 129, 99, 60, 'سوم الإعلانات', ' قام المستخدم عبدالعزيز الدوسري بتقديم سوم بمبلغ 976453 علي الإعلان ( بيع مكيفات مستعمل وجديد )', 976453, 3, 1, '2020-03-20 04:03:19', '2020-03-20 04:03:31'),
(279, 129, 141, 99, 60, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان بيع مكيفات مستعمل وجديد', 0, 4, 1, '2020-03-20 04:03:38', '2020-03-20 04:04:50'),
(280, 130, 129, 100, 61, 'سوم الإعلانات', ' قام المستخدم عبدالعزيز الدوسري بتقديم سوم بمبلغ 646454 علي الإعلان ( عقار جديد )', 646454, 3, 1, '2020-03-20 04:06:14', '2020-03-20 04:07:59'),
(281, 129, 130, 100, 61, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان عقار جديد', 0, 4, 1, '2020-03-20 04:08:05', '2020-03-20 04:10:16'),
(282, 141, 130, 99, 62, 'سوم الإعلانات', ' قام المستخدم hesham بتقديم سوم بمبلغ 500 علي الإعلان ( بيع مكيفات مستعمل وجديد )', 500, 3, 1, '2020-03-20 04:18:22', '2020-03-20 05:23:03'),
(283, 130, 141, 99, 62, 'سوم الإعلانات', ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان بيع مكيفات مستعمل وجديد', 0, 4, 0, '2020-03-20 05:23:10', '2020-03-20 05:23:10'),
(284, 129, 134, 98, 63, 'سوم الإعلانات', ' قام المستخدم maryam بتقديم سوم بمبلغ 6464 علي الإعلان ( فلل )', 6464, 3, 1, '2020-03-21 00:09:44', '2020-03-27 18:59:41');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('ramyahmedhandousa@gmail.com', '$2y$10$mvRYD7feFPpT/OJJ2H6nROUEjyYwghQb.O5u/yq90RBHQw2B9.PYO', '2019-01-16 12:30:38'),
('adminHelper@admin.com', '$2y$10$iO7ouR7i7sx006ChhUQ4U.GM1EeydtjOgXU4SwxMyxV8MQgQuGUEy', '2019-03-26 14:15:38'),
('sell4me.est@hotmail.com', '$2y$10$lBN3jNfyY.vvGn7CN4SCqe8OpyYOoCxndv6wXHRpmlP.G44iskfZy', '2020-01-10 01:20:03');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `ability_id` int(10) UNSIGNED NOT NULL,
  `entity_id` int(10) UNSIGNED NOT NULL,
  `entity_type` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `forbidden` tinyint(1) NOT NULL DEFAULT '0',
  `scope` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`ability_id`, `entity_id`, `entity_type`, `forbidden`, `scope`) VALUES
(4, 1, 'roles', 0, NULL),
(2, 1, 'App\\User', 0, NULL),
(2, 27, 'roles', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `national_card_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `national_card_number` bigint(11) DEFAULT NULL,
  `license_owner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `license_driver` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `truck_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `truck_model` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `truck_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app_percentage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(10) UNSIGNED NOT NULL,
  `rateable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rateable_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `rating` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(10) UNSIGNED DEFAULT NULL,
  `scope` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `title`, `level`, `scope`, `created_at`, `updated_at`) VALUES
(1, '*', 'owner', NULL, NULL, NULL, NULL),
(4, 'مدير أول', 'مدير أول', NULL, NULL, '2018-07-22 06:55:22', '2018-12-04 13:47:44'),
(5, 'أدمن عام', 'أدمن عام', NULL, NULL, '2018-07-22 09:50:56', '2018-07-22 09:50:56'),
(17, 'اداره الاحصائيات و اداره التواصل فقط', 'اداره الاحصائيات و اداره التواصل فقط', NULL, NULL, '2018-09-15 19:16:41', '2018-09-15 19:16:41'),
(18, 'التحكم في ألاقسام', 'التحكم في ألاقسام', NULL, NULL, '2018-12-02 14:20:22', '2018-12-02 14:20:22'),
(19, 'التحكم في العملاء', 'التحكم في العملاء', NULL, NULL, '2018-12-02 14:20:35', '2018-12-02 14:20:35'),
(20, 'التحكم في إرسال إشعارات\r\n', 'التحكم في إرسال إشعارات\r\n', NULL, NULL, '2018-12-02 14:20:35', '2018-12-02 14:20:35'),
(21, 'الإطلاع على التقارير', 'الإطلاع على التقارير', NULL, NULL, '2018-12-02 14:20:35', '2018-12-02 14:20:35'),
(22, 'التحكم في تعديل إلاعدادات.', 'التحكم في تعديل إلاعدادات.', NULL, NULL, '2018-12-02 14:20:35', '2018-12-02 14:20:35'),
(23, 'التحكم في تأكيد أو عدم تأكيد التحويالت البنكية', 'التحكم في تأكيد أو عدم تأكيد التحويالت البنكية', NULL, NULL, '2018-12-02 14:20:35', '2018-12-02 14:20:35'),
(24, 'التحكم في رسائل تواصل معانا', 'التحكم في رسائل تواصل معانا', NULL, NULL, '2018-12-02 14:20:35', '2018-12-02 14:20:35'),
(25, 'التحكم في تعديل وعرض محتوى إدارة محتوى التطبيق.', 'التحكم في تعديل وعرض محتوى إدارة محتوى التطبيق. ', NULL, NULL, '2018-12-02 14:20:35', '2018-12-02 14:20:35'),
(26, 'التحكم في إرسال إشعارات', 'التحكم في إرسال إشعارات', NULL, NULL, '2019-07-20 02:14:32', '2019-07-20 02:14:32');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `body`, `created_at`, `updated_at`) VALUES
(1, 'contactus_phone', '966570006749', '2019-03-26 19:52:22', '2019-12-22 19:40:49'),
(2, 'contactus_email', 'Advertisement@gmail.com', '2019-03-26 19:52:22', '2019-11-19 20:00:39'),
(3, 'contactus_facebook', '', '2019-03-26 19:52:22', '2019-07-24 05:05:41'),
(4, 'contactus_twitter', '', '2019-03-26 19:52:22', '2019-07-24 05:05:41'),
(5, 'contactus_google', '', '2019-03-26 19:52:22', '2019-07-24 05:05:41'),
(6, 'contactus_linkedin', '', '2019-03-26 19:52:22', '2019-07-24 05:05:41'),
(7, 'contactus_snapchat', '', '2019-03-26 19:52:22', '2019-07-24 05:05:41'),
(8, 'contactus_instagram', '', '2019-03-26 19:52:22', '2019-07-24 05:05:41'),
(9, 'about_app_desc_ar', 'تطبيق اعلانات للبيع و الشراء\r\nيوفر تصنيفات رئيسية مختلفة للمستخدمين ليقوموا بإضافة اعلاناتهم حتى يستطيع المستخدمين الاخرين الوصول اليها و مشاهدتها و التفاعل مع التعليقات و المراسلات النصية داخل التطبيق للتوصل الى إتفاق بين البائع و المشتري', '2019-03-26 19:55:03', '2019-09-22 14:26:41'),
(10, 'about_app_image', 'https://sell4me.ml/public/files/settings/1572364187.logo.png', '2019-03-26 19:55:03', '2019-10-29 17:49:47'),
(16, 'suspendElement', 'السلع والإعلانات الممنوعة : السلع الممنوعة السلعة الموضحة هي السلع الممنوعة في التطبيق . هذه السلع ممنوع الاعلان عنها في الموقع و ممنوع ايضا شرائها عبر الموقع. نحن نقوم بحظر المعلن الذي يقوم بالاعلان عن هذه السلع و نقوم بحظر من يتجاوب معه عبر الرسائل الخاصة أو عبر الردود. نرجو ملاحظة ان ليس هناك تنبيه قبل الحظر وان الحظر نهائي لانقاش فيه.  السلع الممنوعة هي : جميع السلع الممنوعة حسب قوانين المملكة العربية السعودية. هذه السلع ممنوعه حتى لوكانت تعتبر شرعية. الأدوية والمنتجات الطبية والصحية‫.‬ هذه السلع ممنوعه حتى لو كان مسموح بها في قوانين وزارة الصحة وحتى لو كانت سلع موصى بها من الوازرة. التسويق الشبكي‫.‬ يمنع نهائيا اي نوع من التسويق الشبكي مهما كان نوعه او صفته أو طريقته. الأسلحة بمافيها الصواعق والمسدسات و الرشاشات واسلحة الحماية الشخصية و مستلزماتها حتى لو كانت مرخصة. المنتجات الجنسية بكافة أشكالها وأنواعها‫.‬ الأسهم و إدارة المحافظ والعملات وتسويقها وجميع مايتعلق بذلك. أجهزة الليزر وأجهزة التجسس و التنصت. المواقع والمنتديات والخدمات الإلكترونية والإيميلات وبيع العضويات والبرامج. بيع أي سلعه مجانية. مثال على ذلك الإيميلات وحسابات تويتر وانستقرام وغيرها. السلع التي فيها إعتداء على حقوق الملكية الفكرية مثلا البرامج المنسوخة والأفلام المنسوخة. منتجات التبغ الإعلان عن منتجات أو خدمات تتطلب ترخيص من دون الحصول على الترخيص من الجهة المنظمة. الأجهزة الممنوعة مثل: أجهزة التشويش أو التشفير أو تقوية إشارة الجوال. الأجهزة ذات المخاطر الأمنية. الإعلان عن شرائح اتصال او خدمات اتصالات من دون الحصول على التراخيص اللازمة و من دون رفعها لنظام التراخيص بالتطبيق . الإعلانات الممنوعه القائمة التالية تحتوي على أغلب أساليب وطرق الإعلانات الممنوعه في الموقع: جميع الإعلانات التي لاتتعلق بالبيع والشراء طرح مواضيع في الموقع. الإعلان لأجل إضافة إقتراح او مناقشة مشكلة مع الإدارة في التطبيق ‫.‬ الإعلانات مخصصة للبيع والشراء فقط‫.‬ إضافة إعلانات عن إقتراحات لتطوير الموقع في المعروضات يلحق الضرر بإعلانات المعلنين في الموقع‫.‬ أفضل طريقة للإقتراح أو الشكوى هي الإتصال بنا عبر نموذج اتصل بنا‫.‬ الإعلان غير مكتمل التفاصيل. إعلان ضعيف الجودة‫.‬ ضعف تواصل المعلن مع الاعضاء المهتمين بالسلعه المعروضة‫.‬ مثلا معلن يعلن بيع سياره ثم لايقوم بالرد على الاتصالات او الرد على الرسائل الخاصة‫.‬ الإعلان في قسم خطأ‫.‬ مثلا الإعلان عن طلب سياره في المعروضات‫.‬ أو مثلا إعلان عن بيع أثاث في قسم حراج السيارات‫.‬ او مثلا الإعلان عن جيب شيروكي للبيع في قسم فورد‫.‬ إضافة إعلان ولديك عضوية أخرى محظورة. يجب أولا مناقشة الحظر معنا قبل إضافة إي اعلان جديد. إضافة صورة ليست لنفس السلعه إذا كانت السلعة سيارة حتى لو كان لغرض التوضيح. إضافة صور لسلعة أخرى غير المعروضة. مثلا معلن يعلن عن بيع جوال مستعمل ثم يعرض صورة لجوال مستعمل اخر من نفس النوع. إي إعلان يحتوي على إشارة لأي أمر عنصري بكافة أشكاله. إي إعلان يحتوي على معلومات خطأ سواء كان الخطأ مقصود أو غير مقصود‫.‬ مثلا معلن يعلن عن سياره ويذكر انها لم تتعرض لحادث ثم بعد ذلك يثبت أن السياره قد تعرضت لحادث‫.‬ يجب على المعلن عدم إضافة أي معلومة عن السلعه إلا التي هو متأكد منها‫.‬ إضافة إعلان بغرض التشهير. إذا كانت لديك شكوى ضد معلن لدينا نرجو مراسلتنا وتوضيح المشكلة. إذا كانت لديك مشكلة مع جهة لاعلاقه لنا بها نرجو الشكوى لدى الجهة المسؤولة عن ذلك. إضافة عنوان إعلان مخالف لمحتوى الإعلان‫.‬ مثلا معلن يكتب في العنوان‫:‬ كامري 2011 ثم في الإعلان يعلن عن طلب كامري 2011‫.‬ الزائر عندما يرى العنوان سيعتقد ان هناك عرض عن كامري 2011 نسخ إعلان لمعلن آخر او جزء منه. الإعلانات العامة التي لايتم تحديد السلعه بعينها مثل‫:‬ الإعلان بعنوان ‫(‬يوجد لدينا اراضي للبيع‫)‬ او مثلا الإعلان بعنوان يوجد في معرضنا سيارات للبيع‫.‬ الصحيح هو الإعلان عن السلعه ذاتها مثلا يوجد لدينا ارض للبيع مساحة كذا وكذا في حي كذا وكذا بمدينة الرياض مثلا‫.‬ او يوجد لدينا سياره موديل كذا وكذا‫.‬ إعلانات التبرع وطلب المساعدة. نظام الدوله يمنع التبرع والعمل الخيري خارج النطاق القانوني المحدد المخصص لذلك. الإعلانات عن مساهمات وإشتراكات. التوسل ومساعدة المتسولين . نحن نقوم بحظر العضو الذي يقوم مساعدة المتسولين في الموقع. طلب الواسطة والمساعدات سواء كانت مشروعه أو غير مشروعه. التطبيق للسلع فقط الإعلانات التي تحتوي على سوء إستغلال سلطة. الإعلان في الردود. الردود الممنوعة القائمة التالية تحتوي على أغلب الردود الممنوعة: الإعلان في الردود. البخس. السب و الشتم سواء كان هناك مبرر ام يكن هناك مبرر. عدم الجدية و عدم الرغبة في الشراء. التعليق لاجل اضافة نكته او سالفة او خبر. الموقع للبيع والشراء فقط. الاستهزاء بالسلعة او المعلن. الرسائل الخاصة الممنوعة القائمة التالية تحتوي على أغلب الرسائل الخاصة الممنوعة: الإعلان في الرسائل الخاصة. السب و الشتم سواء كان هناك مبرر ام يكن هناك مبرر.', '2019-03-27 13:56:00', '2019-12-25 01:50:09'),
(17, 'taxs', '1', '2019-03-29 12:58:42', '2019-10-29 17:47:30'),
(18, 'terms', 'الشروط و الاحكام&nbsp; إن اتفاقية الاستخدام هذه وخصوصية الاستخدام ، والشروط والبنود ، وجميع السياسات التي تم نشرها على&nbsp;مؤسسة بيع لي للتجارة ( تطبيق بيع لي )&nbsp;وضعت لحماية وحفظ حقوق كل من مؤسسة بيع لي للتجارة ( تطبيق بيع لي ) و (&nbsp;المستخدم&nbsp;الذي يصل إلى الموقع بتسجيل او من دون تسجيل ) أو ( العميل المستفيد من الإعلانات بتسجيل أو من دون تسجيل).&nbsp; تم إنشاء الاتفاقية بناء على نظام التعاملات الإلكترونية. تخضع البنود والشروط والأحكام والمنازعات&nbsp;القانونية للقوانين والتشريعات والأنظمة المعمول بها في المملكة العربية السعودية.&nbsp; لكونك مستخدم فأنك توافق&nbsp;على الالتزام بكل ما يرد بهذه الاتفاقية في حال استخدامك للموقع او في حال الوصول اليه او في حالة التسجيل في الخدمة. يحق لـ مؤسسة بيع لي للتجارة ( تطبيق بيع لي ) التعديل على هذه الاتفاقية في أي وقت وتعتبر ملزمة لجميع الأطراف بعد الإعلان عن التحديث في الموقع أو في أي وسيلة آخرى.&nbsp; &nbsp; شروط الاستخدام بصفتك المعلن او المستخدم للتطبيق في هذه الاتفاقية فإنه بموافقتك على الاستفادة من خدمات التطبيق او الموقع فعليك الالتزام بما يلي : بعدم الإعلان أو تحميل محتوى أو عناصر غير ملائمة للتصنيفات المتاحة في الموقع و المسموح ببيعها. وعليك مراجعة شروط الإعلان و السلع الممنوعة. بعدم الاختراق أو التحايل على قوانين وسياسة وأنظمة الموقع أو أي حقوق تتعلق بطرف ثالث. بعدم نسخ الإعلان من&nbsp;مؤسسة بيع لي للتجارة ( تطبيق بيع لي )&nbsp;وإعادة نشرها في مواقع أخرى. بعدم استخدام أي وسيلة غير شرعية للوصول للإعلانات أو لبيانات المستخدمين الآخرين أو انتهاك لسياسة وحقوق&nbsp;مؤسسة بيع لي للتجارة ( تطبيق بيع لي )&nbsp;،أو الوصول لمحتوى الموقع أو تجميع وتحصيل معلومات وبيانات تخص مؤسسة بيع لي للتجارة ( تطبيق بيع لي ) أو عملاء الموقع والاستفادة منها بأي شكل من الأشكال أو إعادة نشرها. بعدم استخدام خدماتنا إذا كنت غير مؤهل قانونيا لإتمام هذه الاتفاقية. على سبيل المثال أنك أقل من 18 سنة أو أنك محجوب بشكل مؤقت أو دائم من استخدام مؤسسة بيع لي للتجارة ( تطبيق بيع لي ). بعدم التلاعب بأسعار السلع سواء في البيع او الشراء وإلحاق الضرر بالمستخدمين الآخرين. بعدم نشر إعلانات أو تعليقات كاذبة أو غير دقيقة أو مضللة أو خادعة أو قذف ، أو تشهير. بعدم التعرض للسياسات أو السيادات الدولية أو الشخصيات المعتبرة أو أي مناقشات لا تتعلق بالبيع والشراء المشروعة في مؤسسة بيع لي للتجارة ( تطبيق بيع لي ). بعدم نقل حسابك أو نشاطك إلى مواقع اخرى بالوقت الذي هو يحمل شعار أو خدمات مؤسسة بيع لي للتجارة ( تطبيق بيع لي ). بعدم انتهاك حقوق الطبع والنشر والعلامات التجارية، وبراءات الاختراع، والدعاية وقواعد البيانات أو غيرها من حقوق الملكية أو الفكرية التي تنتمي لـ مؤسسة بيع لي للتجارة ( تطبيق بيع لي )&nbsp;أو مرخصة لـ مؤسسة بيع لي للتجارة ( تطبيق بيع لي ). بعدم انتهاك حقوق الآخرين الملكية أو الفكرية أو براءة الاختراع. بعدم انتهاك أنظمة حقوق الإنسان أو أنظمة حماية الحياة الفطرية. بعدم جمع معلومات عن مستخدمي الموقع الآخرين لأغراض تجارية أو غيرها. بعدم الإقدام على أي ما من شأنه إلحاق الضرر بسمعة&nbsp;مؤسسة بيع لي للتجارة ( تطبيق بيع لي ). بعدم انتحال صفة&nbsp;مؤسسة بيع لي للتجارة ( تطبيق بيع لي )&nbsp;أو ممثل لها أو موظف فيها أو أي صفة توحي بأنك تابع لمؤسسة بيع لي للتجارة ( تطبيق بيع لي ) ان لم يكون لديك أذن رسمي من مؤسسة بيع لي للتجارة ( تطبيق بيع لي ) يُمنع إستخدام هذه الخدمة من قِبل أي مستخدم غير بشري ويستثنى من ذلك المستخدمين الغير بشريين التابعين للشركات التالية فقط: Google Facebook Twitter إن عدم التزامك بتلك الشروط يمنح&nbsp;مؤسسة بيع لي للتجارة ( تطبيق بيع لي )&nbsp;الحق كاملا بحجب عضويتك ومنعك من الوصول للموقع دون الحاجة لإخطارك بذلك وأنت هنا تتعهد بعدم العودة لاستخدام الموقع إلى بعد موافقة مؤسسة بيع لي للتجارة ( تطبيق بيع لي )&nbsp;على ذلك.&nbsp; شروط اضافة محتوى للموقع تتعهد بعدم الإعلان عن أي&nbsp;سلعة ممنوعة في التطبيق . تتعهد بعدم اضافة أي&nbsp;ردود ممنوعة&nbsp;في التطبيق. تتعهد بعدم ارسال أي&nbsp;رسائل ممنوعة&nbsp;في التطبيق. تتعهد بتحديد سعر بيع السلعة المعلن عنها في التطبيق. تتعهد بمتابعة إعلانك والرد على استفسارات العملاء من خلال الردود او من خلال الرسائل الخاصة. يلزم أن تكون المادة الإعلانية المعلن عنها سلعة أو خدمة فقط. يجب أن يكون الإعلان كامل التفاصيل وفي القسم الصحيح. يحق لـ مؤسسة بيع لي للتجارة ( تطبيق بيع لي ) حذف أي إعلان من دون ذكر سبب الحذف. يُمنع نسخ أي إعلان من مؤسسة بيع لي للتجارة ( تطبيق بيع لي ). إذا كنت تقدم خدمة أو تبيع سلعة تحتاج تصريح، مثل تقديم خدمات التعقيب، يجب ان يكون لديك التصاريح اللازمة لذلك. يُمنع استخدام أي جزء من أي إعلان في مؤسسة بيع لي للتجارة ( تطبيق بيع لي ). يلتزم المعلن بأن تكون الصور المضافة في الإعلان لنفس السلعة المعلن عنها. صور المحتوى يجب أن تكون لائقة أخلاقيا وذات جودة عالية ولا تخالف نظام وزارة العمل أو حقوق الإنسان أو أي جهة حكومية أو خاصة. يجب الإلتزام بعدم الإعلان عن السلع والخدمات التي تندرج في قائمة السلع و الخدمات الممنوعة. كما يجب أيضا الالتزام بقائمة الردود الممنوعة و الرسائل الممنوعة. تتعهد بعدم الإعلان لشخص لا تعرفه او التسجيل لشخص لا تعرفه. يخضع تحديث وتعديل الإعلان لجميع الشروط والضوابط في هذه الاتفاقية ويعتبر المستخدم مسؤول عن أي مخالفات وقعت أثناء إنشاء الإعلان او بعد التحديث او التعديل. عليك بتحديث الإعلان. إذا لم يحدث المستخدم الإعلان فإنه سيتم حذف الإعلان خلال 60 يوم بشكل تلقائي .&nbsp; &nbsp; السياسة الأمنية لـ مؤسسة بيع لي للتجارة ( تطبيق بيع لي ) تلتزم&nbsp;مؤسسة بيع لي للتجارة ( تطبيق بيع لي )&nbsp;&nbsp;باتفاقية الاستخدام و خصوصية الاستخدام ولكننا لسنا طرفا في أي خلاف أو قضايا تنشأ بين المستخدمين لمخالفة أحدهما أو كلاهما اتفاقية الاستخدام إلا أنه يسعى لتعزيز الجانب الأمني في الموقع وذلك للحد والقضاء على التعديات التي يقوم بها بعض من مستخدمي الموقع بشكل يخالف اتفاقية ، سياسة ، خصوصية و شروط الاستخدام وذلك تحقيقا لنزاهة البيع والشراء ومحاربة النصب والاحتيال والغش والخداع وإتباع للقوانين والتشريعات والتنظيمات المتبعة في المملكة العربية السعودية وبذلك فإنه يحق لـ مؤسسة بيع لي للتجارة ( تطبيق بيع لي )&nbsp;اتخاذ الإجراء اللازم تجاه أي فرد أو مؤسسة أو شركة خالفت اتفاقية الاستخدام علما أنه قد يصل الإجراء إلى الملاحقة القانونية والقضائية أمام الجهة ذات العلاقة. ونورد هنا المخالفات الشائعة والإجراء اللازم تجاهها: تعرض العميل لعملية نصب واحتيال من قبل طرف آخر: يجب على العميل إبلاغ الجهات الأمنية المختصة. يجب على العميل تعبئة نموذج الشكوى الخاص بذلك في صفحة اتصل بنا. ستقوم إدارة الموقع بمراجعة الشكوى والتحري حول العميل المدعى عليه وسيتم اتخاذ الإجراء اللازم في حقه في حال تم التأكد بأنه خالف اتفاقية الاستخدام. . لن يكون الموقع طرف في مثل هذه القضايا ولا يتحمل أي مسئولية ولكننا سنقدم ما لدينا للجهات المختصة في حال تم طلب ذلك بصفة رسمية. التعدي على سياسة أو سيادة الدولة : سيتم حذف الإعلان وإيقاف عضوية المعلن وإبلاغ الجهات المختصة بذلك. في حال الإعلان عن أي من السلع الممنوعة : سيتم توثيق وأرشفة الإعلان وحذفه وإيقاف العضو وإبلاغ الجهات ذات الاختصاص. في حال قام فرد أو مؤسسة أو شركة بطريق غير مشروعة ( كاختراق أو استخدام وسائل تجميع البيانات غير المشروعة أو أي وسيلة كانت ) بهدف الوصول إلى محتوى الموقع أو برمجة الموقع أو قواعد البيانات الخاصة&nbsp;لـ مؤسسة بيع لي للتجارة ( تطبيق بيع لي ) أو المعلومات والبيانات التي تخص عملاء مؤسسة بيع لي للتجارة ( تطبيق بيع لي ) بغية نسخ الإعلانات وإعادة نشرها أو الاستفادة منها بأي شكل من الأشكال. فإن&nbsp;&nbsp;مؤسسة بيع لي للتجارة ( تطبيق بيع لي )&nbsp;سيتوجه للجهات المختصة لمقاضاة الطرف الآخر بدعوى الاختراق الإلكتروني وارتكاب جريمة مخالفة أنظمة الجرائم المعلوماتية. في حال قام فرد أو مؤسسة أو شركة بنسخ إعلانات من&nbsp;مؤسسة بيع لي للتجارة ( تطبيق بيع لي )&nbsp;إلى موقع آخر ستتوجه&nbsp;مؤسسة بيع لي للتجارة ( تطبيق بيع لي )&nbsp;&nbsp;للجهات المختصة لرفع دعوى وملاحقة الطرف الآخر بدعوى التعدي على حقوق المؤسسة. التعدي على حقوق الملكية ، أو الفكرية ، أو براءة الاختراع لطرف ثالث: إن كنت تعتقد بأنه تم التعدي على حق من حقوقك فعليك تعبئة نموذج الشكوى الخاص بالتعدي على الحقوق وتقديم ما يثبت التعدي في الإعلان المنشور. ستقوم&nbsp;مؤسسة بيع لي للتجارة ( تطبيق بيع لي )&nbsp;بمراجعة الشكوى ودراستها والتأكد من صحتها وسيتخذ الإجراء اللازم بحسب النتيجة.. عليك التوجه للجهات ذات الاختصاص في حال رغبتك في مقاضاة الطرف الذي اعتدى على حقوقك. علما أن&nbsp;مؤسسة بيع لي للتجارة ( تطبيق بيع لي )&nbsp;لن يكون طرف في القضية وليس مسئولا عنها ولا نتحمل أي مسئولية. في حال قام الطرف الثاني بالتقييم الكاذب لعضو آخر سيتم إيقاف العضوين ومنعهم من استخدام مؤسسة بيع لي للتجارة ( تطبيق بيع لي ) في حال اتضح التعاون بيهنم. وسيتم اتخاذ الإجراءات اللازمة لأي تبعات حدثت بسبب هذا التقييم. التعرض للتشهير وانتهاك الخصوصية: على العميل تعبئة نموذج الشكوى الخاص بذلك. ستقوم إدارة مؤسسة بيع لي للتجارة ( تطبيق بيع لي )&nbsp;بمراجعة الشكوى والتحري حول العميل المدعى عليه وسيتم اتخاذ الإجراء اللازم في حقه وفي حال تم التأكد بأنه خالف اتفاقية الاستخدام. على العميل الذي لحق به الضرر التوجه للجهات ذات العلاقة لتقديم الشكوى. لن تكون&nbsp;مؤسسة بيع لي للتجارة ( تطبيق بيع لي )&nbsp;طرف في مثل هذه القضايا ولا يتحمل أي مسئولية ولكننا سنقدم البيانات اللازمة للجهات المختصة في حال تم طلب ذلك بصفة رسمية. الإزعاج : عند تعرض عميل ما للإزعاج من قبل عميل آخر فعليه إشعار مؤسسة بيع لي للتجارة ( تطبيق بيع لي ) بذلك وسيتم مراجعة الشكوى واتخاذ الإجراء المناسب حيال ذلك من خيار اتصل بنا . &nbsp; &nbsp;', '2019-04-01 21:04:02', '2019-10-29 19:35:33'),
(19, 'taxs_text', 'عمولة التطبيق هي (  1%  ) من قيمة السلعة المباعة او المؤجرة و هي في ذمة المعلن', '2019-05-08 16:43:28', '2019-12-25 01:40:33'),
(20, 'notes_taxs_text', 'عمولة التطبيق هي (  1%  ) من قيمة السلعة المباعة او المؤجرة و هي في ذمة المعلن', '2019-05-09 00:58:56', '2019-12-25 01:40:33'),
(21, 'treaty_of_Use_1', 'بسم الله الرحمن الرحيم  وَأَوْفُوا بِعَهْدِ اللَّهِ إِذَا عَاهَدتُّمْ وَلَا تَنقُضُوا الْأَيْمَانَ بَعْدَ تَوْكِيدِهَا وَقَدْ جَعَلْتُمُ اللَّهَ عَلَيْكُمْ كَفِيلًا ۚ إِنَّ اللَّهَ يَعْلَمُ مَا تَفْعَلُونَ', '2019-04-01 21:04:02', '2019-04-01 21:04:02'),
(22, 'treaty_of_Use_2', 'أتعهد وأقسم بالله أنا المعلن أن أدفع عمولة التطبيق وهي 1% من قيمة السلعة في حالة بيعها او تاجيرها او الاستفادة منها  عن طريق التطبيق أو بسبب التطبيق وأن هذه العموله هي أمانه في ذمتي. ملاحظة: عمولة التطبيق هي على المعلن ولا تبرأ ذمة المعلن من العمولة إلا في حال دفعها،ذمة المعلن لاتبرأ من العمولة بمجرد ذكر أن العمولة على المشتري في الإعلان', '2019-04-01 21:04:02', '2019-11-18 22:49:56'),
(23, 'treaty_of_Use_3', 'أتعهد أنا المعلن أن جميع المعلومات التي سوف أذكرها بالإعلان صحيحة وفي القسم الصحيح وأتعهد أن الصور التي سوف يتم عرضها هي صور حديثة لنفس السلعة وليست لسلعة أخرى مشابهه.', '2019-04-01 21:04:02', '2019-11-18 22:49:56'),
(24, 'treaty_of_Use_4', 'أتعهد أنا المعلن أن لدي نيه جاده في البيع وأن إعلاني ليس لمجرد معرفة سعر السلعة في السوق.', '2019-04-01 21:04:02', '2019-11-18 22:49:56'),
(25, 'treaty_of_Use_5', 'أتعهد انا المعلن أن أقوم بدفع العمولة خلال أقل من 10 أيام من تاريخ إستلام كامل سعر السلعه.', '2019-04-01 21:04:02', '2019-11-18 22:49:56'),
(26, 'showWaring', 'نحذر بشدة من تحويل الأموال أو إرسال البضائع وننصح بالتعامل المباشر', '2019-07-21 23:26:14', '2019-12-25 01:38:10'),
(27, 'showResponsibility', 'تتم العمليات التجارية بدون أي تدخل أو مسئولية من تطبيق بيع لي مطلقاّ', '2019-07-21 23:26:14', '2019-12-25 01:38:10'),
(28, 'showCommission', 'عمولة التطبيق هي (  1%  ) من قيمة السلعة المباعة او المؤجرة و هي في ذمة المعلن', '2019-07-21 23:26:14', '2019-12-25 01:40:45'),
(29, 'whatsContact', '966570006749', '2020-01-03 07:35:54', '2020-01-03 07:35:54');

-- --------------------------------------------------------

--
-- Table structure for table `supports`
--

CREATE TABLE `supports` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `parent_id` tinyint(4) DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supports`
--

INSERT INTO `supports` (`id`, `user_id`, `parent_id`, `type_id`, `name`, `phone`, `email`, `message`, `is_read`, `created_at`, `updated_at`) VALUES
(1, 130, 0, 1, 'hesham', '+201148863042', NULL, 'اضف شكوي او اقتراح', 1, '2020-01-21 18:18:45', '2020-03-14 21:06:47'),
(2, 129, 0, 1, 'عبدالعزيز الدوسري', '+966557576746', NULL, 'مشكلة', 1, '2020-03-14 21:07:02', '2020-03-14 21:07:26'),
(3, 1, 2, 1, NULL, NULL, NULL, 'تم حل الشكوي', 0, '2020-03-14 21:07:44', '2020-03-14 21:07:44'),
(4, 129, 0, 1, 'عبدالعزيز الدوسري', '+966557576746', NULL, 'شكوي', 1, '2020-03-18 20:01:08', '2020-03-18 20:04:11'),
(5, 1, 4, 1, NULL, NULL, NULL, 'Hello', 0, '2020-03-18 20:04:26', '2020-03-18 20:04:26');

-- --------------------------------------------------------

--
-- Table structure for table `types_supports`
--

CREATE TABLE `types_supports` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_suspend` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `types_supports`
--

INSERT INTO `types_supports` (`id`, `name`, `is_suspend`, `created_at`, `updated_at`) VALUES
(1, 'رد', 0, '2019-01-21 20:42:28', '2019-01-21 19:27:43'),
(2, 'اقتراح', 0, '2019-01-21 20:42:37', '2019-03-27 09:53:45'),
(3, 'طلب', 0, '2019-01-21 20:46:01', '2019-01-21 20:46:01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` int(11) NOT NULL DEFAULT '0',
  `api_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `latitute` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitute` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0',
  `is_accepted` tinyint(4) NOT NULL DEFAULT '0',
  `is_suspend` tinyint(4) NOT NULL DEFAULT '0',
  `message` text COLLATE utf8mb4_unicode_ci,
  `login_count` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `email`, `city_id`, `api_token`, `password`, `address`, `latitute`, `longitute`, `image`, `is_active`, `is_accepted`, `is_suspend`, `message`, `login_count`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'عزيز', '966557576746', 'admin@admin.com', 1, 'eagverhbvacethe', '$2y$10$ecydbYC2nnkNvtUBv/POT.DE0q6mshzAFyTu1YQ1HDCw3uF6fkxEa', NULL, NULL, NULL, NULL, 1, 0, 0, NULL, 0, 'wKbaYhKMa8QUl6FOxmGr8BxvyURa4Ma9Smi5yAqrMkvnRSNi9PytreF39kBb', '2019-09-23 15:10:32', '2020-01-11 21:46:09'),
(129, 'عبدالعزيز الدوسري', '+966557576746', NULL, 69, 'dNlA70W0QoueXaTf8Y3mCpwJfhvD9bEfGFXvmH48jrJ0Iy4BOTnkS7y6dtEz', '$2y$10$o5953rCjAvCchL6GbWq2M./CLu.azckPmZOr5k20qThOVK0DbOVYq', 'الخبر', NULL, NULL, 'http://sell4me.ml/public/assets/admin/images/profile.jpg', 1, 0, 0, NULL, 28, NULL, '2019-11-18 21:58:39', '2020-03-20 04:04:45'),
(130, 'hesham', '+201148863042', NULL, 2, 'jyfBvwWeeG62Xxf9xWtp7V7X6PEwaIzaGr8y4PKh8QK2qs691XWhfE7eaOjT', '$2y$10$jmJFDVDf1fBSjMgmAAhpQuaDCtJKw.EusViodPIeC9ZVTIMoje15y', 'riyadh', NULL, NULL, 'http://sell4me.ml/public/assets/admin/images/profile.jpg', 1, 0, 0, NULL, 32, NULL, '2019-11-18 22:06:20', '2020-03-20 04:03:38'),
(131, 'Ramy', '0100857853', NULL, 18, 'jyfBvwWeeG62Xxf9xWtp7V7X6PEwaIzaGr8y4PKh8QK2qs691XWhfE7eartr', '$2y$10$3ahbJkJXYUGFgCSoZLqc2OeNwL06CvK0tP/mNrkv8XXrVKJQdBIPi', 'riyadh', NULL, NULL, 'http://sell4me.ml/public/assets/admin/images/profile.jpg', 1, 0, 0, NULL, 33, NULL, '2019-11-18 22:06:20', '2019-12-22 01:29:29'),
(132, 'ahmed', '+201008578537', NULL, 33, 'kY7ssSQni72xze2MXVWVIFu6MDuN4lMLIeLojpUmGsC8HxIoGwPTDQYNn5Ux', '$2y$10$p1x9yCeiBbkKu4Un8rR00OWPEvy0kK/ojAS4BLtFdqEJelmTxWl.u', 'egypt', NULL, NULL, 'http://sell4me.ml/public/assets/admin/images/profile.jpg', 1, 0, 0, NULL, 17, NULL, '2019-11-19 23:26:09', '2020-01-22 17:50:39'),
(133, 'maher', '512345678', NULL, 8, 'PLjvu127YA1uaUGqV07bM2a5tiqeeH8Kjw65Mw8PllTUfGwKUV6q0cO3hvrd', '$2y$10$6rQDTPsw4Tb6NyH48/oSbOvuPnY2cdz0GKKmiWZ7QHBqguJ7THAay', 'mansoura', NULL, NULL, 'http://sell4me.ml/public/assets/admin/images/profile.jpg', 1, 0, 0, NULL, 2, NULL, '2019-11-19 23:35:49', '2020-01-08 17:06:43'),
(134, 'maryam', '0542547721', NULL, 2, 'jWwEX5pzbQ28BmL5ksOwmUWzLYVQcc7ZREAOipnlsKrwukCoLBfMajkBOyi6', '$2y$10$vOabESXZpH//K8zmzC8AHOQPnkuDLOWRwGHh4bIOjJZFdA1.wvTpC', 'الخبر', NULL, NULL, 'http://sell4me.ml/public/assets/admin/images/profile.jpg', 1, 0, 0, NULL, 0, NULL, '2019-12-01 23:28:10', '2019-12-01 23:28:21'),
(135, 'عبدالعزيز الدوسري', '0570006749', 'sell4me.est@hotmail.com', 0, 'ExINadraNQOYKGazl05mWexzeuRWYwKzIrsh27JrSvQoMenSn0VqZb4Xaeim', '$2y$10$IU.rnT0s2GT6G9Az717R7OZ49QAYWB94JGEMeJej18kkc4UZpi.Ca', NULL, NULL, NULL, 'https://sell4me.ml/public/files/helpAdmin/1575754635.XLrK8XaXybVylX7IIazVالسعودية.png', 1, 0, 1, NULL, 0, 'ZjelmNOvFVV4GNEF8MC9nCUDzundQWmWLH8bl5fv3ijEFGR31ixm3G2UVBV9', '2019-12-08 00:37:16', '2020-01-11 21:42:48'),
(136, 'mah', '+201554504590', NULL, 61, 'OiOr2PV0Xb6IYaNe8kzAPAvMa70cbvK1Zv4aTFIbebO4Br4yGkjry6gJcxhz', '$2y$10$1F8mbignxeLFtsvepNsAd.2cdLoo0dTHwfg/hxmdKcf8gnHlvPRBe', 'ma', NULL, NULL, 'https://sell4me.ml/public/assets/admin/images/profile.jpg', 1, 0, 0, NULL, 4, NULL, '2019-12-22 17:37:03', '2020-01-08 17:32:49'),
(137, 'Ahmed', '+966512345678', NULL, 35, 'vMZEl6O8aTcq4rxkO6RTVTColq1c5UtyH3NGvvab06OXPu13zDi7UYG8wol2', '$2y$10$6xKU2xUcicKYcTyR/O5yzOrTNYdDbSK84iCv2GM35QBijOhEQ.kza', 'sudia', NULL, NULL, 'https://sell4me.ml/public/assets/admin/images/profile.jpg', 1, 0, 0, NULL, 40, NULL, '2020-01-25 14:26:20', '2020-03-20 08:31:13'),
(138, 'وسام', '+966567571313', NULL, 2, 'frOoegUbNxNZbQXhcVifdkG64yXmyiS433BoWXKWTpnHdepawlgwvJzdGAN8', '$2y$10$tJuY.l/2d1/ex6GKo52YoeTvRgY7NDZPPHZGdU90IDEe6DgCdNSx2', 'الرياض', NULL, NULL, 'https://sell4me.ml/public/assets/admin/images/profile.jpg', 1, 0, 0, NULL, 0, NULL, '2020-02-25 18:04:28', '2020-02-25 18:04:42'),
(139, 'ehab', '+201117499022', NULL, 61, 'of1R9zifhkkizLJOPfrPhUjR2Wr7zwO6319HhiTRJWVV9ese6hArb1MGCYC3', '$2y$10$1MnET3leaQguHv9etIK49eWF0jsIFHcwPmv4fHv3EQrW8in4k9E1m', 'nasr city', NULL, NULL, 'https://sell4me.ml/public/files/users/profiles1583481584.SF65DFQtEnnU0NQL3XQtfileName.jpg', 1, 0, 0, NULL, 0, NULL, '2020-03-06 10:54:15', '2020-03-06 10:59:44'),
(140, 'يوسف', '+966553636555', NULL, 36, 'omsbmLiCZvuq4Ge08zmzqNpRrTdpHq2GTn03Xohl25iRlOvzzMqwmHdvjnN4', '$2y$10$Bl9v6hO9NSAoaWRrW.OsFOvrZzQTAvZy4qcDuZPTjp9vcjJhhqUrm', 'عنيزة حي الريان', NULL, NULL, 'https://sell4me.ml/public/files/users/profiles1584649123.kYZJ2iA6F7TyuyFwG4fyfileName.jpg', 1, 0, 0, NULL, 0, NULL, '2020-03-19 23:15:39', '2020-03-19 23:18:43'),
(141, 'عبدالله', '+966557277821', NULL, 35, '2QjZJHEyg2ns5zTsxtTELPknCHuZQu4QDWWzkhzSQEzAc4aqds4Dms4K2kHs', '$2y$10$LMwnLZk7pf/pVSoDSAF2R.H31FvOB8560lGbo8.FkgiOZ019pp9tS', 'الدمام', NULL, NULL, 'https://sell4me.ml/public/assets/admin/images/profile.jpg', 1, 0, 0, NULL, 0, NULL, '2020-03-20 03:28:33', '2020-03-20 03:28:50');

-- --------------------------------------------------------

--
-- Table structure for table `verify_users`
--

CREATE TABLE `verify_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `views`
--

CREATE TABLE `views` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ad_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `views`
--

INSERT INTO `views` (`id`, `user_id`, `ad_id`, `created_at`, `updated_at`) VALUES
(220, 130, 76, '2019-11-18 22:14:19', '2019-11-18 22:14:19'),
(221, 129, 76, '2019-11-18 22:14:42', '2019-11-18 22:14:42'),
(222, 130, 77, '2019-11-18 22:26:04', '2019-11-18 22:26:04'),
(223, 129, 77, '2019-11-18 22:27:17', '2019-11-18 22:27:17'),
(224, 131, 76, '2019-11-19 17:58:25', '2019-11-19 17:58:25'),
(225, 131, 77, '2019-11-19 18:13:56', '2019-11-19 18:13:56'),
(226, 131, 78, '2019-11-19 19:17:32', '2019-11-19 19:17:32'),
(227, 133, 78, '2019-11-19 23:39:46', '2019-11-19 23:39:46'),
(228, 131, 79, '2019-11-20 15:37:43', '2019-11-20 15:37:43'),
(229, 131, 80, '2019-11-20 16:51:13', '2019-11-20 16:51:13'),
(230, 131, 82, '2019-11-20 18:54:06', '2019-11-20 18:54:06'),
(231, 131, 81, '2019-11-20 19:37:06', '2019-11-20 19:37:06'),
(232, 132, 82, '2019-11-20 20:05:41', '2019-11-20 20:05:41'),
(233, 132, 76, '2019-11-21 16:25:45', '2019-11-21 16:25:45'),
(234, 132, 80, '2019-11-21 17:10:06', '2019-11-21 17:10:06'),
(235, 132, 81, '2019-11-21 17:11:14', '2019-11-21 17:11:14'),
(236, 129, 78, '2019-11-22 00:49:20', '2019-11-22 00:49:20'),
(237, 129, 82, '2019-11-22 00:49:55', '2019-11-22 00:49:55'),
(238, 129, 79, '2019-11-22 00:51:56', '2019-11-22 00:51:56'),
(239, 129, 80, '2019-11-22 00:52:45', '2019-11-22 00:52:45'),
(240, 129, 83, '2019-11-22 01:28:30', '2019-11-22 01:28:30'),
(241, 133, 79, '2019-11-23 15:40:31', '2019-11-23 15:40:31'),
(242, 133, 83, '2019-11-23 17:22:50', '2019-11-23 17:22:50'),
(243, 132, 83, '2019-11-23 17:28:37', '2019-11-23 17:28:37'),
(244, 132, 78, '2019-11-23 17:34:35', '2019-11-23 17:34:35'),
(245, 133, 82, '2019-11-23 17:37:33', '2019-11-23 17:37:33'),
(246, 133, 76, '2019-11-23 17:47:49', '2019-11-23 17:47:49'),
(247, 133, 80, '2019-11-24 17:48:12', '2019-11-24 17:48:12'),
(248, 131, 83, '2019-11-24 21:11:47', '2019-11-24 21:11:47'),
(249, 132, 84, '2019-11-26 17:33:20', '2019-11-26 17:33:20'),
(250, 130, 83, '2019-11-26 20:16:12', '2019-11-26 20:16:12'),
(251, 129, 84, '2019-11-26 20:23:41', '2019-11-26 20:23:41'),
(252, 130, 84, '2019-11-26 20:39:18', '2019-11-26 20:39:18'),
(253, 130, 82, '2019-11-26 22:23:56', '2019-11-26 22:23:56'),
(254, 129, 85, '2019-12-01 20:39:37', '2019-12-01 20:39:37'),
(255, 134, 84, '2019-12-01 23:28:35', '2019-12-01 23:28:35'),
(256, 129, 86, '2019-12-24 03:30:12', '2019-12-24 03:30:12'),
(257, 129, 88, '2019-12-24 14:56:26', '2019-12-24 14:56:26'),
(258, 136, 86, '2019-12-24 18:31:39', '2019-12-24 18:31:39'),
(259, 136, 87, '2019-12-24 18:31:42', '2019-12-24 18:31:42'),
(260, 136, 88, '2019-12-24 18:31:59', '2019-12-24 18:31:59'),
(262, 129, 87, '2020-01-08 22:27:13', '2020-01-08 22:27:13'),
(263, 130, 86, '2020-01-09 17:00:29', '2020-01-09 17:00:29'),
(264, 130, 87, '2020-01-09 17:00:33', '2020-01-09 17:00:33'),
(277, 129, 97, '2020-03-14 22:18:04', '2020-03-14 22:18:04'),
(278, 129, 98, '2020-03-14 22:19:27', '2020-03-14 22:19:27'),
(279, 140, 98, '2020-03-19 23:16:02', '2020-03-19 23:16:02'),
(280, 141, 98, '2020-03-20 03:32:14', '2020-03-20 03:32:14'),
(281, 129, 99, '2020-03-20 03:32:18', '2020-03-20 03:32:18'),
(282, 129, 100, '2020-03-20 04:06:00', '2020-03-20 04:06:00'),
(283, 130, 99, '2020-03-20 04:17:48', '2020-03-20 04:17:48'),
(284, 134, 98, '2020-03-21 00:09:28', '2020-03-21 00:09:28'),
(285, 141, 99, '2020-03-26 21:09:12', '2020-03-26 21:09:12'),
(286, 140, 99, '2020-03-28 22:11:39', '2020-03-28 22:11:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abilities`
--
ALTER TABLE `abilities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `abilities_scope_index` (`scope`);

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ads_user_id_foreign` (`user_id`),
  ADD KEY `ads_category_id_foreign` (`category_id`),
  ADD KEY `ads_city_id_foreign` (`city_id`);

--
-- Indexes for table `ads_taxs`
--
ALTER TABLE `ads_taxs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ads_taxs_user_id_foreign` (`user_id`),
  ADD KEY `ads_taxs_ad_id_foreign` (`ad_id`);

--
-- Indexes for table `assigned_roles`
--
ALTER TABLE `assigned_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assigned_roles_entity_index` (`entity_id`,`entity_type`,`scope`),
  ADD KEY `assigned_roles_role_id_index` (`role_id`),
  ADD KEY `assigned_roles_scope_index` (`scope`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_transfers`
--
ALTER TABLE `bank_transfers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blockes`
--
ALTER TABLE `blockes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_foreign` (`user_id`),
  ADD KEY `comments_ad_id_foreign` (`ad_id`);

--
-- Indexes for table `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversation_user`
--
ALTER TABLE `conversation_user`
  ADD PRIMARY KEY (`user_id`,`conversation_id`);

--
-- Indexes for table `counteries`
--
ALTER TABLE `counteries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `devices_user_id_foreign` (`user_id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorite_user`
--
ALTER TABLE `favorite_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `followers`
--
ALTER TABLE `followers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD KEY `permissions_entity_index` (`entity_id`,`entity_type`,`scope`),
  ADD KEY `permissions_ability_id_index` (`ability_id`),
  ADD KEY `permissions_scope_index` (`scope`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `profiles_truck_number_unique` (`truck_number`),
  ADD KEY `profiles_user_id_foreign` (`user_id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ratings_rateable_type_rateable_id_index` (`rateable_type`,`rateable_id`),
  ADD KEY `ratings_rateable_id_index` (`rateable_id`),
  ADD KEY `ratings_rateable_type_index` (`rateable_type`),
  ADD KEY `ratings_user_id_index` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`,`scope`),
  ADD KEY `roles_scope_index` (`scope`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `settings_key_index` (`key`);

--
-- Indexes for table `supports`
--
ALTER TABLE `supports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `types_supports`
--
ALTER TABLE `types_supports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`);

--
-- Indexes for table `verify_users`
--
ALTER TABLE `verify_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `verify_users_user_id_foreign` (`user_id`);

--
-- Indexes for table `views`
--
ALTER TABLE `views`
  ADD PRIMARY KEY (`id`),
  ADD KEY `views_user_id_foreign` (`user_id`),
  ADD KEY `views_ad_id_foreign` (`ad_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abilities`
--
ALTER TABLE `abilities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `ads_taxs`
--
ALTER TABLE `ads_taxs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `assigned_roles`
--
ALTER TABLE `assigned_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bank_transfers`
--
ALTER TABLE `bank_transfers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blockes`
--
ALTER TABLE `blockes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=222;

--
-- AUTO_INCREMENT for table `conversations`
--
ALTER TABLE `conversations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `counteries`
--
ALTER TABLE `counteries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=233;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `favorite_user`
--
ALTER TABLE `favorite_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `followers`
--
ALTER TABLE `followers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=285;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `supports`
--
ALTER TABLE `supports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `types_supports`
--
ALTER TABLE `types_supports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT for table `verify_users`
--
ALTER TABLE `verify_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `views`
--
ALTER TABLE `views`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=287;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ads`
--
ALTER TABLE `ads`
  ADD CONSTRAINT `ads_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ads_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `counteries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ads_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ads_taxs`
--
ALTER TABLE `ads_taxs`
  ADD CONSTRAINT `ads_taxs_ad_id_foreign` FOREIGN KEY (`ad_id`) REFERENCES `ads` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ads_taxs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ad_id_foreign` FOREIGN KEY (`ad_id`) REFERENCES `ads` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `devices`
--
ALTER TABLE `devices`
  ADD CONSTRAINT `devices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions_ability_id_foreign` FOREIGN KEY (`ability_id`) REFERENCES `abilities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ratings`
--
ALTER TABLE `ratings`
  ADD CONSTRAINT `ratings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `verify_users`
--
ALTER TABLE `verify_users`
  ADD CONSTRAINT `verify_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `views`
--
ALTER TABLE `views`
  ADD CONSTRAINT `views_ad_id_foreign` FOREIGN KEY (`ad_id`) REFERENCES `ads` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `views_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
