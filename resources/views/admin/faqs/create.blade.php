@extends('admin.layouts.master')

@section('title',  __('maincp.common_questions') )



@section('styles')


    <style>
        .dropify-wrapper {
            height: 180px !important;
        }
    </style>
@endsection
@section('content')

    <div id="messageError"></div>
    <form data-parsley-validate novalidate method="POST" action="{{ route('faqs.store') }}"
          enctype="multipart/form-data">
    {{ csrf_field() }}
    <!-- Page-Title -->
        <div class="row">
            <div class="col-lg-12 ">
                <div class="btn-group pull-right m-t-15">


                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> @lang('maincp.back') <span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </button>


                </div>
                <h4 class="page-title">@lang('maincp.common_questions')</h4>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-12 ">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">@lang('maincp.add_new_item')</h4>

                    <div class="form-group">
                        <label for="userName">@lang('maincp.question') *</label>
                        <input type="text" name="question" data-parsley-trigger="keyup" required
                               placeholder="@lang('maincp.question')..." class="form-control description"
                               id="userName" 
                               data-parsley-required-message="هذا الحقل إلزامي">
                    </div>


                    <div class="form-group">
                        <label for="userName">@lang('maincp.answer')*</label>


                        <textarea class="form-control msg_body" name="answer" required  data-parsley-required-message="هذا الحقل إلزامي" 
                        	data-parsley-trigger="keyup" placeholder="@lang('maincp.answer')..."></textarea>
                    </div>



                    <div class="form-group text-right m-b-0 ">
                        <button class="btn btn-warning waves-effect waves-light m-t-20" type="submit">@lang('maincp.save_data')
                        </button>
                        <button onclick="window.history.back();return false;"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">@lang('maincp.disable')  
                        </button>
                    </div>


                </div>
            </div><!-- end col -->

            {{--<div class="col-lg-4">
                <div class="card-box" style="overflow: hidden;">

                    <h4 class="header-title m-t-0 m-b-30">شعار العلامة التجارية</h4>

                    <div class="form-group">
                        <input type="file" name="image" class="dropify" data-max-file-size="6M"/>
                    </div>

                </div>
            </div>--}}


        </div>
        <!-- end row -->
    </form>


@endsection


@section('scripts')

    <script type="text/javascript"
            src="{{ request()->root() }}/public/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>



    {{--<script type="text/javascript">--}}

    {{--$('form').on('submit', function (e) {--}}
    {{--e.preventDefault();--}}
    {{--var formData = new FormData(this);--}}
    {{--$.ajax({--}}
    {{--type: 'POST',--}}
    {{--url: $(this).attr('action'),--}}
    {{--data: formData,--}}
    {{--cache: false,--}}
    {{--contentType: false,--}}
    {{--processData: false,--}}
    {{--success: function (data) {--}}

    {{--//  $('#messageError').html(data.message);--}}

    {{--var shortCutFunction = 'success';--}}
    {{--var msg = data.message;--}}
    {{--var title = 'نجاح';--}}
    {{--toastr.options = {--}}
    {{--positionClass: 'toast-top-left',--}}
    {{--onclick: null--}}
    {{--};--}}
    {{--var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists--}}
    {{--$toastlast = $toast;--}}
    {{--setTimeout(function () {--}}
    {{--window.location.href = '{{ route('categories.index') }}';--}}
    {{--}, 3000);--}}
    {{--},--}}
    {{--error: function (data) {--}}

    {{--}--}}
    {{--});--}}
    {{--});--}}

    {{--</script>--}}
@endsection




