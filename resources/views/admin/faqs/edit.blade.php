@extends('admin.layouts.master')
@section('title', __('maincp.common_questions'))
@section('content')

    <div id="messageError"></div>
    <form data-parsley-validate novalidate method="POST" action="{{ route('faqs.update', $faq->id) }}"
          enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
        
    <!-- Page-Title -->
        <div class="row">
              <div class="col-lg-12 ">
                <div class="btn-group pull-right m-t-15">
                    <a href="{{ route('faqs.index') }}" class="btn btn-custom waves-effect waves-light"> رجوع <span
                                class="m-l-5">
                            <i class="fa fa-reply"></i></span>
                    </a>
                </div>
                <h4 class="page-title">  @lang('maincp.common_questions')</h4>
            </div>
        </div>

        <div class="row">
               <div class="col-lg-12 ">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30"> @lang('maincp.edit_question')( {{ $faq->question }} )</h4>

                    <div class="form-group">
                        <label for="userName">@lang('maincp.name')*</label>
                        <input type="text" name="question" data-parsley-trigger="keyup" required
                               placeholder="نوع البطارية..." class="form-control description"
                               id="userName" value="{{ $faq->question  }}"

                               data-parsley-required-message="هذا الحقل إلزامي">
                    </div>


                    <div class="form-group">
                        <label for="userName">@lang('maincp.answer')*</label>


                        <textarea class="form-control msg_body" name="answer" required  data-parsley-required-message="هذا الحقل إلزامي" data-parsley-trigger="keyup" placeholder="الإجابة...">{{ $faq->answer }}</textarea>
                    </div>




                    <div class="form-group text-right m-b-0 ">
                        <button class="btn btn-warning waves-effect waves-light m-t-20" type="submit"> @lang('maincp.save_data')  
                        </button>
                        <button onclick="window.history.back();return false;"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20"> @lang('maincp.disable') 
                        </button>
                    </div>


                </div>
            </div><!-- end col -->




        </div>


        <!-- end row -->
    </form>


@endsection

@section('scripts')
    <script type="text/javascript"
            src="{{ request()->root() }}/public/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>

@endsection


{{--<script type="text/javascript">--}}

{{--$('form').on('submit', function (e) {--}}
{{--e.preventDefault();--}}
{{--var formData = new FormData(this);--}}
{{--$.ajax({--}}
{{--type: 'POST',--}}
{{--url: $(this).attr('action'),--}}
{{--data: formData,--}}
{{--cache: false,--}}
{{--contentType: false,--}}
{{--processData: false,--}}
{{--success: function (data) {--}}

{{--//  $('#messageError').html(data.message);--}}

{{--var shortCutFunction = 'success';--}}
{{--var msg = data.message;--}}
{{--var title = 'نجاح';--}}
{{--toastr.options = {--}}
{{--positionClass: 'toast-top-left',--}}
{{--onclick: null--}}
{{--};--}}
{{--var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists--}}
{{--$toastlast = $toast;--}}
{{--setTimeout(function () {--}}
{{--window.location.href = '{{ route('categories.index') }}';--}}
{{--}, 3000);--}}
{{--},--}}
{{--error: function (data) {--}}

{{--}--}}
{{--});--}}
{{--});--}}

{{--</script>--}}
{{--@endsection--}}




