@extends('admin.layouts.master')
@section('title', 'تفاصيل الاعلانات')
@section('content')





    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-15">
                    {{--<button type="button" class="btn btn-custom  waves-effect waves-light"--}}
                        {{--onclick="window.history.back();return false;"> @lang('maincp.back') <span class="m-l-5"><i--}}
                                {{--class="fa fa-reply"></i></span>--}}
                    {{--</button>--}}

                    <a href="{{ route('ads.index') }}"
                       class="btn btn-custom  waves-effect waves-light">
												<span><span>رجوع  </span>
													<i class="fa fa-reply"></i>
												</span>
                    </a>

                </div>
                <h4 class="page-title">بيانات الإعلان </h4>
            </div>
        </div>


        <div class="row">


                <div class="col-sm-12">

                <div class="card-box">
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="card-box p-b-0">


                                <h4 class="header-title m-t-0 m-b-30">التفاصيل</h4>

                                <form>
                                    <div id="basicwizard" class=" pull-in">
                                        <ul class="nav nav-tabs navtab-wizard nav-justified bg-muted">
                                            <li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">البيانات الاساسية</a></li>
                                            <li class=""><a href="#tab2" data-toggle="tab" aria-expanded="true">صور الاعلان </a></li>
                                        </ul>
                                        <div class="tab-content b-0 m-b-0">
                                            <div class="tab-pane m-t-10 fade active in" id="tab1">
                                                <div class="row">
                                                    <div class="col-sm-8">
                                                        <div class="col-xs-12 col-lg-12">

                                                            <hr>
                                                        </div>

                                                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                            <label> إسم الاعلان :</label>
                                                            <input class="form-control" value="{{ $ad->name }}"><br>
                                                        </div>

                                                        @if($ad->description )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12" style="margin: 10px auto">
                                                                <label> تفاصيل الإعلان  :</label>
                                                                <textarea class="form-control">{{ $ad->description }}</textarea>
                                                            </div>
                                                        @endif

                                                        @if($ad->phone )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>@lang('maincp.mobile_number') :</label>
                                                                <input class="form-control" value="{{ $ad->phone }}"><br>
                                                            </div>
                                                        @endif

                                                        @if($ad->email )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>@lang('maincp.e_mail')  :</label>
                                                                <input class="form-control" value="{{ $ad->email }}"><br>
                                                            </div>
                                                        @endif

                                                        @if($ad->category )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>القسم  :</label>
                                                                <input class="form-control" value="{{ optional($ad->category)->name  }}"><br>
                                                            </div>
                                                        @endif

                                                        @if($ad->category->parent )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>القسم الرئيسي  :</label>
                                                                <input class="form-control" value="{{ optional($ad->category->parent)->name  }}"><br>
                                                            </div>
                                                        @endif

                                                        @if($ad->city )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>المدينة  :</label>
                                                                <input class="form-control" value="{{ optional($ad->city)->name  }}"><br>
                                                            </div>
                                                        @endif

                                                        @if($ad->city->parent )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>الدولة  :</label>
                                                                <input class="form-control" value="{{ optional($ad->city->parent)->name  }}"><br>
                                                            </div>
                                                        @endif

                                                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                            <label>@lang('maincp.address') :</label>
                                                            <input class="form-control" value="{{ $ad->address }}"><br>
                                                        </div>


                                                    </div>

                                                </div>

                                            </div>
                                            <div class="tab-pane m-t-10 fade " id="tab2">

                                                @if( $ad->images)
                                                    <div class="form-group">

                                                    @if(count($ad->images) > 0)

                                                        @foreach(\App\Models\Image::whereIn('id',$ad['images'])->get() as $item)


                                                            <div class="col-lg-4">

                                                                <a data-fancybox="gallery"

                                                                   href="{{ $helper->getDefaultImage($item->url, request()->root().'/public/assets/admin/custom/images/default.png') }}">
                                                                    <img style="width: 100% ; height: 100%" src="{{ $helper->getDefaultImage($item->url, request()->root().'/public/assets/admin/custom/images/default.png') }}"/>
                                                                </a>
                                                            </div>
                                                        @endforeach
                                                    @endif
                                                    </div>
                                                @else
                                                        لا يوجد اي صور لهذا الإعلان
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>



                    </div>
                </div>
            </div>

        </div>




@endsection

