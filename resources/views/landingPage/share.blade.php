<!DOCTYPE html>
<html lang="ar">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Meta-->
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title-->
    <title>الاعلانات</title>

    <!--Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Tajawal" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ request()->root()  }}/public/landingPage/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ request()->root()  }}/public/landingPage/css/bootstrap-rtl.min.css">
    <link rel="stylesheet" href="{{ request()->root()  }}/public/landingPage/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ request()->root()  }}/public/landingPage/css/owl.theme.default.min.css">
    <link rel="stylesheet " href="{{ request()->root()  }}/public/landingPage/css/animate.css ">
    <link rel="stylesheet " href="{{ request()->root()  }}/public/landingPage/css/style2.css ">
<link rel="shortcut icon" type="image/png" href="images/ico.ico">
</head>

<body>
    <div class="container pb-5">
        <div class="text-center my-5">
            <img src="{{ request()->root()  }}/public/landingPage/images/logo2.png" class="img-fluid logo wow fadeInDown">
        </div>
        <div class="row product-card p-3">
            <div class="col-md-6 p-0 ltr ">
                <!-- slider -->

                <div class="owl-carousel owl-theme wow zoomIn" id="product-slider">
                    @foreach($advertisement['images'] as $image)
                    <div class="item">
                        <img src="{{ $image['url']}}" class="img-fluid m-auto ">
                    </div> 
                    @endforeach


                </div>

            </div>
            <div class="col-md-6 details ">
                <div class="m-auto wow zoomIn">
                    <h3 class="py-4 m-0">
                       {{ isset($advertisement) ? $advertisement->name : ''}}
                    </h3>
                    <h1 class="w-700 price">
                         {{ isset($advertisement) ? $advertisement->price : ''}}
                    </h1>
                    <p>
                        {{ isset($advertisement) ? $advertisement->description : ''}}  
                    </p> 

                </div>
                <!-- store block in web -->
                <div class="store py-3 wow fadeInUp">
                    <a href="https://apps.apple.com/eg/app/%D8%A8%D9%8A%D8%B9-%D9%84%D9%8A/id1467609770">
                        <img src="{{ request()->root()  }}/public/landingPage/images/appstore.png">
                    </a>
                    <a href="https://play.google.com/store/apps/details?id=com.saned.sellforme">
                        <img src="{{ request()->root()  }}/public/landingPage/images/googleplay.png">
                    </a>
                </div>
            </div>

        </div>
<p class="text-center mt-3">        لمشاهدة رقم المعلن والتواصل معه حمل تطبيق بيع لي و ابحث بإسم الاعلان ( {{ isset($advertisement) ? $advertisement->name : ''}} )
</p>
        <!-- fixed store in mobile -->
        <div class="mob-store">
            <a href="https://apps.apple.com/eg/app/%D8%A8%D9%8A%D8%B9-%D9%84%D9%8A/id1467609770">
                <img src="{{ request()->root()  }}/public/landingPage/images/appstore.png">
            </a>
            <a href="https://play.google.com/store/apps/details?id=com.saned.sellforme">
                <img src="{{ request()->root()  }}/public/landingPage/images/googleplay.png">
            </a>
        </div>
    </div>


    <!--scripts -->
    <script type="text/javascript " src="{{ request()->root()  }}/public/landingPage/js/jquery-3.2.1.min.js "></script>
    <script type="text/javascript " src="{{ request()->root()  }}/public/landingPage/js/bootstrap.min.js "></script>
    <script type="text/javascript " src="{{ request()->root()  }}/public/landingPage/js/owl.carousel.min.js "></script>
    <script type="text/javascript" src="{{ request()->root()  }}/public/landingPage/js/wow.min.js"></script>
    <script type="text/javascript" src="{{ request()->root()  }}/public/landingPage/js/scripts2.js"></script>

</body>

</html>