<!DOCTYPE html>
<html lang="ar">

<head>
    <!-- Meta-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title-->
    <title>الاعلانات</title>

    <!--Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Tajawal" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ request()->root()  }}/public/landingPage/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ request()->root()  }}/public/landingPage/css/bootstrap-rtl.min.css">
    <link rel="stylesheet" type="text/css" href="{{ request()->root()  }}/public/landingPage/css/slick.css" />
    <link rel="stylesheet" type="text/css" href="{{ request()->root()  }}/public/landingPage/css/slick-theme.css" />
    <link rel="stylesheet" href="{{ request()->root()  }}/public/landingPage/css/style.css">

</head>

<body>

<!-- header -->
<header>
    <div class="container-fluid text-white">
        <div class="overlay">
            <img src="{{ request()->root()  }}/public/landingPage/images/logo.png" class="img-fluid mb-5">
            <h3 class="mt-5 mb-3">
                تطبيق بيع لي يبيع لك أي شئ فى بالك !
            </h3>
            <ul class="p-0">
                <li>
                    اسرع
                </li>
                <li>
                    اسهل
                </li>
                <li>
                    افضل
                </li>
            </ul>
        </div>
    </div>

</header>
<!-- header slider -->
<div class="container text-center">
    <ul class="header-slider p-0 ">
        <li>
            <img src="{{ request()->root()  }}/public/landingPage/images/mockup1.png" class="img-fluid m-auto">
        </li>
        <li>
            <img src="{{ request()->root()  }}/public/landingPage/images/mockup2.png" class="img-fluid m-auto">
        </li>
        <li>
            <img src="{{ request()->root()  }}/public/landingPage/images/mockup3.png" class="img-fluid m-auto">
        </li>

    </ul>
</div>
<!-- about -->
<div class="container ">
    <div class="row ">
        <div class="col-lg-6 des">
            <h2 class="pb-5">نبذة عن تطبيق بيع لي</h2>
            <p> {{$aboutUs}}</p>
        </div>
        <div class="col-lg-6">
            <div>
                <img src="{{ request()->root()  }}/public/landingPage/images/pic.png" class="img-fluid img1">
                <img src="{{ request()->root()  }}/public/landingPage/images/adv.png" class="img-fluid img2">
            </div>
            <div>
                <img src="{{ request()->root()  }}/public/landingPage/images/advcomment.png" class="img-fluid img3">
                <img src="{{ request()->root()  }}/public/landingPage/images/pic1.png" class="img-fluid img4">
            </div>
        </div>
    </div>
</div>
<!-- download -->
<div class="container py-5">
    <div class="row">
        <div class="col-xl-6 col-lg-6">
            <img src="{{ request()->root()  }}/public/landingPage/images/mockup.png" class="img-fluid m-auto">
        </div>
        <div class="col-xl-6 col-lg-6 des">
            <h2 class="pb-5">متاح الأن للتحميل على جوالك</h2>
            <p>
                يمكنك الان تحميل تطبيق بيع لي على جوالك من خلال منصتي جوجل بلاي و ابل ستور
            </p>
            <div class="store">
                <a href="https://play.google.com/store/apps/details?id=com.saned.sellforme">
                    <img src="{{ request()->root()  }}/public/landingPage/images/googleplay.png" class="img-fluid">
                </a>
                <a href="https://apps.apple.com/us/app/sell-for-me/id1467609770?ls=1">
                    <img src="{{ request()->root()  }}/public/landingPage/images/appstore.png" class="img-fluid">
                </a>
                
            </div>
        </div>
    </div>
</div>
<!-- footer -->
<footer class="container-fluid text-white text-center py-4">
    <div>
        <!--{{$phone}}-->
        <p class="d-inline-block">التواصل معنا عن طريق الواتساب</p>
       <a href="https://api.WhatsApp.com/send?phone={{$phone}}"><img class="d-inline-block social" src="{{ request()->root()  }}/public/landingPage/images/whatsapp.png"></a>
       <span class="copy-right d-block">جميع الحقوق محفوظة لمؤسسة بيع لي</span>
    </div>
</footer>
<!--scripts -->
<script src="{{ request()->root()  }}/public/landingPage/js/jquery-3.2.1.min.js"></script>
<script src="{{ request()->root()  }}/public/landingPage/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ request()->root()  }}/public/landingPage/js/slick.min.js"></script>
<script src="{{ request()->root()  }}/public/landingPage/js/scripts.js"></script>
</body>

</html>