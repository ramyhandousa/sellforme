$(document).ready(function() {
    // ================= wow animate ====================
    new WOW().init();
    // =================== slider =======================
    $('#product-slider').owlCarousel({
        center: true,
        items: 1,
        loop: true,
        margin: 10,
        dots: true,
        responsive: {
            600: {
                items: 1
            }
        }
    })


});