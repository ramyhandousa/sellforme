importScripts('https://www.gstatic.com/firebasejs/4.10.1/firebase-app.js');   
// importScripts('https://www.gstatic.com/firebasejs/5.8.5/firebase-app.js'); 

importScripts('https://www.gstatic.com/firebasejs/4.10.1/firebase-messaging.js'); 
// importScripts('https://www.gstatic.com/firebasejs/5.8.5/firebase-messaging.js');

 var config = {
    apiKey: "AIzaSyA5ajU7xpbZbGcs40QUvDQva0iuDtfstmc",
    authDomain: "sellforme-995b3.firebaseapp.com",
    databaseURL: "https://sellforme-995b3.firebaseio.com",
    projectId: "sellforme-995b3",
    storageBucket: "sellforme-995b3.appspot.com",
    messagingSenderId: "461523329340",
    // appId: "1:461523329340:web:9ad51a2383267673"
  };
  firebase.initializeApp(config);
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    body: 'Background Message body.',
    icon: 'https://i.pinimg.com/736x/61/f1/cb/61f1cb5e0db9cb346501c68fdf75a10d.jpg',
    
  };


self.addEventListener('notificationclick', function (event) {
  event.notification.close();

  var clickResponsePromise = Promise.resolve();
    clickResponsePromise = clients.openWindow('facebook');

  event.waitUntil(Promise.all([clickResponsePromise, self.analytics.trackEvent('notification-click')]));
});



  return self.registration.showNotification(notificationTitle,
      notificationOptions);
});
