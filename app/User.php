<?php

namespace App;

use App\Models\Ad;
use App\Models\Countery;
use App\Models\Conversation;
use App\Models\Notification;
use App\Models\Device;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use App\Notifications\MyAdminResetPassword as ResetPasswordNotification;
use willvincent\Rateable\Rateable;
use willvincent\Rateable\Rating;

class User extends Authenticatable
{
    use Notifiable ,HasRolesAndAbilities;
    use  Rateable;
    
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
        
    }
    
    protected $fillable = [
        'name',
        'phone',
        'email',
        'city_id',
        'password',
        'image',
        'api_token',
        'latitute',
        'longitute',
        'address',
        'is_active',
        'is_accepted',
        'is_suspend',
        'message',
        'login_count',
    ];

    
    protected $hidden = [
        'password','remember_token'
    ];
    
    
    public function hasAnyRoles()
    {
        if (auth()->check()) {
            
            
            if (auth()->user()->roles->count()) {
                return true;
            }
            
        } else {
            redirect(route('admin.login'));
        }
    }
    
    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }


    public function city()
    {
        return $this->belongsTo(Countery::class);
    }


   
    public function ratings()
    {
        return $this->morphMany('willvincent\Rateable\Rating', 'rateable');
    }



    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function ads()
    {
        return $this->hasMany(Ad::class);
    }

    public function my_favorites()
    {

        return $this->belongsToMany(Ad::class, 'favorite_user', 'user_id', 'ads_id')->withTimestamps();

    }

       public function devices()
    {
        return $this->hasMany(Device::class);
    }
    
    public function conversations()
    {
        return $this->belongsToMany(Conversation::class);
    }
    
    public function followers()
    {
        return $this->belongsToMany(User::class, 'followers', 'user_id', 'follow_id');
    }
    
    
     public function blockers()
    {
        return $this->belongsToMany(User::class, 'blockes', 'user_id', 'blocker_id');
    }

    public function blocking()
    {
        return $this->belongsToMany(User::class, 'blockes', 'blocker_id', 'user_id');
    }

   
    
}
