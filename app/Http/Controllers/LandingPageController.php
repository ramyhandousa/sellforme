<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\Ad;
use App\Models\Image;
use Illuminate\Http\Request;

class LandingPageController extends Controller
{
    public function index(){
    
        $aboutUs = Setting::whereKey('about_app_desc_ar')->first()->body;
        $phone = Setting::whereKey('contactus_phone')->first()->body;

        return view('landingPage.index',compact('aboutUs','phone'));
    }
    
    public function share(Request $request){
        
        $advertisement = Ad::findOrFail($request->id); 
        $advertisement['images'] =  $advertisement['images'] ? $this->getImagesWithIds($advertisement['images']) : [];
       
        // return $advertisement['images'];
        //
        
        return view('landingPage.share', compact('advertisement'));

    }
    
    
    private function getImagesWithIds($images){
        $image = Image::whereIn('id',$images)->select('id','url')->get();

        return $image;
    }


}
