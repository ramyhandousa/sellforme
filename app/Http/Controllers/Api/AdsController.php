<?php

namespace App\Http\Controllers\Api;

use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Http\Resources\Ads;
use App\Models\Ad;
use App\Models\Message;
use App\Models\Conversation;
use App\Models\AdsTaxs;
use App\Models\View;
use App\Models\Comment;
use App\User;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdsController extends Controller
{

    public $public_path;
    public $defaultImage;
    public $headerApiToken;
    public $notify;
    public $push;
    public function __construct(InsertNotification $notification, PushNotification $push )
    {
        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
        app()->setLocale($language);

        $this->public_path = 'files/users/profiles';

        // default Image upload in profile user
        $this->defaultImage = \request()->root() . '/' . 'public/assets/images/default-profile-img.png';

        // api token from header
        $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';

        $this->notify = $notification;

        $this->push = $push;
    }
    
      public function listAdsLatest(){

        $list = Ad::where('is_suspend',0)->whereHas('user',function ($q){
                        $q->where('is_active',1)->where('is_suspend',0);
                    })->latest()->get();

        $data = Ads::collection($list);

        return response()->json( [
            'status' => 200 ,
            'data' => $data,
        ] , 200 );

    }
    

    public function store(Request $request){
        

        $user = User::where( 'api_token' , $this->headerApiToken )->first();

        $advert = new  Ad();
        $advert->user_id =      $user->id;
        $advert->category_id =  $request->categoryId;
        $advert->city_id =      $request->cityId;
        $advert->name =         $request->name;
        $advert->description =  $request->description;
        $advert->phone =        $request->phone;
        $advert->address =      $request->address;
        $advert->latitute =     $request->latitute;
        $advert->longitute =    $request->longitute;
        $advert->can_comment =    $request->can_comment;

        // ids from upload Images
        if ($request->images ){
            $myids =  implode(',',$request->images) ;
            $filterIds = explode(',',$myids);
            $advert->images = $filterIds;

        }else{
            $advert->images = ["141"];
        }

        $advert->save();

        $data = new Ads($advert);

        return response()->json( [
            'status' => 200 ,
            'data' => $data,
        ] , 200 );

    }
    
    
    
    public function editAdvertisement(Request $request){
        $user = User::where( 'api_token' , $this->headerApiToken )->first();

        $advert = Ad::whereId($request->advertisementId)->first();

        if (!$advert){ return $this->advertisementNotFound(); }

        $advert->user_id =      $user->id;
        $advert->category_id =  $request->categoryId;
        $advert->city_id =      $request->cityId;
        $advert->name =         $request->name;
        $advert->description =  $request->description;
        $advert->phone =        $request->phone;
        $advert->address =      $request->address;
        $advert->latitute =     $request->latitute;
        $advert->longitute =    $request->longitute;
        $advert->can_comment =    $request->can_comment;
        
        // ids from upload Images
        if ($request->images ){
            $myids =  implode(',',$request->images) ;
            $filterIds = explode(',',$myids);
            $advert->images = $filterIds;

        }else{
            $advert->images = ["141"];
        }

        $advert->save();

        $data = new Ads($advert);

        return response()->json( [
            'status' => 200 ,
            'data' => $data,
        ] , 200 );
    }


    public function delete(Request $request){
        $advert = Ad::whereId($request->advertisementId)->first();

        if (!$advert){ return $this->advertisementNotFound(); }

        $advert->delete(); 
        
        

        return response()->json( [
            'status' => 200 ,
            'message' => 'تم مسح الإعلان بنجاح',
        ] , 200 );
    }
    
    public function getAdvertisementById(Request $request){
        $advertisement = Ad::where('is_suspend',0)->whereId($request->id)->with('user','comments')->first();
        
         

        if (!$advertisement){ return $this->advertisementNotFound(); }
        
         if ($this->headerApiToken != ' ' && $this->headerApiToken){

            $user = User::whereApiToken($this->headerApiToken)->first();
            
            $advertisement->update(['is_read' => 'true']);

          $exist = View::where(['user_id' => $user->id,'ad_id' =>$request->id])->first();

          if (!$exist){
              $view = new View();
              $view->user_id = $user->id;
              $view->ad_id = $request->id;
              $view->save();
          }

        }
        

        $mainAdvertisement = new Ads($advertisement);


        $ads = Ad::whereIsSuspend(0);

        if ($request->cityId || $request->categoryId){

            if ($request->cityId){

                $ads->where('city_id',$request->cityId);
            }

            if ($request->categoryId){

                $ads->whereCategoryId($request->categoryId);

            }
            $similarData = Ads::collection($ads->where('id','!=',$mainAdvertisement->id)->with('comments')->latest()->get());

            $data = ['data' => $mainAdvertisement , 'similarAdvertisement' => $similarData];

            return response()->json( [
                'status' => 200 ,
                'data' => $data ,
            ] , 200 );


        }else{

            return response()->json( [
                'status' => 200 ,
                'data' => $mainAdvertisement,
            ] , 200 );

        }

    }

    public function myAdvertisements(Request $request){
        
        $user = User::whereId($request->userId)->first();

        if ( ! $user ) {   return $this->UserNotFound();  }

            $ads = Ads::collection(Ad::where('is_suspend',0)->whereUserId($user->id)->latest()->get());

        return response()->json( [
            'status' => 200 ,
            'data' => $ads,
        ] , 200 );
        
    }

    public function similarAdvertisement(Request $request){
        $advertisement = Ad::where('is_suspend',0)->whereId($request->id)->first();

        if (!$advertisement){ return $this->advertisementNotFound(); }

        $mainAdvertisement = new Ads($advertisement);
        
        $ads = Ad::whereIsSuspend(0)->where('city_id',$advertisement->city_id)
                                    ->where('category_id',$advertisement->category_id);


        $similarData = Ads::collection($ads->where('id','!=',$mainAdvertisement->id)->with('comments')->latest()->get());


        return response()->json( [
            'status' => 200 ,
            'data' => $similarData ,
        ] , 200 );


    }


    public function dataAds(){

        if ($this->headerApiToken != ' ' && $this->headerApiToken){

            $user = User::whereApiToken($this->headerApiToken)->first();

            if ( ! $user ) {   return $this->UserNotFound();  }
 
             $idsBlocked =  $user->blocking->pluck('id');

            $ads = Ads::collection(Ad::where('is_suspend',0)->whereNotIn('user_id', $idsBlocked)->whereCityId($user->city_id)->latest()->get());

        }else{

            // $ads = Ads::collection(Ad::inRandomOrder()->latest()->get());
             $ads = Ads::collection(Ad::where('is_suspend',0)->latest()->get());

        }


        return response()->json( [
            'status' => 200 ,
            'data' => $ads,
        ] , 200 );
    }
    
      public function adsTaxs(Request $request){
        $user = User::whereApiToken($this->headerApiToken)->with('devices')->first();

        if ( ! $user ) {   return $this->UserNotFound();  }

        $advertisement = Ad::where('is_suspend',0)->whereId($request->advertisementId)->with('user.devices')->first();

        if (!$advertisement){ return $this->advertisementNotFound(); }
        
         $havePreviousConversation = Conversation::where('ad_id',$advertisement->id)->whereHas('users', function ($q) use ($user) {
            $q->whereId($user->id);
        })->whereHas('users', function ($q) use ( $advertisement) {
            $q->whereId($advertisement->user_id);
        })->first();

        if ($havePreviousConversation){
            $conversation = $havePreviousConversation->id;
        }else{
            $conversation = 0;
        }
        
        $userFilter = collect(new \App\Http\Resources\UserFilter($user));
        

        // Person who get Notify Of Price
       $sender =  $advertisement['user']['devices'];

        $taxs = new AdsTaxs();
        $taxs->user_id = $user->id;
        $taxs->ad_id = $advertisement->id;
        $taxs->price = $request->price;
        $taxs->currency = $request->currency;
        $taxs->notes = $request->notes;
        $taxs->save();

        $this->notify->NotificationDbType(3,$advertisement->user_id,$user,$request->price,$taxs , $advertisement );
        if (count($sender) > 0 ) {

            // $deviceUser = $advertisement['user']['devices']->pluck('device');
            
            $deviceIos = $advertisement['user']['devices']->where('device_type','Ios')->pluck('device');
            $deviceAndroid = $advertisement['user']['devices']->where('device_type','android')->pluck('device');

            $this->push->sendPushNotification($deviceAndroid, $deviceIos, 'سوم الإعلانات',
                 ' قام المستخدم '.$user->name .' بتقديم سوم بمبلغ '.$request->price .' علي الإعلان ' . '( '.$advertisement->name .' )' ,
                [
                    'type'=> 3,
                'advertisement' => ['advertisementId' =>  $advertisement->id,'advertisementName' =>  $advertisement->name,'taxId' =>  $taxs->id,'price' => $request->price,'convId' => $conversation ],
                'chat' => $userFilter
                ]
            );
            
        }

        return response()->json( [
            'status' => 200 ,
            'message' => 'تم ارسال السعر بنجاح' ,
        ] , 200 );
    }

    public function acceptedOrRefuseTax(Request $request){

        $user = User::whereApiToken($this->headerApiToken)->first();

        if ( ! $user ) {   return $this->UserNotFound();  }

        $tax = AdsTaxs::whereId($request->taxId)->with('user.devices','ads')->first();

        if (!$tax) {   return $this->taxsNotFound(); }
        
        $taxFound = AdsTaxs::whereId($request->taxId)->where('status','!=', 0)->first();
        
        if($taxFound){
             return response()->json([   'status' => 400,  'error' => (array) ' لا يمكنك قبول او رفض السوم نظرا لحدوث عميلة مسبقة '   ],200);
        }
        
         $userOfAdvertisement = $tax['ads']['user']->id;


        $havePreviousConversation = Conversation::where('ad_id',$tax->ad_id)->whereHas('users', function ($q) use ($user) {
            $q->whereId($user->id);
        })->whereHas('users', function ($q) use ( $userOfAdvertisement) {
            $q->whereId($userOfAdvertisement);
        })->first();

        if ($havePreviousConversation){
            $conversation = $havePreviousConversation->id;
        }else{
            $conversation = 0;
        }

        $nameOfAds =  $tax['ads']->name;

        // Person who get Notify Of Accepted Or refuse
        $sender =  $tax['user']['devices'];
        
         $userFilter = collect(new \App\Http\Resources\UserFilter($user));

        if ($request->type == 1){

            $tax->update(['status' => 1 ]);

            $this->notify->NotificationDbType(4, $tax['user']->id,$user, ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان ' . $nameOfAds ,$tax->ad_id,$tax->id);

            if (count($sender) > 0 ) {

                $deviceIos = $sender->where('device_type','Ios')->pluck('device');
                $deviceAndroid = $sender->where('device_type','android')->pluck('device');

                $this->push->sendPushNotification($deviceAndroid, $deviceIos, 'سوم الإعلانات',
                    ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان '.$nameOfAds  ,
                    ['type'=> 4,'advertisement' => ['advertisementId' => $tax['ads']->id ,'convId' => $conversation ],'chat' => $userFilter  ]
                );
                
                
               

            }
            return response()->json( [
                'status' => 200 ,
                'message' => 'تم قبول السعر بنجاح' ,
            ] , 200 );
        }else{

            $tax->update(['status' => -1 ]);

            $this->notify->NotificationDbType(4, $tax['user']->id,$user,' قام صاحب الإعلان برفض الطلب الخاص بك علي اسم الإعلان ' . $nameOfAds ,$tax->ad_id,$tax->id);

            if (count($sender) > 0 ) {
                
                $deviceIos = $sender->where('device_type','Ios')->pluck('device');
                $deviceAndroid = $sender->where('device_type','android')->pluck('device');
                
                
                $this->push->sendPushNotification($deviceAndroid, $deviceIos, 'سوم الإعلانات',
                     ' قام صاحب الإعلان برفض الطلب الخاص بك علي اسم الإعلان ' .$nameOfAds ,
                    ['type'=> 4,'advertisement' => ['advertisementId' =>  $tax['ads']->id,'convId' => $conversation],'chat' => $userFilter  ]
                );
                
               
 
            }

            return response()->json( [
                'status' => 200 ,
                'message' => 'تم رفض السعر بنجاح' ,
            ] , 200 );

        }
    }
    
    
    public function canComment(Request $request){
        $advertisement = Ad::where('is_suspend',0)->whereId($request->advertisementId)->first();

        if (!$advertisement){ return $this->advertisementNotFound(); }

        if ($request->type == 1){

            $advertisement->update(['can_comment' => 1]);
            return response()->json([   'status' => 200,  'message' =>   'هذ الإعلان لن يتم عليه التعليقات'   ],200);
        }else{

            $advertisement->update(['can_comment' => 0]);

            return response()->json([   'status' => 200,  'message' =>   ' هذ الإعلان  يتم عليه التعليقات الأن'   ],200);
        }
    }


    public function updateAdvertisement(Request $request){
        $advert = Ad::whereId($request->advertisementId)->first();

        if (!$advert){ return $this->advertisementNotFound(); }

        $advert->created_at = Carbon::now();
        $advert->save();

        return response()->json( [
            'status' => 200 ,
        ] , 200 );
    }
    


    private  function advertisementNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذ الإعلان غير متوفر'   ],200);
    }

    private  function taxsNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'سوم الإعلان غير متوفر'   ],200);
    }

    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],401);
    }

}
