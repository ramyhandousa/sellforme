<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\bank;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Faq;
use App\Models\Device; 
use App\Models\Setting;
use App\Models\Support;
use App\User;
use Illuminate\Http\Request;

use App\Models\BankTransfer;
use UploadImage;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
      public $public_path;
       public $headerApiToken;
       public $language;
        public $notify;
        public $push;
       
       public function __construct(InsertNotification $notification,PushNotification $push )
       {
	    $this->language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
	    app()->setLocale($this->language);
	    
	    // api token from header
	    $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
	    
	    $this->notify = $notification;
           $this->push = $push;
	    
	    $this->public_path = 'files/transfers';
	    
       }
       
        public function testNotify(){
            $devices = Device::where('user_id', 131)->pluck('device');
            
            return   $this->push->sendPushNotification($devices, null, 'الحجوزات',
                '2019' );
       }
       
       
   public function aboutUs(){
   
	return response()->json( [
	        'status' => 200 ,
	        'data' => Setting::whereKey('about_app_desc_ar')->first()->body ,
	] , 200 );
	
   }
   
   
   
    public function treaty_of_Use(){
        
        
       $number1 =  Setting::whereKey('treaty_of_Use_1')->first()->body;
        $number2 =  Setting::whereKey('treaty_of_Use_2')->first()->body;
         $number3 =  Setting::whereKey('treaty_of_Use_3')->first()->body;
          $number4 =  Setting::whereKey('treaty_of_Use_4')->first()->body;
           $number5 =  Setting::whereKey('treaty_of_Use_5')->first()->body;
        
        $data = [
            'quran' => $number1,
            'text_1' => $number2,
             'text_2' => $number3,
              'text_3' => $number4,
               'text_4' => $number5,
            ];
   
    	return response()->json( [
    	        'status' => 200 ,
    	        'data' => $data ,
    	] , 200 );
	
   }

   
    public function global(){

    $phoneContact =  Setting::whereKey('contactus_phone')->first() ;
    $whatsContact =  Setting::whereKey('whatsContact')->first() ;
    
     $number1 =  Setting::whereKey('treaty_of_Use_1')->first()->body;
        $number2 =  Setting::whereKey('treaty_of_Use_2')->first()->body;
         $number3 =  Setting::whereKey('treaty_of_Use_3')->first()->body;
          $number4 =  Setting::whereKey('treaty_of_Use_4')->first()->body;
           $number5 =  Setting::whereKey('treaty_of_Use_5')->first()->body;
           
           $showWaring = Setting::whereKey('showWaring')->first()->body;
            $showResponsibility = Setting::whereKey('showResponsibility')->first()->body;
             $showCommission = Setting::whereKey('showCommission')->first()->body;
    
   $staticData = [
   
		   $showWaring,
		   $showResponsibility ,
		  $showCommission 
		   
		   ];
		   
		   $terms = [$number2 ,$number3 ,$number4 ,$number5] ; 
 
    $data = ['phoneContact' => $phoneContact ?$phoneContact->body : " ",
            'whatsContact' => $whatsContact ?$whatsContact->body : " ",
            'staticData' => $staticData,
            'quran' => $number1,
            'terms' =>   $terms
            ];
    	return response()->json( [
    	        'status' => 200 ,
    	        'data' => $data ,
    	] , 200 );
   }
  
   
   public function frequentlyAsked(){

   $frequentlyAsked =  Faq::select('question','answer')->latest()->get();

	return response()->json( [
	        'status' => 200 ,
	        'data' => $frequentlyAsked ,
	] , 200 );
   }


   public function suspendElement(){

   $suspendElement =  Setting::whereKey('suspendElement')->first()->body ;

	return response()->json( [
	        'status' => 200 ,
	        'data' => $suspendElement ,
	] , 200 );
   }

   public function terms(){

   $terms =  Setting::whereKey('terms')->first()->body ;

	return response()->json( [
	        'status' => 200 ,
	        'data' => $terms ,
	] , 200 );
   }
   
   public function appPercentage(){
       
       $appPercentage =  Setting::whereKey('taxs')->first()->body ;
       
       $textAppPercentage  =  Setting::whereKey('taxs_text')->first()->body ;
       
       $notesAppPercentage  =  Setting::whereKey('notes_taxs_text')->first()->body ;
       
       $data = ['appPercentage' => (double)$appPercentage , 'textAppPercentage' => $textAppPercentage ,'notesAppPercentage' => $notesAppPercentage];

        return response()->json( [
                'status' => 200 ,
                'data' => $data ,
        ] , 200 );
   }

    public function listBanks(){

        $data = bank::collection(\App\Models\Bank::where('is_active',1)->get());

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }


    public function contactUs(Request $request){
        // check for user
        $user = User::where( 'api_token' , $this->headerApiToken )->first();
        if ( ! $user ) {   return $this->UserNotFound();  }

        $support = new Support();
        $support->user_id = $user->id;
        $support->type_id =  1;
        $support->name = $request->name;
        $support->phone = $request->phone;
        $support->email = $request->email;
        $support->message = $request->message;
        $support->save();
         $this->notify->NotificationDbType(2,null,$user->id,$request->message);

        return response()->json( [
            'status' => 200 ,
            'message' => trans('global.message_was_sent_successfully'),
        ] , 200 );

    }
    
    
    public function reportsForads(Request $request){

        $user = User::whereApiToken($this->headerApiToken)->first();

        if ( ! $user ) {   return $this->UserNotFound();  }

        $this->notify->NotificationDbType(8,null,$user->id,$request->message,$request->adsId);
        
        return response()->json( [
            'status' => 200 ,
            'message' => 'تم إرسال التقرير بنجاح',
        ] , 200 );
        
    }
    
    public function reportComment(Request $request){

        $user = User::whereApiToken($this->headerApiToken)->first();

        if ( ! $user ) {   return $this->UserNotFound();  }

        $this->notify->NotificationDbType(9,null,$user->id,$request,$request->adsId);

        return response()->json( [
            'status' => 200 ,
            'message' => 'تم الإبلاغ عن التعليق بنجاح',
        ] , 200 );

    }
    
    
    
    public function bankTransfer(Request $request){

        $user = User::where( 'api_token' , $this->headerApiToken )->first();
        if ( ! $user ) {   return $this->UserNotFound();  }

        $bank = new BankTransfer();
        $bank->user_id =  $user->id;
        $bank->userName =  $request->userName;
        $bank->userAccount =  $request->userAccount;
        $bank->iban =  $request->iban;
        $bank->money =  $request->money;
        $bank->bank =  $request->bank;
        if ( $request->hasFile( 'image' ) ):
            $bank->image =  $request->root() . '/public/' . $this->public_path . UploadImage::uploadImage( $request , 'image' , $this->public_path );
        endif;
        $bank->save();
        return response()->json( [
            'status' => 200 ,
            'data' => $bank,
            'message' => 'تمت العملية بنجاح',
        ] , 200 );
    }


    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }
   
}
