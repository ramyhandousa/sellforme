<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\MessageChat;
use App\Libraries\PushNotification;
use App\Models\Ad; 
use App\Models\Conversation;
use App\Models\Device;
use App\Models\Message;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{

    public $headerApiToken;
    public $push;
    public function __construct(PushNotification $push  )
    {
        // api token from header
        $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
        $this->push = $push;
    }

     public function index(Request $request){

        $user = User::where( 'api_token' , $this->headerApiToken )->first();

        if (!$user){  return $this->UserNotFound();  }

        $ids = [];
        foreach ($user->conversations as $row) {
            $ids[] = $row->id;
        }

        $pageSize = $request->pageSize;
        $skipCount = $request->skipCount;
        $page = $request->page;

        $currentPage = $request->get('page', $page); // Default to 1

        $convs = Conversation::latest()->whereIn('id', $ids)->get();


        $data = MessageChat::collection($convs)->slice($skipCount + (($currentPage - 1) * $pageSize))->take($pageSize)->values();

        return response()->json([
            'status' => 200,
            'data' => $data
        ]);

    }

   public function storeMessage(Request $request){


        $user = User::where( 'api_token' , $this->headerApiToken )->first();

        if (!$user){  return $this->UserNotFound();  }

        $advertisement = Ad::whereId($request->advertisementId)->first();

        // if (!$advertisement){ return $this->advertisementNotFound(); }
        
        if($advertisement){
                 $receiver = User::where( 'id' , $advertisement->user_id )->with('devices')->first();
   
        }


        if ($request->convId) {

            $havePreviousConversation = Conversation::whereId($request->convId)->first();

        } else {

            $havePreviousConversation = Conversation::where('ad_id',$request->advertisementId)->whereHas('users', function ($q) use ($user) {
                $q->whereId($user->id);
            })->whereHas('users', function ($q) use ( $receiver) {
                $q->whereId($receiver->id);
            })->first();

        }
        
        if (count((array)$havePreviousConversation) == 0) {
 
            $conversation = new Conversation;
            $conversation->ad_id = $request->advertisementId;

            if ($conversation->save()) {

                $conversation->users()->attach([$user->id, $receiver->id]);

                if(isset($request->message)){

                    $message = new Message();
                    $message->message = $request->message;
                    $message->conversation_id = $conversation->id;
                    $message->user_id = $user->id;

                    if($message->save()){
                        
                    //For Notification
                    $userFilter = collect(new \App\Http\Resources\UserFilter($user));
                    $chat = ['advertisementId' => $request->advertisementId ?(int)$request->advertisementId: $havePreviousConversation->ad_id ,
                                'advertisementName' => $advertisement->name,
                                'conversation_id' => $conversation->id,
                                'message' => $request->message];
                    $userFilter->put('chatting' ,$chat);
                    //---------------

                        // $deviceUser = $receiver['devices']->pluck('device');
                        
                         $deviceIos = $receiver['devices']->where('device_type','Ios')->pluck('device');
                         
                        $deviceAndroid = $receiver['devices']->where('device_type','android')->pluck('device');

                        $this->push->sendPushNotification( $deviceAndroid, $deviceIos, 'رسالة جديدة', $user->name.''.' ارسل لك رسالة '   ,
                                                            ['type'=> 20 ,'chat' => $userFilter]);
                    }

                    return response()->json([
                        'status' => 200,
                        'data' => $message
                    ]);

                }

            } else {
                return response()->json([
                    'status' => 400
                ]);
            }
        }else {

            $to = $havePreviousConversation->users()->where('id', '!=', $user->id)->with('devices')->first();
             
            $message = new Message;
            $message->message = $request->message;
            $message->conversation_id = $havePreviousConversation->id;
            $message->user_id = $user->id;

            if($message->save()){
                
                //For Notification
                    $userFilter = collect(new \App\Http\Resources\UserFilter($user));
                    
                    $chat = ['advertisementId' => $request->advertisementId ?(int)$request->advertisementId: $havePreviousConversation->ad_id ,
                                'advertisementName' =>  Ad::whereId($havePreviousConversation->ad_id)->first()->name,
                                'conversation_id' => $havePreviousConversation->id,
                                'message' => $request->message];
                    $userFilter->put('chatting' ,$chat);
                    //---------------

                // $deviceUser = $to->devices()->pluck('device');
             
                 $deviceIos = $to['devices']->where('device_type','Ios')->pluck('device');
                    $deviceAndroid = $to['devices']->where('device_type','android')->pluck('device');
                        

                $this->push->sendPushNotification( $deviceAndroid, $deviceIos, 'رسالة جديدة', $user->name.''.' ارسل لك رسالة '   ,
                    ['type'=> 20 ,'chat' => $userFilter]);

            }

            DB::table('conversation_user')
                ->where(['user_id' => $to->id, 'conversation_id' => $request->convId])
                ->update(['read_at' => null]);


            DB::table('conversation_user')
                ->where(['user_id' => $user->id, 'conversation_id' => $request->convId])
                ->update(['read_at' => date('Y-m-d H:i:s')]);


            if ($request->convId) {
                $conversation = Conversation::where('id', $request->convId)->first();
                $conversation->updated_at = date('Y-m-d H:i:s');
                $conversation->update();
            }

            return response()->json( [
                'status' => 200 ,
                'data' => $message,
            ] , 200 );
        }
    }
    
    public function getAllMessageBetween(Request $request){

        $user = User::where( 'api_token' , $this->headerApiToken )->first();

        if (!$user){  return $this->UserNotFound();  }

        $pageSize = $request->pageSize;
        $skipCount = $request->skipCount;

        $currentPage = $request->get('page', 1); // Default to 1

        $conversation = Conversation::with(['users', 'messages'])->find($request->convId);



        if (!$conversation || $request->convId == 0 ){
                    return response()->json([
                    'status' => 200,
                    'data' => []
                ]);
        }

        $sender = collect($conversation->users)->filter(function ($userId) use ($user) {
            return $userId->id == $user->id;
        })->first();


        $senderConversationDeletedAt = $sender->pivot->deleted_at ?: null;

        $messages = collect($conversation->messages)->filter(function ($message) use ($senderConversationDeletedAt) {
            return $message->created_at > $senderConversationDeletedAt;
        })->slice($skipCount + (($currentPage - 1) * $pageSize))->take($pageSize)->values();

        DB::table('conversation_user')
            ->where(['user_id' => $user->id, 'conversation_id' => $request->convId])
            ->update(['read_at' => date('Y-m-d H:i:s')]);


        $data = \App\Http\Resources\Chat::collection($messages);

        return response()->json([
            'status' => 200,
            'data' => $data
        ]);

    }

    public function makeAsRead(Request $request){

        $message = Chat::whereId($request->messageId)->first();

        if (!$message){ return $this->MessageNotFound();  }

        $message->is_read = 1;

        $message->save();

        $data = new \App\Http\Resources\Chat($message);

        return response()->json( [
            'status' => 200 ,
            'data' => $data,
        ] , 200 );

    }


    private  function advertisementNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذ الإعلان غير متوفر'   ],200);
    }

    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],401);
    }

    private  function MessageNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'الرسالة غير موجودة لدينا'   ],401);
    }

    private  function ReceiverNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'Receiver Not Found'   ],401);
    }

}
