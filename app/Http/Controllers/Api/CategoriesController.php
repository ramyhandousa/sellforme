<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Ads;
use App\Models\Ad;

use App\Models\Countery;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function getCategory(Request $request , Category $category) {


        $categories =  $category->newQuery();

       if ($request->has('subCategory')) {

            $subCategories  =   $category->whereParentId($request->subCategory)
                                            ->whereIsSuspend(0)->get();

        $all = \App\Http\Resources\Category::collection($subCategories);

        if(count($all) > 0){
            $data = $all->push(['id' =>(int) $request->subCategory, 'name' => 'الكل' ,'image' => $request->root().'/public/adsDefault.png' ]);
            
        }elseif($request->has('subCategory') == 13){
            $data = $all->push(['id' =>(int) 13, 'name' => 'الكل' ,'image' => $request->root().'/public/adsDefault.png' ]);
        }else{
            $data = [];
        }

            return response()->json( [
                'status' => 200 ,
                'data' => $data ,
            ] , 200 );
        }

         if ($request->has('adsCategory')){



            $category =  Category::whereId($request->adsCategory)->with('children')->whereIsSuspend(0)->first();

             if ($category){
                
                if($category->id == 13){
                    
                     $data = Ads::collection(Ad::whereIsSuspend(0)->latest()->get());
    
                    return response()->json( [
                        'status' => 200 ,
                        'data' => $data ,
                    ] , 200 );
                    
                }else{
                    $childrenIds =  $category['children']->pluck('id');
                    
                    $filter =  Ad::where('category_id',$category->id)->whereIsSuspend(0);
                    
                    if($request->cityId){
                        
                         $city = Countery ::whereId($request->cityId)->with('children')->first();
                        
                         $ids =  $city['children']->pluck('id');
                         
                        $filter->where('city_id', $request->cityId)
                                ->orWhereIn('category_id',$childrenIds)->whereIsSuspend(0)->where('city_id', $request->cityId)
                                ->orWhereIn('city_id',$ids)->where('category_id',$category->id)
                                ->orWhereIn('category_id',$childrenIds)->WhereIn('city_id',$ids)->whereIsSuspend(0);
                        
                    }else{
                        
                         $filter->orWhereIn('category_id',$childrenIds)->whereIsSuspend(0);
                    }

                    $ads =  $filter->whereIsSuspend(0)->get();
                    
                    $data = Ads::collection($ads);
    
                    return response()->json( [
                        'status' => 200 ,
                        'data' => $data ,
                    ] , 200 );
                }
            }else{

                return response()->json( [
                    'status' => 200 ,
                    'data' => [] ,
                ] , 200 );

            }


        }


        $mainCategires = $categories->whereParentId(0)
                                    ->whereIsSuspend(0)->get();

        $data = \App\Http\Resources\Category::collection($mainCategires);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }
}
