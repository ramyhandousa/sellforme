<?php

namespace App\Http\Controllers\api;

use App\Http\Helpers\Sms;
use App\Http\Requests\api\resgister;
use App\Models\Countery;
use App\Models\Profile;
use App\Models\VerifyUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class RegisterController extends Controller
{
       public $public_path;
       public $defaultImage;
       public $headerApiToken;
       
       public function __construct( )
       {
	    $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
	    app()->setLocale($language);
     
	    $this->public_path = 'files/users/profiles';
	    
	    // default Image upload in profile user
	    $this->defaultImage = \request()->root() . '/' . 'public/assets/admin/images/profile.jpg';
	    
	    // api token from header
	    $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
	    
       }
       
    public function register(resgister $request){
     
	    $action_code = substr( rand(), 0, 4);

	   $city  = Countery::where('is_active', 0)->whereId($request->cityId)->with('parent')->first();
	   
	   if (!$city){  return $this->CityNotFound(); }
        
        $user = new User();
        $user->name = $request->name;
        // $user->phone = $city['parent']->key_city .ltrim($request->phone, '0');
        $user->phone = $request->phone;
        $user->city_id = $request->cityId;
        $user->api_token = str_random(60);
        $user->password = $request->password;
        $user->address = $request->address;
        $user->latitute = $request->latitute;
        $user->longitute = $request->longitute;
        $user->is_active = 0;
        $user->image = $request->image ? $request->root() . '/public/' . $this->public_path . \UploadImage::uploadImage( $request , 'image' , $this->public_path ): $this->defaultImage;
        $user->save();
        $this->createVerfiy($request , $user , $action_code ,$city);

	    Sms::sendMessage('Activation code:' . $action_code, $request->phone);

        return response()->json( [
            'status' => 200 ,
            'code' => $action_code
        ] , 200 );
    }

   private function createVerfiy($request , $user , $action_code ,$city = null){
       $verifyPhone = new VerifyUser();
       $verifyPhone->user_id = $user->id;
    //   $verifyPhone->phone = $city['parent']->key_city .ltrim($request->phone, '0');
       $verifyPhone->phone = $request->phone;
       $verifyPhone->action_code = $action_code;
       $verifyPhone->save();
   }

    private  function CityNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'المدينة او الدولة غير موجودة'   ],200);
    }
  
}
