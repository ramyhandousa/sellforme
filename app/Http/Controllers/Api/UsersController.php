<?php
    
    namespace App\Http\Controllers\api;
    
    use App\Http\Controllers\Controller;
    use App\Models\Countery;
    use App\Http\Helpers\Sms;
    use App\Http\Resources\Ads;
    use App\Http\Resources\following; 
    use App\Models\Ad;
    use App\Http\Resources\notificationRecource;
    use App\Http\Resources\UserFilter;
    use App\Models\Notification;
    use App\Models\Image; 
    use App\Models\VerifyUser;
    use App\User;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Http\Request;
    use UploadImage;
    use Validator;
    class UsersController extends Controller
    {
    
	 public $public_path;
	 public $defaultImage;
	 public $headerApiToken;
  
	 public function __construct( )
	 {
	        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
	        app()->setLocale($language);
	    
	        $this->public_path = 'files/users/profiles';
	    
	        // default Image upload in profile user
	        $this->defaultImage = \request()->root() . '/' . 'public/assets/admin/images/profile.jpg';
	    
	        // api token from header
	        $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
	    
	 }
  

	 public function uploadImage(Request $request){
	      
	        $image = new Image();
	        $image->url = $request->root() . '/public/' . $this->public_path . UploadImage::uploadImage( $request , 'image' , $this->public_path );
	        $image->save();
	        return response()->json( [
		      'status' => 200 ,
		      'data' => $image ,
	        ] , 200 );
	 }
	 
	 public function adsFollowers(){

         $user = User::whereApiToken($this->headerApiToken)->first();

         if (!$user){  return $this->UserNotFound();  }

         $ids = $user->followers->pluck('id');
         

         if (count($ids) > 0){

             $data = Ads::collection(Ad::where('is_suspend',0)->whereIn('user_id',$ids)->with('user')->get());

             return response()->json([
                 'status' => 200,
                 'data' => $data
             ], 200);

         }else{

             return response()->json([
                 'status' => 200,
                 'data' => []
             ], 200);

         }

     }
     
	  public function listFollowers(Request $request){
         $user = User::whereId($request->userId)->first();

         if (!$user){  return $this->UserNotFound();  }


         return response()->json([
             'status' => 200,
             'data' => following::collection($user->followers)
         ], 200);
     }

	 public function followOrUnFollowUser(Request $request){
         $user = User::whereApiToken($this->headerApiToken)->first();

         if (!$user){  return $this->UserNotFound();  }

 

         if(!$user->followers()->where('follow_id', $request->followId)->first()){

            $user->followers()->attach($request->followId);
             return response()->json([
                 'status' => 200,
                 'message' => 'تم اضافته ضمن المتابعين'
             ], 200);

         }else{

             $user->followers()->detach($request->followId);

             return response()->json([
                 'status' => 201,
                 'message' => 'تم  حذفه من المتابعين'
             ], 200);
         }
     }
     
     
     public function blockUser(Request $request){
         $user = User::whereApiToken($this->headerApiToken)->first();

         if (!$user){  return $this->UserNotFound();  }

         if(!$user->blockers()->where('blocker_id', $request->blockerId)->first()){

             $user->blockers()->attach($request->blockerId);
             return response()->json([
                 'status' => 200,
                 'message' => 'تم اضافته ضمن المحظورين'
             ], 200);

         }else{

             $user->blockers()->detach($request->blockerId);

             return response()->json([
                 'status' => 201,
                 'message' => 'تم  حذفه من المحظورين'
             ], 200);
         }
     }
	 
	 
	 public function editProfile(Request $request){
        
         $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();
         
         $valResult = Validator::make( $this->DataStudent( $request, $user ) , $this->validData( $user ) , $this->messages());

         if ($valResult->fails()) {
             return response()->json( [ 'status' => 400 , 'error' => $valResult->errors()->all() ] , 200 );
         }

         if ($request->cityId ||    $request->cityId != null ){

             $city  = Countery::where('is_active', 0)->whereId($request->cityId)->first();

             if (!$city){   return $this->CityNotFound();  }
         }else{
             $city = Countery::whereId($user->city_id)->first();
         } 

         $action_code = substr(rand(), 0, 4);

         if ($request->phone){
             $checkPhoneChange = $user->wherePhone($request->phone)->count();

             if ($checkPhoneChange == 0){

            
                 $foundPhone = VerifyUser::whereUserId($user->id)->first();

                 if ($foundPhone){

                     $foundPhone->update(['action_code' => $action_code ]);

                 }else{

                     $verify = new  VerifyUser();
                     $verify->user_id= $user->id;
                    //   $verify->phone = $city->key_city .$request->phone;
                       $verify->phone =$request->phone;
                     $verify->action_code = $action_code;
                     $verify->save();

                 }
                  Sms::sendMessage('Activation code:' . $action_code, $request->phone);

                 $user->update(['is_active' => 0]);
             }
         }
         
         $user->update($this->DataStudent( $request, $user ));
         $data = new \App\Http\Resources\User($user->load('ads'));

         return response()->json( [
             'status' => 200 ,
             'code' => $request->phone&&$checkPhoneChange == 0 ?  (string)$action_code : '',
             'message' => 'تم تعديل البيانات بنجاح',
             'data' => $data ,
         ] , 200 );
         
         }
         
     public function getUser(Request $request){
        $user  = User::whereId($request->id)
                            ->whereDoesntHave('roles')
                            ->whereDoesntHave('abilities')
                            ->with('ads')
                            ->first();
	     
	        if (!$user){  return $this->UserNotFound();  }

           $data = new \App\Http\Resources\User($user);
           
             return response()->json( [
                 'status' => 200 ,
                 'data' => $data ,
             ] , 200 );
             
     }

     public  function makeFavourite(Request $request) {

            $user = User::whereApiToken($this->headerApiToken)->first();

            if ( !$user ) {  return    $this->UserNotFound();}

            if(!$user->my_favorites()->where('ads_id', $request->advertisementId)->first()){

                $user->my_favorites()->attach($request->advertisementId);

                return response()->json([
                    'status' => 200,
                    'message' => 'تم اضافته ضمن مفضلتي'
                ], 200);

            }else{

                $user->my_favorites()->detach($request->advertisementId);

                return response()->json([
                    'status' => 201,
                    'message' => 'تم  حذفه من ضمن مفضلتي'
                ], 200);
            }


        }

        public  function  listFavourite() {

            $user = User::whereApiToken($this->headerApiToken)->first();

            if(!$user){  return $this->UserNotFound();  }
            
            $data = Ads::collection( $user->my_favorites);

            return response()->json([
                'status' => 200,
                'data' => $data
            ], 200);

        }
        
        public function listBlocking(Request $request){
            $user = User::whereApiToken($this->headerApiToken)->first();

            if(!$user){  return $this->UserNotFound();  }

            $data = UserFilter::collection( $user->blockers);

            return response()->json([
                'status' => 200,
                'data' => $data
            ], 200);
        }
         public function notification(Request $request){
            $user = User::whereApiToken($this->headerApiToken)->first();

            if ( ! $user ) {   return $this->UserNotFound();  }

            $notification = notificationRecource::collection( Notification::whereUserId($user->id)->latest()->get());
            $notification->each->update(['is_read' => 1]);

            return response()->json( [
                'status' => 200 ,
                'data' => $notification,
            ] , 200 );
        }
        
        
         public function notifyAndMessageCount(Request $request){
            $user = User::whereApiToken($this->headerApiToken)->first();

            if ( ! $user ) {   return $this->UserNotFound();  }
            
            $messageCount =    DB::table('conversation_user')->where('user_id', $user->id)->whereNull('read_at')->count();

            $notificationCount =   Notification::whereUserId($user->id)->where('is_read',0)->count();
           
            $data = [ 'messageunRead' => $messageCount , 'notificationunRead' => $notificationCount ];
            
            return response()->json( [
                'status' => 200 ,
                'data' => $data,
            ] , 200 );
        }

        private function DataStudent ( $request , $user)
        {
            $postData = [
                'name' => $request->name ?$request->name : $user->name ,
                'phone' => $request->phone ?$request->phone : $user->phone ,
                'password' => $request->password ,
                'image' => $request->image ? $request->root() . '/public/' . $this->public_path . \UploadImage::uploadImage( $request , 'image' , $this->public_path ):$user->image ,
                'city_id' => $request->cityId?$request->cityId : $user->city_id,
                 'address' => $request->address ?$request->address : $user->address ,
                'latitute' => $request->latitute ?$request->latitute : $user->latitute ,
                'longitute' => $request->longitute ?$request->longitute : $user->longitute ,
            ];

            return $postData;
        }
    
    
        private function validData ( $user )
        {
            $valRules = [ ];
    
            if(\request()->has('phone')){
                $valRules['phone'] = 'unique:users,phone,' . $user->id ;
            }
            
            
            return $valRules;
        }
    
        private function messages()
        {
            return [
                'email.unique' => trans('global.unique_email'),
                'phone.unique' => trans('global.unique_phone'),
            ];
        }

        private  function UserNotFound(){
            return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
        }
        private  function CityNotFound(){
            return response()->json([   'status' => 400,  'error' => (array)'المدينة او الدولة غير موجودة'   ],200);
        }
        
        private  function adNotFoundByUser(){
            return response()->json([   'status' => 401,  'error' => (array) 'هذ الإعلان  لا ينتمي للمستخدم'   ],200);
        }
        
    }
