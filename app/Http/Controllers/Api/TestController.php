<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Ads;
use App\Http\Resources\AdsCollection;
use App\Http\Resources\Category;
use App\Models\Ad;
use App\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use UploadImage;
use Validator;
class TestController extends Controller
{


    public $headerApiToken;

    public function __construct( )
    {

        $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';

    }


    public function testResource(Request $request){


        $user = new \App\Http\Resources\User(User::with('ads')->find($request->id));
        return response()->json( [
            'status' => true ,
            'data' => $user ,
        ] , 200 );
    }
    public function testcategory(Request $request){


//        $user = Category::collection(\App\Models\Category::whereParentId(0)->get());
        $user = Category::collection(\App\Models\Category::whereParentId($request->id)->get());

        return response()->json( [
            'status' => true ,
            'data' => $user ,
        ] , 200 );
    }

    public function getAllAds(Request $request){


//        $user = Category::collection(\App\Models\Category::whereParentId(0)->get());
//        $user = Ads::collection(Ad::with('comments')->latest()->get());
//        $user = Ads::collection(Ad::latest()->get());

        $ads = Ad::whereIsSuspend(0);
        $findAd =  new Ads(Ad::whereId(1)->first());
        if ($request->cityId || $request->categoryId){

            if ($request->cityId){
                // search for city id
                $ads->where('city_id',$request->cityId);


            }

            if ($request->categoryId){
                // search for category id
                $ads->whereCategoryId($request->categoryId);

            }
            $similarData = Ads::collection($ads->where('id','!=',$findAd->id)->latest()->get());
            $data = ['data' => $findAd , 'similarData' => $similarData];
            return response()->json( [
                'status' => true ,
                'data' => $data ,
            ] , 200 );


        }else{

            return response()->json( [
                'status' => true ,
                'data' => $findAd ,
            ] , 200 );

        }

    }

    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }

    private  function driverPhoneFound(){
        return response()->json([   'status' => 401,  'error' => (array) 'رقم السائق غير موجود لدي حساب المالك'   ],200);
    }






}
