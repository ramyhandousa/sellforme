<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Ads;
use App\Models\Ad;
use App\Models\Category;
use App\Models\Countery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function searchAds(Request $request){

        $ads = Ad::where('is_suspend',0);

        if ($request->cityId){
            
            // search for city id
            $city = Countery::whereId($request->cityId)->with('children')->first();
            if($city){
                 $childrenIds =  $city['children']->pluck('id');

                $ads->where('city_id',$request->cityId)->orWhereIn('city_id',$childrenIds)->get();
                
                 
            }
           
        }

         if ($request->categoryId){
            // search for category id

            $category =  Category::whereId($request->categoryId)->with('children')->first();

            if ($category){

                $childrenIds =  $category['children']->pluck('id');

                $ads->where('category_id',$category->id)->orWhereIn('category_id',$childrenIds)->get();

            }else{

                    return    $this->categoryNotFound();
            }


        }
        
         
             $data = Ads::collection($ads->where('is_suspend',0)->latest()->get());
        
        return response()->json( [
            'status' => 200 ,
            'data' => $data,
        ] , 200 );

    }
    
    
      private  function categoryNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا القسم غير موجود'   ],200);
    }



}
