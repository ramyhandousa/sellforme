<?php

namespace App\Http\Controllers\Admin;


use App\Models\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications;
use App\User;
use App\Models\Device;
use App\Libraries\Main;
use App\Libraries\PushNotification;
use Carbon\Carbon;

class NotificationsController extends Controller
{


    public $push;
    public $main;

    public function __construct(PushNotification $push, Main $main)
    {

        $this->push = $push;
        $this->main = $main;

        Carbon::setLocale(config('app.locale'));

    }

    public function index()
    {
         
        // $notifications = auth()->user()->notifications;
        $notifications = auth()->user()->notifications()->orderBy('created_at', 'desc')->get();


        // $notifications->map(function ($q) {


        //     $notifyData = $this->localizationNotification($q->type, $q->order_id);
        //     $q->type = (int)$q->type;
        //     $q->senderImage = $this->getSenderImage($q->sender_id);
        //     $q->title = $notifyData['title'];
        //     $q->body = $notifyData['body'];

        // });


        // return $notifications;

        return view('admin.notifications.index')->with(compact('notifications'));
    }
    
    
     public function problems()
    { 
          
        $notifications = auth()->user()->notifications()->whereIn('type',[0,9])
                                        ->groupBy('sender_id', 'ads_id')
                                        ->orderBy('created_at', 'desc')
                                        ->with('Ad','sender')->get();
                                        
        return view('admin.notifications.problems')->with(compact('notifications'));
    }


    private function localizationNotification($type, $orderId = null)
    {


        $type = (int)$type;

        $data = [];
        switch ($type) {
            case $type == 3:
                $data['title'] = __('trans.newOrderTitle', ['id' => $orderId]);
                $data['body'] = __('trans.newOrderBody');
                break;

            default:
                $data['title'] = "default title";
                $data['body'] = "default body";
        }

        return $data;

    }

    private function getSenderImage($id)
    {
        $user = User::findOrFail($id);

        if (!$user)
            return "";

        $image = "";

        if ($user->userType() == 'company') {
            $image = $user->company_logo;
        } else {

            $image = $user->image;
        }

        return $image == null ? "" : $image;


    }





    /**
     * Remove User from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function groupDelete(Request $request)
    {


        $ids = $request->ids;

        foreach ($ids as $id) {
            $model = auth()->user()->notifications()->whereId($id)->first();
            $model->delete();
        }


        return response()->json([
            'status' => true,
            'data' => [
                'id' => $request->id
            ]
        ]);
    }


    public function publicNotifications()
    {

        $notifications = Notification::whereType(15)->with('user')->get();
        
        

        return view('admin.notifications.list')->with(compact('notifications'));

    }


    public function createPublicNotifications()
    {
        return view('admin.notifications.public');
    }


    public function sendPublicNotifications(Request $request)
    {


        $users = User::whereIsSuspend(0)->whereIsActive(1)->get();
        
  
         $this->sendNotificationByLang( $request, $users );


        session()->flash('success', __('trans.successSendPublicNotifications'));

        return redirect()->back();


    }

    private function sendNotificationByLang($request, $users)
    {

        $data = array(
            'title' =>  $request->title  ,
            'body' =>     $request->message,
            'type' => '15'
        );

        $notificationData = [];

        $devices = [];

        foreach ($users as $user) {
            
            $notificationData[] = array(
                'user_id' => $user->id,
                'title' => $data['title'],
                'body' => $data['body'],
                'ads_id' => 0,
                'type' => 15,
                'sender_id' => auth()->id(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            );

            foreach ($user->devices as $device) {
                $devices[] = $device;
            }

        }
    
         $this->main->insertData(Notification::class, $notificationData);
         
         foreach ($users as $user) {
           
           $badge = $user->notifications()->whereIsRead(0)->count();
        }
         $IosDevice = collect($devices)->where('device_type', 'Ios')->pluck('device');
	    $androidDevice = collect($devices)->where('device_type', 'android')->pluck('device');
         $this->push->sendPushNotification($androidDevice, $IosDevice, $data['title'], $data['body'], $data , $badge);
         
    }


    public function getNotifications()
    {
        $user = auth()->user();
        $notifications = $user->notifications;
        return view('admin.notifications.index')->with(compact('notifications'));

    }


    public function delete(Request $request)
    {

        $model = Notification::whereId($request->id)->first();


        if ($model->delete()) {
            return response()->json([
                'status' => true,
                'data' => $request->id
            ]);
        }


    }


}
