<?php

namespace App\Http\Controllers\Admin;

use App\Models\Ad;
use App\Models\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Gate;
use UploadImage;
use Validator;
class AdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $ads = Ad::latest()->get();
         

        return view('admin.ads.index', compact('ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $ad = Ad::with('user')->findOrFail($id);

        return view('admin.ads.show', compact('ad'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }

    public function suspend(Request $request)
    {
        $model = Ad::findOrFail($request->id);
        $model->is_suspend = $request->type;
        if ($request->type == 1) {
            $message = "لقد تم حظر  بنجاح";

        } else {
            $message = "لقد تم فك الحظر بنجاح";
        }

        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type

            ]);
        }

    }


    private function getImages($id) {

        $ads =    ad::whereId($id)->first();

        implode(" ,",  $ads->images);
        $filter =  implode(',',  $ads->images) ;
        $ids = explode(',',$filter);

        return   Image::whereIn('id',$ids)->get();
    }

}
