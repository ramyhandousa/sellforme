<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Faq;
use Validator;
use Illuminate\Support\Facades\Gate;

class FaqsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $faqs = Faq::latest()->get();

        return view('admin.faqs.index',compact('faqs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.faqs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Faq;
        $model->question = $request->question;
        $model->answer = $request->answer;

        if ($model->save()) {
            session()->flash('success', 'لقد تم إضافة سؤال بنجاح ' . "($model->question)");
            return redirect(route('faqs.index'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faq = Faq::whereId($id)->first();
        if (!$faq) {
            abort(404);
        }

        return view('admin.faqs.edit',compact('faq'));
    }


    public function update(Request $request, $id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $model = Faq::findOrFail($id);
        $model->question = $request->question;
        $model->answer = $request->answer;
        $model->save();
        session()->flash('success', 'لقد تم تعديل السؤال بنجاح.');
        return redirect()->route('faqs.index');
    }


    public function destroy($id)
    {
        $model = Faq::findOrFail($id);

        if ($model->delete()) {
            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }
    }
}
