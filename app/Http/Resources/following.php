<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class following extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data  = [
            'id'     => $this->id,
            'name'     => $this->name,
            'phone' => $this->phone,
            'image'     => $this->when($this->image , $this->image),
            'city' =>  $this->when($this->city_id , new City($this->city)),

        ];

        return $data;
    }
}
