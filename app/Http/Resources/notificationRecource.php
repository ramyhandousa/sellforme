<?php

namespace App\Http\Resources;

use App\Models\Conversation;
use App\Models\Ad;
use Illuminate\Http\Resources\Json\JsonResource;

class notificationRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
         $havePreviousConversation = Conversation::where('ad_id',$this->ads_id)->whereHas('users', function ($q)  {
            $q->whereId($this->sender_id);
        })->whereHas('users', function ($q)  {
            $q->whereId($this->user_id);
        })->first();
        
        $advertisement = Ad::whereId( $this->ads_id)->first();

        return [
            'id' => $this->id,
            'user_id' => $this->sender_id,
            'ads_id' => $this->ads_id,
            'tax_id' => $this->when($this->tax_id , $this->tax_id) ,
            'advertisementName' => $advertisement ? $advertisement->name : '' ,
            'title' => $this->when($this->title , $this->title) ,
            'body' => $this->when($this->body , $this->body) ,
            'price' => $this->when($this->price , $this->price),
            'is_read' =>  $this->is_read ,
            'type' =>  $this->type ,
            'time'     => $this->when($this->created_at , date('h:i A', strtotime($this->created_at))),
            'conversation_id'     =>
                $this->when(($this->type == 3) ||($this->type == 4) ||($this->type == 5)  ,
                // Check if has Conversation before or not
                                    $havePreviousConversation ? $havePreviousConversation->id : 0
                            ),
            'user' =>  $this->when(($this->type == 3) ||($this->type == 4)   ,
            
                                 new UserFilter(\App\User::whereId($this->sender_id)->first())
                            ),
            
           
        ];
    }
}
