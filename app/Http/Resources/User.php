<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Models\Setting;
class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
//    public function toArray($request)
//    {
//        return parent::toArray($request);
//    }

    public function toArray($request)
    {
        $setting = Setting::whereKey('contactus_phone')->first()->body;
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->when($this->email , $this->email) ,
            'image' => $this->when($this->image , $this->image) ,
            'address' => $this->when($this->address , $this->address) ,
            'latitute' => $this->when($this->latitute , $this->latitute) ,
            'longitute' => $this->when($this->longitute , $this->longitute) ,
            'action_code' => $this->when($this->action_code,$this->action_code) ,
            'is_active' => $this->is_active,
            'is_suspend' => $this->is_suspend,
            'api_token' => $this->api_token,
            'message' => $this->when($this->is_suspend == 1 , 'يرجي  التواصل مع الإدارة علي الرقم ' .$setting  ) , 
            'city' =>  $this->when($this->city_id , new City($this->city)),
            'rate' =>  $this->when($this->ratings , $this->ratings->sum('rating')),
            'ads' => Ads::collection($this->whenLoaded('ads'))
        ];
    }

}
