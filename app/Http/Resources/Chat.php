<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Chat extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
             'id' => $this->id,
//            'conversation_id' => $this->conversation_id,
            'message' => $this->message,
            'time'     => $this->when($this->created_at ,$this->created_at->diffForHumans()),
            'created_at'     => $this->when($this->created_at ,$this->created_at->toDateTimeString()),
            'user' => new UserFilter(\App\User::whereId($this->user_id)->first()),
        ];
    }
}
