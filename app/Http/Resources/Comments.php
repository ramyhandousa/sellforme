<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Comments extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
//    public function toArray($request)
//    {
//        return parent::toArray($request);
//    }

    public function toArray($request)
    {

        $data  = [
            'id'       =>  $this->id,
            // 'user_name'       =>  $this->user->name,
            // 'user_image'       =>  $this->when($this->user->image , $this->user->image),
            'comment'     => $this->comment,
            'time'     => $this->when($this->created_at , date('h:i A', strtotime($this->created_at))),
            'user' =>  new UserFilter($this->user),
            'children' =>   Comments::collection(Comments::where('parent_id',$this->id)->get()),
        ];

        return $data;
    }
}
