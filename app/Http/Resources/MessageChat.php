<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Models\Ad;
class MessageChat extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = \App\User::where( 'api_token' , request()->headers->get('apiToken') )->first();
        $advertisementName = Ad::whereId( $this->ad_id)->first();
        return [
            'conversation_id' => $this->id,
            'advertisementName' => $advertisementName ? $advertisementName->name : ' ' ,
            'read_at' => \DB::table('conversation_user')->where(['user_id' => $user->id, 'conversation_id' => $this->id])->first()->read_at?:"",
            'message' => $this->messages()->latest()->where('conversation_id', $this->id)->first()->message,
            'user' => new UserFilter($this->users()->where('id', '!=', $user->id)->first())
        ];
    }
}
