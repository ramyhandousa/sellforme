<?php

namespace App\Http\Requests\api;

use App\Rules\storeOffer;
use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
class OfferStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'price' => 'required',
	      'periodStart' => [   'date', new  storeOffer()],
	      'periodEnd' => 'required|date',
        ];
    }
       
       public function withValidator($validator)
       {
	    $validator->after(function ($validator) {
		 
		 if ( $this->periodEnd <=  $this->periodStart ) {
		        $validator->errors()->add('periodEnd',trans('global.offer_date_end') );
		 }
		 
	    });
	    return;
       }
       protected function failedValidation(Validator $validator) {
	    
	    $values = $validator->errors()->all();
	    
	    throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
       }
}
