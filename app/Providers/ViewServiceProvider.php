<?php
/**
 * Created by PhpStorm.
 * User: Hassan Saeed
 * Date: 11/16/2017
 * Time: 9:29 AM
 */

namespace App\Providers;
use App\Models\Category;
use App\Libraries\Main;
use App\Models\Setting;
use App\User;
use App\Models\Ad;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use App\Models\Countery;

use App\Models\Support;

class ViewServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
 
            $helper = new \App\Http\Helpers\Images();
            $main_helper = new \App\Http\Helpers\Main();
            $setting = new Setting;
            $main = new Main();
            
            $userHelpAdminCount = User::where('id', '!=', \Auth::id())->whereHas('roles', function ($q) {
						    		$q->where('name', '!=', 'owner');
							 })->count();
							 
			 $usersCount = User::count();
			 
			 $adsCount =Ad::count();
			 
			 $mainCategoryCount = Category::where('parent_id', 0)->count();
			 
            $childCategoryCount = Category::where('parent_id','!=', 0)->count();
            
            
            $mainCounteryCount = Countery::where('parent_id', 0)->count();
            
            $childCounteryCount = Countery::where('parent_id','!=', 0)->count();
            
            $messageNotReadCount = Support::whereIsRead(0)->count();


            $view->with(compact('cats', 'helper', 'main', 'setting','main_helper','cities', 'branches',
                                    'mainCounteryCount','childCounteryCount',
                                'userHelpAdminCount','usersCount','adsCount','mainCategoryCount','childCategoryCount','messageNotReadCount'));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}


