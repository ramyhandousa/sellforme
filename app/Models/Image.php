<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['url'];
    
    protected $hidden = [
        'updated_at','created_at'
    ];
}
