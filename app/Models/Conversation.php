<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    
    protected $hidden = array('pivot');
    
    public function users(){
        return $this->belongsToMany(User::class)->withPivot('deleted_at');
    }
    public function messages(){
        return $this->hasMany(Message::class);
    }


}
