<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class AdsTaxs extends Model
{
    protected $fillable = [
        'status'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }


    public function ads() {
        return $this->belongsTo(Ad::class, 'ad_id');
    }
}
