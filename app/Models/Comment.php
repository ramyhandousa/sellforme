<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
      public function children()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }


    public function parent(){
        return $this->belongsTo(Comment::class,'parent_id');
    }
    
    
}
