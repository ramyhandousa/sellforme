<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Countery extends Model
{
    protected $fillable = ['name','parent_id'];

    public function children()
    {
        return $this->hasMany(Countery::class, 'parent_id');
    }


    public function parent(){
        return $this->belongsTo(Countery::class,'parent_id');
    }

}
