<?php

namespace App\Models;

use App\User;
use App\Models\Ad;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'is_read', 
    ];
    
    public function user()
    {

        return $this->belongsTo(User::class);
    }
    
    
    
    public function sender()
    {

        return $this->belongsTo(User::class,'sender_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
    
    
    public function Ad()
    {
        return $this->belongsTo(Ad::class, 'ads_id');
    }
}
