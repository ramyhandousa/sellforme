<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $fillable = ['user_id',
                            'category_id' ,
                            'city_id' ,
                            'name' ,
                            'description' ,
                            'phone' ,
                            'address' ,
                            'latitute' ,
                            'longitute' ,
                            'images',
                        'can_comment' ,
                        'is_read'
                        ];

    protected $casts = [ 'images' => 'array'];


    public function user() {
        return $this->belongsTo(User::class);
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function city() {
        return $this->belongsTo(Countery::class);
    }

    public function images() {
        return $this->hasMany(Image::class);
    }

    public function views() {
        return $this->hasMany(View::class);
    }

    public function comments() {
        return $this->hasMany(Comment::class);
    }
    
    public function mainComments() {
        return $this->hasMany(Comment::class)->where('parent_id',0);
    }


}
