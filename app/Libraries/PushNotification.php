<?php

namespace App\Libraries;

use App\Libraries\FirebasePushNotifications\Firebase;
use App\Libraries\FirebasePushNotifications\Push;
use App\Message;
use App\User;
use App\Models\Notification;

class PushNotification
{

    public function sendPushNotification($regIdsAndroid = null, $regIdsIos = null,  $title = null, $body = null, $data = [] ,$badge = null )
    {

        $push = new Push();
        $firebase = new Firebase();

        // optional payload
        $dataLoad = array();

        $dataLoad['chat'] = isset($data['chat']) ? $data['chat'] : null;
        
        $dataLoad['href'] = isset($data['href']) ? $data['href'] : null;
        
        $dataLoad['advertisement'] = isset($data['advertisement']) ? $data['advertisement'] : null;
        
        $dataLoad['type'] = isset($data['type']) ? $data['type'] : null;
         
        // notification title
        $push->setTitle($title);
        
        
        if( isset($data['type'])  && $data['type'] == 15   ){
            
            $push->setBadge($badge);
        }else{
             $push->setBadge($this->NotifyCount());
        }
       
        
        
        // $push->setBadge(2);

        // notification message 
        $message = $body;

        $push_type = isset($push_type) ? $push_type : 'multi'; 

        // $include_image = isset($data['image']) ? TRUE : FALSE;

        $push->setMessage($body);

        // if ($include_image) {
        //     $push->setImage($data['image']);
        // } else {
        //     $push->setImage('');
        // }


        //$push->setImage('https://api.androidhive.info/images/minion.jpg');
        $push->setIsBackground(TRUE);
        $push->setData($dataLoad);


        $responseIos = '';
        $responseAndroid = '';

            
            
if (count($regIdsAndroid) >  0) {
             $json = $push->getPushData();
           // $push = $push->getPushNotification();
          $responseAndroid = $firebase->sendMultipleAndroid($regIdsAndroid, $json);
}

if(count($regIdsIos) >  0){
    
    // $json = $push->getPushData();
            $push = $push->getPushNotification();
            $responseIos = $firebase->sendMultipleIos($regIdsIos, $push);
}
    
    
    return [$responseAndroid, $responseIos];
        
    }
    
    
    private  function NotifyCount(){
        
        $user = User::whereApiToken(request()->headers->get('apiToken'))->first();
        
        if (!$user){  return 0;  }
        
        $notify = Notification::where('user_id', $user->id)->where('is_read', 0)->count();
        
        return $notify;
        
    }    

}