<?php

namespace App\Listeners;

use App\Events\EventConversations;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ListenerConversations
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EventConversations $event
     * @return void
     */
    public function handle(EventConversations $event)
    {
        return $event;
    }
}
